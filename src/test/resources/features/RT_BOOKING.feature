Feature: RT BOOKING
  I want to book RT flight

  Scenario Outline: Verify RT Booking with One Way segment
    Given Open the browser
    When Get Connector Result from "<From>" to "<To>" date "<ODN>" adult "<Adult>" child "<Child>" infant "<Infant>" preferedAirLine "<Preferred>" returnDate "<RDN>" isSplitRT "<isSplitRT>" travelclass "<class>" OW CONFIG "<Supplier-OutboundTo>" "<Outbound-AirlineTo>" "<InBound-AirlineTo>" RT CONFIG "<Supplier-InboundFro>" "<Outbound-AirlineFro>" "<InBound-AirlineFro>" Channel "<Channel>" Domain "<Domain>" isIntl "<INTL>"
    Then Validate Connector Result
    When Open HomePage for Channel "<Channel>" Domain "<Domain>" On "<Environment>"
    Then Domain Handling
    When Login
    Then Validate Login
    When Search from "<From>" to "<To>" date "<ODN>" adult <Adult> child <Child> infant <Infant> preferedAirLine "<Preferred>" returnDate "<RDN>" isSplitRT "<isSplitRT>" travelclass "<class>" isTicketingEnabled "<ticketing>" isIntl "<INTL>"
    Then Validate Pre SRP
    When SELECT CONNECTED FLIGHT "<Connected>" OW CONFIG "<Supplier-OutboundTo>" "<Outbound-AirlineTo>" "<InBound-AirlineTo>" RT CONFIG "<Supplier-InboundFro>" "<Outbound-AirlineFro>" "<InBound-AirlineFro>" nearby "<NEARBY>" isSpecialRT "<isSpecialRT>"
    Then Validate SRP
    When Click Book on SRP
    Then Validate Pre Itinarary
    When SELECT Itinarary
    When SELECT SSR "<SEAT>" "<MEAL>" "<BAGGAGE>" "<Insurance>"
    Then Validate Itinerary
    And Apply Coupon "<COUPON_KEY>"
    Then Validate SSR
    When Click Book on Itinarary
    Then Flight Price Change Popup
    Then Flight Change Popup
    Then Check Login Page
    Then Validate Pre Traveller
    When Update Traveller
    Then Flight Price Change Popup
    Then Flight Change Popup
    Then Validate Pre Payment Page
    When Fetch Fare Details
    Then Validate Fare Detail
    When Payment
    Then validate Pre Confirmation
    When Click Payment
    Then Flight Price Change Popup
    Then Flight Change Popup
    Then Confirmation Page
    Then Validate Confirmation Page
    When Get TripXML Information
    Then Validate TripXML
    When Get Stats Information
    Then Validate Stats Information
    When HQOpen
    Then HQLogin
    And Get Booking Information
    Then Validate HQ STEP

    #And Close browser
    #ODN: Onward date number and RDN: Return date number
    Examples: 
      | From | To  | ODN | Adult | Child | Infant | Preferred | RDN | isSplitRT | class   | Connected | Supplier-OutboundTo | Outbound-AirlineTo | InBound-AirlineTo | Supplier-InboundFro | Outbound-AirlineFro | InBound-AirlineFro | Insurance | SEAT  | MEAL  | BAGGAGE | COUPON_KEY | INTL  | NEARBY | isSpecialRT | Channel | Domain | Environment |
      #========================== SG RT split screen DOM =================================================================================================================================================================================================================================================================================================
      | BLR  | DEL |  30 |     1 |     0 |      0 |           |  40 | TRUE     | Economy | FALSE     | SPICEJET            | SG                 | X                 | SPICEJET            | SG                  | X                  | TRUE      | FALSE | FALSE | FALSE   | FALSE      | FALSE | TRUE   | FALSE       | B2C     | COM    | QA2         |
      | BLR  | DEL |  31 |     2 |     1 |      0 |           |  41 | TRUE     | Economy | TRUE      | SPICEJET            | SG                 | SG                | SPICEJET            | SG                  | SG                 | TRUE      | FALSE | TRUE  | TRUE    | FALSE      | FALSE | TRUE   | FALSE       | B2C     | COM    | QA2         |
      | BLR  | DEL |  32 |     1 |     0 |      1 |           |  42 | TRUE     | Economy | FALSE     | SPICEJET            | SG                 | X                 | SPICEJET            | SG                  | X                  | TRUE      | FALSE | TRUE  | FALSE   | FALSE      | FALSE | TRUE   | FALSE       | B2C     | COM    | QA2         |
      | BLR  | DEL |  33 |     1 |     1 |      1 |           |  43 | TRUE     | Economy | FALSE     | SPICEJET            | SG                 | X                 | SPICEJET            | SG                  | X                  | TRUE      | TRUE  | TRUE  | TRUE    | FALSE      | FALSE | TRUE   | FALSE       | B2C     | COM    | QA2         |
      | BLR  | DEL |  34 |     2 |     0 |      0 |           |  44 | TRUE     | Economy | FALSE     | SPICEJET            | SG                 | SG                | SPICEJET            | SG                  | SG                 | TRUE      | FALSE | FALSE | FALSE   | FALSE      | FALSE | TRUE   | FALSE       | B2C     | COM    | QA2         |
      | BLR  | DEL |  35 |     2 |     2 |      2 |           |  45 | TRUE     | Economy | FALSE     | SPICEJET            | SG                 | X                 | SPICEJET            | SG                  | X                  | TRUE      | FALSE | TRUE  | TRUE    | FALSE      | FALSE | TRUE   | FALSE       | B2C     | COM    | QA2         |
      | BLR  | DEL |  36 |     1 |     1 |      1 |           |  46 | TRUE     | Economy | TRUE      | SPICEJET            | SG                 | SG                | SPICEJET            | SG                  | SG                 | TRUE      | TRUE  | TRUE  | TRUE    | FALSE      | FALSE | TRUE   | FALSE       | B2C     | COM    | QA2         |
      | BLR  | DEL |  37 |     2 |     2 |      2 |           |  47 | TRUE     | Economy | TRUE      | SPICEJET            | SG                 | SG                | SPICEJET            | SG                  | SG                 | TRUE      | FALSE | FALSE | FALSE   | FALSE      | FALSE | TRUE   | FALSE       | B2C     | COM    | QA2         |
      #========================== SG RT split screen DOM AE Domain =======================================================================================================================================================================================================================================================================================
      | BLR  | DEL |  50 |     1 |     0 |      0 |           |  60 | TRUE     | Economy | FALSE     | SPICEJET            | SG                 | X                 | SPICEJET            | SG                  | X                  | FALSE     | FALSE | TRUE  | FALSE   | FALSE      | FALSE | TRUE   | FALSE       | B2C     | AE     | QA2         |
      | BLR  | DEL |  51 |     2 |     1 |      0 |           |  61 | TRUE     | Economy | TRUE      | SPICEJET            | SG                 | SG                | SPICEJET            | SG                  | SG                 | FALSE     | FALSE | TRUE  | TRUE    | FALSE      | FALSE | TRUE   | FALSE       | B2C     | AE     | QA2         |
      | BLR  | DEL |  52 |     2 |     2 |      2 |           |  62 | TRUE     | Economy | TRUE      | SPICEJET            | SG                 | SG                | SPICEJET            | SG                  | SG                 | FALSE     | FALSE | TRUE  | TRUE    | FALSE      | FALSE | TRUE   | FALSE       | B2C     | AE     | QA2         |
