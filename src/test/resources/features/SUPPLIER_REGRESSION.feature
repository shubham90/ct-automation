Feature: Supplier Regression

  Scenario Outline: Bookings for Supplier Regression
    Given Open the browser
    When Get Connector Result from "<From>" to "<To>" date "<ODN>" adult "<Adult>" child "<Child>" infant "<Infant>" preferedAirLine "<Preferred>" returnDate "<RDN>" isSplitRT "<isSplitRT>" travelclass "<class>" OW CONFIG "<Supplier-OutboundTo>" "<Outbound-AirlineTo>" "<InBound-AirlineTo>" RT CONFIG "<Supplier-InboundFro>" "<Outbound-AirlineFro>" "<InBound-AirlineFro>" Channel "<Channel>" Domain "<Domain>" isIntl "<INTL>"
    Then Validate Connector Result
    When Open HomePage for Channel "<Channel>" Domain "<Domain>" On "<Environment>"
    Then Domain Handling
    #When Login
    #Then Validate Login
    When Search from "<From>" to "<To>" date "<ODN>" adult <Adult> child <Child> infant <Infant> preferedAirLine "<Preferred>" returnDate "<RDN>" isSplitRT "<isSplitRT>" travelclass "<class>" isTicketingEnabled "<ticketing>" isIntl "<INTL>"
    Then Validate Pre SRP
    When SELECT CONNECTED FLIGHT "<Connected>" OW CONFIG "<Supplier-OutboundTo>" "<Outbound-AirlineTo>" "<InBound-AirlineTo>" RT CONFIG "<Supplier-InboundFro>" "<Outbound-AirlineFro>" "<InBound-AirlineFro>" nearby "<NEARBY>" isSpecialRT "<isSpecialRT>"
    Then Validate SRP
    When Click Book on SRP
    Then Validate Pre Itinarary
    When SELECT Itinarary
    When SELECT SSR "<SEAT>" "<MEAL>" "<BAGGAGE>" "<Insurance>"
    Then Validate Itinerary
    And Apply Coupon "<COUPON_KEY>"
    Then Validate SSR
    When Click Book on Itinarary
    Then Check Login Page
    Then Validate Pre Traveller
    When Update Traveller
    Then Validate Pre Payment Page
    When Fetch Fare Details
    Then Validate Fare Detail
    When Payment
    Then validate Pre Confirmation
    When Click Payment
    Then Confirmation Page
    Then Validate Confirmation Page
    When Get TripXML Information
    Then Validate TripXML
    When Get Stats Information
    Then Validate Stats Information
    When HQOpen
    Then HQLogin
    And Get Booking Information
    Then Validate HQ STEP
    And AssertAll
    And Close browser

    #ODN: Onward date number and RDN: Return date number
    Examples: 
      | From | To  | ODN | Adult | Child | Infant | Preferred | RDN | isSplitRT | class   | Connected | Supplier-OutboundTo | Outbound-AirlineTo | InBound-AirlineTo | Supplier-InboundFro | Outbound-AirlineFro | InBound-AirlineFro | Insurance | SEAT  | MEAL  | BAGGAGE | COUPON_KEY | ticketing | NEARBY | isSpecialRT | Channel | Domain | Environment | INTL |
      #============================ Trujet SMS ON Bookings ===================================================================================================================================================================================================================================================================================================
      #| HYD  | MAA |  20 |     2 |     1 |      1 |           |     | False     | Economy | FALSE     | TRUJET              | 2T                 | X                 | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      #| HYD  | MAA |  21 |     2 |     1 |      1 |           |     | False     | Economy | true      | TRUJET              | 2T                 | 2T                | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      #| HYD  | MAA |  22 |     2 |     1 |      1 |           |  32 | TRUE      | Economy | FALSE     | TRUJET              | 2T                 | X                 | TRUJET              | 2T                  | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      #| HYD  | MAA |  23 |     2 |     1 |      1 |           |  33 | TRUE      | Economy | true      | TRUJET              | 2T                 | 2T                | TRUJET              | 2T                  | 2T                 | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      #============================ 6E SMS OFF Bookings ======================================================================================================================================================================================================================================================================================================
      | DEL  | MAA |  20 |     2 |     1 |      1 |           |     | False     | Economy | FALSE     | INDIGO              | 6E                 | X                 | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      #| DEL  | MAA |  21 |     2 |     1 |      1 |           |     | False     | Economy | true      | INDIGO              | 6E                 | 6E                | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      #| DEL  | MAA |  22 |     2 |     1 |      1 |           |  32 | TRUE      | Economy | FALSE     | INDIGO              | 6E                 | X                 | INDIGO              | 6E                  | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      | DEL  | MAA |  23 |     2 |     1 |      1 |           |  33 | TRUE      | Economy | true      | INDIGO              | 6E                 | 6E                | INDIGO              | 6E                  | 6E                 | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      | PAT  | DXB |  24 |     2 |     1 |      1 |           |  34 | False     | Economy | FALSE     | INDIGO              | 6E                 | X                 | INDIGO              | 6E                  | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | TRUE |
      #============================ Amadeus UK Bookings ======================================================================================================================================================================================================================================================================================================
      #| DEL  | BOM |  20 |     2 |     1 |      1 |           |     | False     | Economy | FALSE     | X                   | UK                 | X                 | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      | DEL  | BOM |  21 |     2 |     1 |      1 |           |     | False     | Economy | true      | X                   | UK                 | UK                | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      | DEL  | BOM |  22 |     2 |     1 |      1 |           |  32 | TRUE      | Economy | FALSE     | X                   | UK                 | X                 | X                   | UK                  | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | TRUE        | B2C     | COM    | QA2         | false |
      #| DEL  | BOM |  23 |     2 |     1 |      1 |           |  33 | TRUE      | Economy | true      | X                   | UK                 | UK                | X                   | UK                  | UK                 | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      | BLR  | SIN |  24 |     2 |     1 |      1 |           |  34 | False     | Economy | FALSE     | X                   | UK                 | X                 | X                   | UK                  | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | TRUE |
      #============================ GAL AI Bookings ==========================================================================================================================================================================================================================================================================================================
      | DEL  | BOM |  20 |     2 |     1 |      1 |           |     | False     | Economy | FALSE     | X                   | AI                 | X                 | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      #| DEL  | BOM |  21 |     2 |     1 |      1 |           |     | False     | Economy | true      | X                   | AI                 | AI                | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      #| DEL  | BOM |  22 |     2 |     1 |      1 |           |  32 | TRUE      | Economy | FALSE     | X                   | AI                 | X                 | X                   | AI                  | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | TRUE        | B2C     | COM    | QA2         | false |
      | DEL  | BOM |  23 |     2 |     1 |      1 |           |  33 | TRUE      | Economy | true      | X                   | AI                 | AI                | X                   | AI                  | AI                 | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      | BLR  | SIN |  24 |     2 |     1 |      1 |           |  34 | False     | Economy | FALSE     | X                   | AI                 | X                 | X                   | AI                  | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | TRUE |
      #============================ SG SMS OFF Bookings ======================================================================================================================================================================================================================================================================================================
      #| DEL  | BOM |  20 |     2 |     1 |      1 |           |     | False     | Economy | FALSE     | X                   | SG                 | X                 | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      | DEL  | BOM |  21 |     2 |     1 |      1 |           |     | False     | Economy | true      | X                   | SG                 | SG                | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      | DEL  | BOM |  22 |     2 |     1 |      1 |           |  32 | TRUE      | Economy | FALSE     | X                   | SG                 | X                 | X                   | SG                  | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | TRUE        | B2C     | COM    | QA2         | false |
      #| DEL  | BOM |  23 |     2 |     1 |      1 |           |  33 | TRUE      | Economy | true      | X                   | SG                 | SG                | X                   | SG                  | SG                 | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      | BLR  | SIN |  24 |     2 |     1 |      1 |           |  34 | False     | Economy | FALSE     | X                   | SG                 | X                 | X                   | SG                  | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | TRUE |
      #============================= 6E SMS ON Bookings ======================================================================================================================================================================================================================================================================================================
      | BOM  | AMD |  20 |     2 |     1 |      1 |           |     | False     | Economy | FALSE     | INDIGO              | 6E                 | X                 | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      #| BOM  | AMD |  21 |     2 |     1 |      1 |           |     | False     | Economy | true      | INDIGO              | 6E                 | 6E                | X                   | X                   | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      #| BOM  | AMD |  22 |     2 |     1 |      1 |           |  32 | TRUE      | Economy | FALSE     | INDIGO              | 6E                 | X                 | INDIGO              | 6E                  | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | TRUE        | B2C     | COM    | QA2         | false |
      | BOM  | AMD |  23 |     2 |     1 |      1 |           |  33 | TRUE      | Economy | true      | INDIGO              | 6E                 | 6E                | INDIGO              | 6E                  | 6E                 | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      | DEL  | SIN |  24 |     2 |     1 |      1 |           |  34 | False     | Economy | FALSE     | INDIGO              | 6E                 | X                 | INDIGO              | 6E                  | X                  | FALSE     | FALSE | FALSE | FALSE   | FALSE      | FALSE     | TRUE   | FALSE       | B2C     | COM    | QA2         | TRUE |
      
