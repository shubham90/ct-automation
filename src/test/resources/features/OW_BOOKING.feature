Feature: OW BOOKING
  Oneway booking flow

  Scenario Outline: Verify Oneway Booking
    Given Open the browser
    When Get Connector Result from "<From>" to "<To>" date "<ODN>" adult "<Adult>" child "<Child>" infant "<Infant>" preferedAirLine "<Preferred>" returnDate "<RDN>" isSplitRT "<isSplitRT>" travelclass "<class>" OW CONFIG "<Supplier-OutboundTo>" "<Outbound-AirlineTo>" "<InBound-AirlineTo>" RT CONFIG "<Supplier-InboundFro>" "<Outbound-AirlineFro>" "<InBound-AirlineFro>" Channel "<Channel>" Domain "<Domain>" isIntl "<INTL>"
    Then Validate Connector Result
    When Open HomePage for Channel "<Channel>" Domain "<Domain>" On "<Environment>"
    Then Domain Handling
    #When Login
    #Then Validate Login
    When Search from "<From>" to "<To>" date "<ODN>" adult <Adult> child <Child> infant <Infant> preferedAirLine "<Preferred>" returnDate "<RDN>" isSplitRT "<isSplitRT>" travelclass "<class>" isTicketingEnabled "<ticketing>" isIntl "<INTL>"
    Then Validate Pre SRP
    When SELECT CONNECTED FLIGHT "<Connected>" OW CONFIG "<Supplier-OutboundTo>" "<Outbound-AirlineTo>" "<InBound-AirlineTo>" RT CONFIG "<Supplier-InboundFro>" "<Outbound-AirlineFro>" "<InBound-AirlineFro>" nearby "<NEARBY>" isSpecialRT "<isSpecialRT>"
    Then Validate SRP
    When Click Book on SRP
    Then Validate Pre Itinarary
    When SELECT Itinarary
    When SELECT SSR "<SEAT>" "<MEAL>" "<BAGGAGE>" "<Insurance>"
    Then Validate Itinerary
    And Apply Coupon "<COUPON_KEY>"
    Then Validate SSR
    When Click Book on Itinarary
    Then Check Login Page
    Then Validate Pre Traveller
    When Update Traveller
    Then Validate Pre Payment Page
    When Fetch Fare Details
    Then Validate Fare Detail
    When Payment
    Then validate Pre Confirmation
    When Click Payment
    Then Confirmation Page
    Then Validate Confirmation Page
    When Get TripXML Information
    Then Validate TripXML
    When Get Stats Information
    Then Validate Stats Information
    When HQOpen
    Then HQLogin
    And Get Booking Information
    Then Validate HQ STEP
    And AssertAll
    And Close browser

    #ODN: Onward date number and RDN: Return date number
    Examples: 
      | From | To  | ODN | Adult | Child | Infant | Preferred | RDN | isSplitRT | class   | Connected | Supplier-OutboundTo | Outbound-AirlineTo | InBound-AirlineTo | Supplier-InboundFro | Outbound-AirlineFro | InBound-AirlineFro | Insurance | SEAT  | MEAL | BAGGAGE | COUPON_KEY | ticketing | NEARBY | isSpecialRT | Channel | Domain | Environment | INTL |
      #========================== SG OW DOM ==========================================================================================================================================================================================================================================================================================================================
      #| BLR  | DEL |  20 |     1 |     0 |      0 |           |     | FALSE     | Economy | FALSE     | SPICEJET            | SG                 | X                 | X                   | X                   | X                  | TRUE      | FALSE | TRUE | TRUE    | SHACHI     | TRUE      | TRUE   | FALSE       | B2C     | COM    | QA2         | false |
      