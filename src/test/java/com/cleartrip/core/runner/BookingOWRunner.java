package com.cleartrip.core.runner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.vimalselvam.cucumber.listener.ExtentProperties;
import com.vimalselvam.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = { "classpath:features/OW_BOOKING.feature" }, glue = { "classpath:com.cleartrip.core.steps",
		"classpath:com.cucumber.core" }, monochrome = true, plugin = { "pretty", "html:target/cucumber-reports/cucumber.html","json:target/OW_BOOKING.json",
				"rerun:rerun/failed_scenarios.txt",
				"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:" })
@Test
public class BookingOWRunner extends AbstractTestNGCucumberTests {
	
	@BeforeClass
	public void setup() {
		ExtentProperties extentProperties = ExtentProperties.INSTANCE;
		String dateTime=getCurrentDateTime();
		String featureName=this.getClass().getSimpleName().replace("Runner", "");
		extentProperties.setReportPath("target/cucumber-reports/"+featureName+"_report_"+dateTime+".html");
	}
	
	@AfterClass
	public static void teardown() {
		Reporter.loadXMLConfig(System.getProperty("user.dir") + "/extent-config.xml");
	}
	
	public static String getCurrentDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss");
		Date date = new Date();
		String currenctDateTime=dateFormat.format(date);
		return currenctDateTime;
	}
}
