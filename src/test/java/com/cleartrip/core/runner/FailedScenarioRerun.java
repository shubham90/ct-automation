package com.cleartrip.core.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		features = {"@rerun/failed_scenarios.txt"}, 
        monochrome = true,
        		glue = {
        		        "classpath:com.cleartrip.core.steps",
        		        "classpath:com.cucumber.core" }, 
        		plugin = { "pretty"}
)
public class FailedScenarioRerun extends AbstractTestNGCucumberTests{

}
