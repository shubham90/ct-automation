package com.cleartrip.core.manager;

import com.cleartrip.core.model.TravellerDetail;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigurationManager {

    private Configuration config = null;
    private ObjectMapper objectMapper = new ObjectMapper();

    public ConfigurationManager(String fileName) throws Exception {
        config = new PropertiesConfiguration(fileName);
    }

    public String getBrowser() {
        return config.getString("browser");
    }

    public String getWait() {
        return config.getString("wait");
    }

    public String getHomePageURL(String key) {
        return config.getString(key);
    }

    public String getUserName() {
        return config.getString("username");
    }

    public String getPassword() {
        return config.getString("password");
    }

    public String getMobileNumber() {
        return config.getString("mobilenumber");
    }

    public String getCardNumber() {
        return config.getString("mastercard.number");
    }
    public String getCardExpiryMonth() {
        return config.getString("mastercard.exp.month");
    }
    public String getCardExpiryYear() {
        return config.getString("mastercard.exp.year");
    }
    public String getCardCVV() {
        return config.getString("mastercard.cvv");
    }
    public String getCardHolder() {
        return config.getString("mastercard.holdername");
    }
    public String getHQURL() {
        return config.getString("hq.page.url");
    }
    public String getStatsURL() {
        return config.getString("stats.url");
    }
    public String getTripXMLURL() {
        return config.getString("trip.xml.url");
    }
    public String getConnectorURL() {
        return config.getString("connector.url");
    }

    List<TravellerDetail> travellerDetailsAdult = objectMapper.
            readValue(this.getClass().getClassLoader().getResourceAsStream("travellers_adult.json"),
                    new TypeReference<List<TravellerDetail>>() { });
    List<TravellerDetail> travellerDetailsInfant = objectMapper.
            readValue(this.getClass().getClassLoader().getResourceAsStream("travellers_infant.json"),
                    new TypeReference<List<TravellerDetail>>() { });
    List<TravellerDetail> travellerDetailsChild = objectMapper.
            readValue(this.getClass().getClassLoader().
                    getResourceAsStream("travellers_child.json"), new TypeReference<List<TravellerDetail>>() { });
    Map<String, String> coupons = objectMapper.
            readValue(this.getClass().getClassLoader().
                    getResourceAsStream("coupons.json"), new TypeReference<HashMap<String,String>>() {});

    public List<TravellerDetail> getTravellerDetailsAdult() {
        return travellerDetailsAdult;
    }

    public void setTravellerDetailsAdult(List<TravellerDetail> travellerDetailsAdult) {
        this.travellerDetailsAdult = travellerDetailsAdult;
    }

    public List<TravellerDetail> getTravellerDetailsInfant() {
        return travellerDetailsInfant;
    }

    public void setTravellerDetailsInfant(List<TravellerDetail> travellerDetailsInfant) {
        this.travellerDetailsInfant = travellerDetailsInfant;
    }

    public List<TravellerDetail> getTravellerDetailsChild() {
        return travellerDetailsChild;
    }

    public void setTravellerDetailsChild(List<TravellerDetail> travellerDetailsChild) {
        this.travellerDetailsChild = travellerDetailsChild;
    }

    public Map<String, String> getCoupons() {
        return coupons;
    }

    public void setCoupons(Map<String, String> coupons) {
        this.coupons = coupons;
    }
    public boolean isNoMealFeatureTesting() {
        return Boolean.valueOf(config.getString("isMealFeatureTesting"));
    }
    public boolean isNoMealTestCase() {
        return Boolean.valueOf(config.getString("noMeal.case"));
    }

}
