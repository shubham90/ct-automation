package com.cleartrip.core.manager;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.ConnectorResponse;
import com.cleartrip.core.model.FareDetailResponse;
import com.cleartrip.core.model.FlightRecordInfo;
import com.cleartrip.core.model.HQResponse;
import com.cleartrip.core.model.ItineraryResponse;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.PassangerDetail;
import com.cleartrip.core.model.PaymentResponse;
import com.cleartrip.core.model.SRPResponse;
import com.cleartrip.core.model.StatsResponse;
import com.cleartrip.core.model.TripResponse;
import com.cleartrip.core.model.TripXMLResponse;
import com.cleartrip.core.pages.ConfirmationPage;
import com.cleartrip.core.pages.FareDetailsPage;
import com.cleartrip.core.pages.HQPage;
import com.cleartrip.core.pages.HomePage;
import com.cleartrip.core.pages.IntermediatePage;
import com.cleartrip.core.pages.ItineraryPage;
import com.cleartrip.core.pages.MiscellaneousPage;
import com.cleartrip.core.pages.PaymentPage;
import com.cleartrip.core.pages.SRPPage;
import com.cleartrip.core.pages.SSRPage;
import com.cleartrip.core.pages.TravellerPage;

public class PageObjectManager {

    private long wait;
    private String homePageURL;
    private String hqPageURL;
    private MiscellaneousPage miscellaneousPage;
    private FlightRecordInfo flightRecordInfo;
    private MiscellaneousResponse miscellaneousResponse;
    private SoftAssert softAssert;
    private ConnectorResponse connectorResponse;
    private TripXMLResponse tripXMLResponse;
    private HQResponse hqResponse;
    private ItineraryResponse itineraryResponse;
    private StatsResponse statsResponse;

    public PaymentResponse getPaymentResponse() {
        return paymentResponse;
    }

    public void setPaymentResponse(PaymentResponse paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    public SRPResponse getSrpResponse() {
        return srpResponse;
    }

    public void setSrpResponse(SRPResponse srpResponse) {
        this.srpResponse = srpResponse;
    }

    private TripResponse tripResponse;
    private BookingRequest bookingRequest;
    private List<PassangerDetail> passangerDetails;
    private FareDetailResponse fareDetailResponse;
    private PaymentResponse paymentResponse;
    private SRPResponse srpResponse;


    public void setMiscellaneousPage(MiscellaneousPage miscellaneousPage) {
        this.miscellaneousPage = miscellaneousPage;
    }

    public void setFlightRecordInfo(FlightRecordInfo flightRecordInfo) {
        this.flightRecordInfo = flightRecordInfo;
    }

    public void setMiscellaneousResponse(MiscellaneousResponse miscellaneousResponse) {
        this.miscellaneousResponse = miscellaneousResponse;
    }

    public void setSoftAssert(SoftAssert softAssert) {
        this.softAssert = softAssert;
    }

    public void setConnectorResponse(ConnectorResponse connectorResponse) {
        this.connectorResponse = connectorResponse;
    }

    public void setTripXMLResponse(TripXMLResponse tripXMLResponse) {
        this.tripXMLResponse = tripXMLResponse;
    }

    public void setHqResponse(HQResponse hqResponse) {
        this.hqResponse = hqResponse;
    }

    public void setItineraryResponse(ItineraryResponse itineraryResponse) {
        this.itineraryResponse = itineraryResponse;
    }

    public void setStatsResponse(StatsResponse statsResponse) {
        this.statsResponse = statsResponse;
    }

    public void setTripResponse(TripResponse tripResponse) {
        this.tripResponse = tripResponse;
    }

    public void setBookingRequest(BookingRequest bookingRequest) {
        this.bookingRequest = bookingRequest;
    }

    public void setPassangerDetails(List<PassangerDetail> passangerDetails) {
        this.passangerDetails = passangerDetails;
    }

    public void setFareDetailResponse(FareDetailResponse fareDetailResponse) {
        this.fareDetailResponse = fareDetailResponse;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        PageObjectManager.driver = driver;
    }

    private static WebDriver driver;
    private HomePage homePage;
    private SRPPage srpPage;
    private TravellerPage travellerPage;
    private ItineraryPage itineraryPage;
    private PaymentPage paymentPage;
    private FareDetailsPage fareDetailsPage;
    private HQPage hqPage;
    private ConfirmationPage confirmationPage;
    private IntermediatePage intermediatePage;
	private SSRPage ssrPage;

    public PageObjectManager(long wait, String homePage, String hqPage) {
        this.wait = wait;
        this.homePageURL = homePage;
        this.hqPageURL = hqPage;
        this.itineraryResponse = new ItineraryResponse();
        this.softAssert = new SoftAssert();
        this.passangerDetails = new ArrayList<>();
        this.miscellaneousResponse = new MiscellaneousResponse();
        this.fareDetailResponse=new FareDetailResponse();
        srpResponse = new SRPResponse();
        paymentResponse = new PaymentResponse();
    }

    public HomePage getHomePage() throws Exception {
        return (homePage == null) ? homePage = new HomePage(driver, wait, homePageURL) : homePage;
    }

    public SRPPage getSRPPage() throws Exception {
        return (srpPage == null) ? srpPage = new SRPPage(driver, wait, homePageURL) : srpPage;
    }

    public ItineraryPage getItineraryPage() throws Exception {
        return (itineraryPage == null) ? itineraryPage = new ItineraryPage(driver, wait, homePageURL) : itineraryPage;
    }

    public TravellerPage getTravellerPage() throws Exception {
        return (travellerPage == null) ? travellerPage = new TravellerPage(driver, wait, homePageURL) : travellerPage;
    }

    public PaymentPage getPaymentPage() throws Exception {
        return (paymentPage == null) ? paymentPage = new PaymentPage(driver, wait, homePageURL) : paymentPage;
    }

    public FareDetailsPage getFareDetailsPage() throws Exception {
        return (fareDetailsPage == null) ? fareDetailsPage = new FareDetailsPage(driver, wait, homePageURL) : fareDetailsPage;
    }

    public HQPage getHQPage() throws Exception {
        return (hqPage == null) ? hqPage = new HQPage(driver, wait, hqPageURL) : hqPage;
    }

    public ConfirmationPage getConfirmationPage() throws Exception {
        return (confirmationPage == null) ? confirmationPage = new ConfirmationPage(driver, wait, hqPageURL) : confirmationPage;
    }

    public IntermediatePage getIntermediatePage() throws Exception {
        return (intermediatePage == null) ? intermediatePage = new IntermediatePage(driver, wait) : intermediatePage;
    }

    public SSRPage getSSRPage() throws Exception {
        return (ssrPage == null) ? ssrPage = new SSRPage(driver, wait): ssrPage;
    }
    public MiscellaneousPage getMiscellaneousPage() throws Exception {
        return (miscellaneousPage == null) ? miscellaneousPage = new MiscellaneousPage(driver, wait): miscellaneousPage;
    }

    public FlightRecordInfo getFlightRecordInfo() {
        return flightRecordInfo;
    }

    public MiscellaneousResponse getMiscellaneousResponse() {
        return miscellaneousResponse;
    }

    public SoftAssert getSoftAssert() {
        return softAssert;
    }

    public ConnectorResponse getConnectorResponse() {
        return connectorResponse;
    }

    public TripXMLResponse getTripXMLResponse() {
        return tripXMLResponse;
    }

    public HQResponse getHqResponse() {
        return hqResponse;
    }

    public ItineraryResponse getItineraryResponse() {
        return itineraryResponse;
    }

    public StatsResponse getStatsResponse() {
        return statsResponse;
    }

    public TripResponse getTripResponse() {
        return tripResponse;
    }

    public BookingRequest getBookingRequest() {
        return bookingRequest;
    }

    public List<PassangerDetail> getPassangerDetails() {
        return passangerDetails;
    }

    public FareDetailResponse getFareDetailResponse() {
        return fareDetailResponse;
    }

    public void reset() {
        flightRecordInfo = new FlightRecordInfo();
        miscellaneousResponse = new MiscellaneousResponse();
        softAssert = new SoftAssert();
        connectorResponse = new ConnectorResponse();
        tripXMLResponse = new TripXMLResponse();
        hqResponse = new HQResponse();
        itineraryResponse = new ItineraryResponse();
        statsResponse = new StatsResponse();
        tripResponse = new TripResponse();
        passangerDetails = new ArrayList<>();
        fareDetailResponse = new FareDetailResponse();
        srpResponse = new SRPResponse();
        paymentResponse = new PaymentResponse();

    }
}
