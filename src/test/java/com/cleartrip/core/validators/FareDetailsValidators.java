package com.cleartrip.core.validators;

import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.FareBreakupInfo;
import com.cleartrip.core.model.FareDetailResponse;
import com.cleartrip.core.model.ItineraryResponse;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.pages.PageBase;
import com.cleartrip.core.utils.Helper;

public class FareDetailsValidators {


    public void validate(SoftAssert softAssert, BookingRequest bookingRequest,
                         FareDetailResponse fareDetailResponse) {

//        PageBase.addLog("fareDetailResponse : " + fareDetailResponse);


        Assert.assertNotNull(fareDetailResponse, "Fare Details response cannot be null");
        Assert.assertNotNull(fareDetailResponse.getAdultCount(), "Adult count cannot be null");
        Assert.assertNotNull(fareDetailResponse.getBaseFareAdult(), "Adult Base Fare cannot be null");

        if (bookingRequest.getInfants() > 0) {
            Assert.assertNotNull(fareDetailResponse.getInfantCount(), "Infant count cannot be null");
            Assert.assertNotNull(fareDetailResponse.getBaseFareInfant(), "Infant Base Fare cannot be null");
        }

        if (bookingRequest.getChilds() > 0) {
            Assert.assertNotNull(fareDetailResponse.getChildCount(), "Child count cannot be null");
            Assert.assertNotNull(fareDetailResponse.getBaseFareChild(), "Child Base Fare cannot be null");
        }
		
		if (bookingRequest.isBaggageRequested()) {
            Assert.assertNotNull(fareDetailResponse.getMealsAndBaggage_fare(), "Meal & Baggage Amount cannot be null");
        }

        if (bookingRequest.isMealRequested()) {
            Assert.assertNotNull(fareDetailResponse.getMealsAndBaggage_fare(), "Meal & Baggage Amount cannot be null");
        }

        if (bookingRequest.isSeatRequested()) {
            Assert.assertNotNull(fareDetailResponse.getSeat_fare(), "Seat Amount cannot be null");
        }

        if (bookingRequest.isInsuranceSelected()) {
            Assert.assertNotNull(fareDetailResponse.getInsuranceValue(), "Insurance Amount cannot be null");
        }
        }
		
	public void validatePaxCountAndBaseFare(SoftAssert softAssert, BookingRequest bookingRequest,FareBreakupInfo fri_srp, ItineraryResponse
            itinerary, FareDetailResponse fareDetailResponse) {

        PageBase.addLog("Validating pax counts and their base fare on fare break details pop up on payment pages.");
//        PageBase.addLog("fareDetailResponse : " + fareDetailResponse);
        PageBase.addLog("Validating pax count on payment page.");
        softAssert.assertTrue(bookingRequest.getAdults() == fareDetailResponse.getAdultCount(), "The adult count is incorrect. Expected[SRP]: " + bookingRequest.getAdults() + " Actual[PaymentPage]: " + fareDetailResponse.getAdultCount());
        softAssert.assertTrue(bookingRequest.getChilds() == fareDetailResponse.getChildCount(), "The child count is incorrect. Expected[SRP]: " + bookingRequest.getChilds() + " Actual[PaymentPage]: " + fareDetailResponse.getChildCount());
        softAssert.assertTrue(bookingRequest.getInfants() == fareDetailResponse.getInfantCount(), "The infant count is incorrect. Expected[SRP]: " + bookingRequest.getInfants() + " Actual[PaymentPage]: " + fareDetailResponse.getInfantCount());

        PageBase.addLog("Validating total base fare for all the pax between SRP and payment pages.");
        Double total_baseFare_paymentPage = Helper.roundAvoid(fareDetailResponse.getBaseFareAdult() + fareDetailResponse.getBaseFareChild() + fareDetailResponse.getBaseFareInfant(),0);
        softAssert.assertEquals(Helper.roundAvoid(fri_srp.getBaseFare(),0), Helper.roundAvoid(total_baseFare_paymentPage,0), "The total base fare is incorrect. Expected [SRP]: " + fri_srp.getBaseFare() + " Actual[paymentPage]: " + total_baseFare_paymentPage);

        PageBase.addLog("Validating pax wise base fare between payment and itinerary pages");

        softAssert.assertTrue(Helper.roundAvoid(fareDetailResponse.getBaseFareAdult(),0) > 0.0, "Adult base fare. Expected[ItineraryPage]: > 0.0 , Actual[PaymentPage]: " + fareDetailResponse.getBaseFareAdult());
        if (bookingRequest.getChilds() > 0) {
            softAssert.assertTrue(Helper.roundAvoid(fareDetailResponse.getBaseFareChild(),0) > 0.0, "Child base fare. Expected[ItineraryPage]: > 0.0 , Actual[PaymentPage]: " + fareDetailResponse.getBaseFareChild());
        }
        if (bookingRequest.getInfants() > 0) {
            softAssert.assertTrue(Helper.roundAvoid(fareDetailResponse.getBaseFareInfant(),0) > 0.0, "Infant base fare. Expected[ItineraryPage]: > 0.0 , Actual[PaymentPage]: " + fareDetailResponse.getBaseFareInfant());
        }
    }

    public void validateSSRFare(SoftAssert softAssert, ItineraryResponse itinerary,MiscellaneousResponse miscellaneousResponse, FareDetailResponse
            fareDetailResponse) {

        PageBase.addLog("Validating SSR fare details on fare break details pop up on payment page.");
        PageBase.addLog("Validating mealAndBaggage fare between itinerary page step 1 and payment page.");
        softAssert.assertEquals(Helper.roundAvoid(itinerary.getMealAndBaggageFare(),0),Helper.roundAvoid(fareDetailResponse.getMealsAndBaggage_fare(),0), "The mealAndBaggage fare is incorrect on payment page. Expected[ItineraryPage]: " + itinerary.getMealAndBaggageFare() + " Actual[PaymentPage]: " + fareDetailResponse.getMealsAndBaggage_fare());

        PageBase.addLog("Validating seat fare between itinerary page step 1 and payment page.");
        softAssert.assertEquals(Helper.roundAvoid(itinerary.getSeatFare(),0),Helper.roundAvoid(fareDetailResponse.getSeat_fare(),0), "The seat fare is incorrect on payment page. Expected[ItineraryPage]: " + itinerary.getSeatFare() + " Actual[PaymentPage]: " + fareDetailResponse.getSeat_fare());

        PageBase.addLog("Validating coupon discount value between itinerary page step 1 and payment page.");
        softAssert.assertEquals(Helper.roundAvoid(miscellaneousResponse.getCouponDiscountValue(),0),Helper.roundAvoid(fareDetailResponse.getCouponDiscountValue(),0), "The coupon discount value is incorrect on payment page. Expected[ItineraryPage]: " + miscellaneousResponse.getCouponDiscountValue() + " Actual[PaymentPage]: " + fareDetailResponse.getCouponDiscountValue());

        PageBase.addLog("Validating insurance fare between itinerary page step 1 and payment page.");
        softAssert.assertEquals(Helper.roundAvoid(itinerary.getInsuranceValue(),0),Helper.roundAvoid(fareDetailResponse.getInsuranceValue(),0), "The insurance fare is incorrect on payment page. Expected[ItineraryPage]: " + itinerary.getInsuranceValue() + " Actual[PaymentPage]: " + fareDetailResponse.getInsuranceValue());
    }

    public void validateTotalFeeAndTotalTax(SoftAssert softAssert, FareBreakupInfo fri_srp, FareDetailResponse fareDetailResponse) {

        PageBase.addLog("Validating total tax on payment page to be paid by travellers");
        softAssert.assertEquals(Helper.roundAvoid(fri_srp.getTax(),0),Helper.roundAvoid(fareDetailResponse.getTotalTax(),0), "The total tax is incorrect on payment page. Expected[SRP]: " + fri_srp.getTax() + " Actual[PaymentPage]: " + fareDetailResponse.getTotalTax());

        PageBase.addLog("Validating total amount on payment page to be paid by travellers based on fare break calculations.");
        Double totalAmountOnPaymentPage = Helper.roundAvoid(fareDetailResponse.getBaseFareAdult() + fareDetailResponse.getBaseFareChild() + fareDetailResponse.getBaseFareInfant() + fareDetailResponse.getConvenienceFee() + fareDetailResponse.getMealsAndBaggage_fare() + fareDetailResponse.getSeat_fare() + fareDetailResponse.getTotalTax() - fareDetailResponse.getCouponDiscountValue() + fareDetailResponse.getInsuranceValue() + fareDetailResponse.getEmiProcessingFee() - fareDetailResponse.getCleartripCash() - fareDetailResponse.getGiftVoucherValue(),0);
        softAssert.assertEquals(totalAmountOnPaymentPage,Helper.roundAvoid(fareDetailResponse.getTotalAmount(),0), "The total amouont to be paid by traveller is incorrect based on fare break up calcuation. Expected[PaymentPage fare break up calculation]: " + totalAmountOnPaymentPage + " Actual[PaymentPage Total]: " + fareDetailResponse.getTotalAmount());
    }

    public void validateMisc(SoftAssert softAssert, FareDetailResponse fareDetailResponse) {
        PageBase.addLog("Validating the total convenience fee based on number of travellers.");
        Double totalConvenienceFee = Helper.roundAvoid(fareDetailResponse.getConvenienceFeePerPerson() * (fareDetailResponse.getAdultCount() + fareDetailResponse.getChildCount()),0);
        softAssert.assertEquals(totalConvenienceFee , Helper.roundAvoid(fareDetailResponse.getConvenienceFee(), 0), "The total convenience fee based on traveller count is incorrect. Expected[based on count]: " + totalConvenienceFee + " Actual[PaymentPage]: " + fareDetailResponse.getConvenienceFee());

        PageBase.addLog("Validating the total amount paid by user on payment page near make payment.");
        softAssert.assertEquals(Helper.roundAvoid(fareDetailResponse.getTotalAmount(),0),Helper.roundAvoid(fareDetailResponse.getTotalAmountOnPaymentPage(),0), "The total amount showing on payment page and on break up page are mismatched. Expected[breakup page]: " + fareDetailResponse.getTotalAmount() + " Actual[Make payment page]: " + fareDetailResponse.getTotalAmountOnPaymentPage());
    }


}
