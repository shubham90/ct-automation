package com.cleartrip.core.validators;

import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.FareBreakupInfo;
import com.cleartrip.core.model.SRPResponse;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.model.FlightRecordInfo;

import java.util.List;

public class SRPValidators {

    private final Logger log = org.apache.log4j.Logger.getLogger(SRPValidators.class);

    public void validateSRPDetails(SoftAssert softAssert,
                                   BookingRequest bookingRequest, SRPResponse srpResponse) throws Exception {

        Assert.assertNotNull(srpResponse, "SRP Object is null");
        Assert.assertNotNull(srpResponse.getSelectionBookElement(), "SRP Booking Selection Not found");
        Assert.assertNotNull(srpResponse.getFareBreakupInfo(), "SRP FareDetails cannot be null");
        validateFareBreakUp(softAssert, srpResponse.getFareBreakupInfo());
        if (! bookingRequest.isSplitRT()) {
            validateFlightDetails(softAssert, srpResponse.getFlightRecordInfos());
        }
    }

    private void validateFareBreakUp(SoftAssert softAssert, FareBreakupInfo fareBreakupInfo) {
        softAssert.assertNotNull(fareBreakupInfo.getBaseFare());
        softAssert.assertNotNull(fareBreakupInfo.getTax());
        softAssert.assertNotNull(fareBreakupInfo.getTotalFare());
    }

    private void validateFlightDetails(SoftAssert softAssert, List<FlightRecordInfo> flightRecordInfos) {

        Assert.assertNotNull(flightRecordInfos.size() > 0, "Terminal Should be more than zero");

        for ( int index = 0; index < flightRecordInfos.size(); index++) {
            Assert.assertNotNull(flightRecordInfos.get(index).getTo(), "To location cannot be null");
            Assert.assertNotNull(flightRecordInfos.get(index).getFrom(), "From location cannot be null");
            Assert.assertNotNull(flightRecordInfos.get(index).getFlightCodeAndNumber(), "Flight code and Number cannot be null");
            Assert.assertNotNull(flightRecordInfos.get(index).getTravelClass(), "Travel Class cannot be null");
            Assert.assertNotNull(flightRecordInfos.get(index).getAirlineName(), "AirLine Number cannot be null");
            Assert.assertNotNull(flightRecordInfos.get(index).getFromAirportTerminalName(), "From Terminal cannot be null");
            Assert.assertNotNull(flightRecordInfos.get(index).getToAirportTerminalName(), "To Terminal cannot be null");
            Assert.assertNotNull(flightRecordInfos.get(index).getFromDate(), "From Date cannot be null");
            Assert.assertNotNull(flightRecordInfos.get(index).getToTime(), "To Terminal cannot be null");
        }
    }


}
