package com.cleartrip.core.validators;

import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.SRPResponse;
import com.cleartrip.core.pages.PageBase;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.model.FlightRecordInfo;
import com.cleartrip.core.model.ItineraryResponse;

import java.util.List;

public class ItineraryPageValidators {
	
	private final Logger log = org.apache.log4j.Logger.getLogger(ItineraryPageValidators.class);

    public void validateItineraryResponse(SRPResponse srpResponse, ItineraryResponse itineraryResponse, BookingRequest bookingRequest, SoftAssert softAssert) throws Exception {
        validateItineraryResponse(itineraryResponse, bookingRequest,softAssert);
        compareResponsesFlightRecordInfo(itineraryResponse.getFlightRecordInfos(),
                srpResponse.getFlightRecordInfos(), bookingRequest.isSplitRT(),softAssert);

    }

    private void compareResponsesFlightRecordInfo(List<FlightRecordInfo> itineraryResponse,
                                                  List<FlightRecordInfo> srpResponse, boolean isSRT,SoftAssert softAssert) {
        if (isSRT) {
            return;
        }

        softAssert.assertEquals(itineraryResponse.size(), srpResponse.size(), "Size should be same" +itineraryResponse.size() +
        "  " + srpResponse.size());


        for (int index = 0; index < itineraryResponse.size(); index++) {
//            PageBase.addLog("SRP Response " + srpResponse.get(index));
//            PageBase.addLog("itineraryResponse :  " + itineraryResponse.get(index));
            softAssert.assertEquals(srpResponse.get(index).getAirlineName(),
                    itineraryResponse.get(index).getAirlineName(), "AirLine should be same");

            softAssert.assertEquals(srpResponse.get(index).getFlightCodeAndNumber(),
                    itineraryResponse.get(index).getFlightCodeAndNumber(), "Flight And Code should be same");

            softAssert.assertEquals(srpResponse.get(index).getFrom(),
                    itineraryResponse.get(index).getFrom(), "From should be same");


            softAssert.assertTrue(itineraryResponse.get(index).getFromAirportTerminalName()
                            .contains(srpResponse.get(index).getFromAirportTerminalName())
                    , "From Airline should be same");

            softAssert.assertTrue(itineraryResponse.get(index).getFromDate().replaceAll(",", "").contains(srpResponse.get(index).getFromDate()), "From Date should be same");

            softAssert.assertEquals(srpResponse.get(index).getTo(),
                    itineraryResponse.get(index).getTo(), "To should be same");


            softAssert.assertTrue(itineraryResponse.get(index).getToAirportTerminalName().contains(srpResponse.get(index).getToAirportTerminalName())
                    , "To Airport should be same");

            softAssert.assertEquals(srpResponse.get(index).getToTime(),
                    itineraryResponse.get(index).getToTime(), "To Time should be same");
            softAssert.assertEquals(srpResponse.get(index).getTravelClass().replaceAll(" ","_"),
                    itineraryResponse.get(index).getTravelClass(), "Travel Class should be same");
            softAssert.assertTrue(itineraryResponse.get(index).getToDate().replaceAll(",", "").contains(srpResponse.get(index).getToDate())
                    , "To Date should be same");
            softAssert.assertEquals(srpResponse.get(index).getFromTime(),
                    itineraryResponse.get(index).getFromTime(), "From Time should be same");
        }
    }

    private void validateItineraryResponse(ItineraryResponse itineraryResponse, BookingRequest bookingRequest, SoftAssert softAssert) {
        Assert.assertNotNull(itineraryResponse, "itineraryResponse cannot ne null");
        Assert.assertNotNull(itineraryResponse.getItinaryId(), "Itinarary cannot be null");
        Assert.assertNotNull(itineraryResponse.getFlightRecordInfos(), "FlightRecordInfos cannot be null");
        Assert.assertNotNull(itineraryResponse.getItineraryBtnElement(), "ItineraryBtnElement cannot be null");
        Assert.assertFalse(itineraryResponse.getFlightRecordInfos().size() == 0 , "FlightRecordInfos cannot be zero");
        Assert.assertTrue(bookingRequest.isInsuranceSelected() ? itineraryResponse.isInsuranceSelected() : true, "Insurance must be true");

        for (int index = 0; index < itineraryResponse.getFlightRecordInfos().size(); index++) {
            FlightRecordInfo flightRecordInfo = itineraryResponse.getFlightRecordInfos().get(index);
            softAssert.assertNotNull(flightRecordInfo.getToTime(), "To Time cannot be null");
            softAssert.assertNotNull(flightRecordInfo.getTo(), "To cannot be null");

            softAssert.assertNotNull(flightRecordInfo.getFromDate(), "from Date cannot be null");
            softAssert.assertNotNull(flightRecordInfo.getFrom(), "From cannot be null");
            softAssert.assertNotNull(flightRecordInfo.getToAirportTerminalName(), "To Airport Terminal cannot be null");
            softAssert.assertNotNull(flightRecordInfo.getFromAirportTerminalName(), "From Airport Terminal cannot be null");
            softAssert.assertNotNull(flightRecordInfo.getAirlineName(), "Airline cannot be null");
            softAssert.assertNotNull(flightRecordInfo.getTravelClass(), "Travel Class cannot be null");
            softAssert.assertNotNull(flightRecordInfo.getFlightCodeAndNumber(), "FlightCode and Number cannot be null");

            softAssert.assertNotNull(flightRecordInfo.getFromTime(), "From Time cannot be null");
            softAssert.assertNotNull(flightRecordInfo.getToDate(), "To Date cannot be null");
        }

    }

}
