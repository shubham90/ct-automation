package com.cleartrip.core.validators.connector;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;
import org.apache.commons.jxpath.xml.DOMParser;
import org.apache.commons.lang.StringUtils;
import org.testng.asserts.SoftAssert;
import org.w3c.dom.Document;

import com.cleartrip.core.model.APIResponse;
import com.cleartrip.core.model.HQResponse;
import com.cleartrip.core.pages.PageBase;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IndigoValidator {

    private static String AVAILABILITY_JOURNEY_REGEX = "/s:Envelope/s:Body/x:GetAvailabilityByTripResponse/y:GetTripAvailabilityResponse/y:Schedules/y:ArrayOfJourneyDateMarket/y:JourneyDateMarket/y:Journeys/y:Journey";
    private static String AVAILABILITY_BOOK_SERVICE_NAME_SPACE = "http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService";
    private static String AVAILABILITY_BOOKING_NAME_SPACE = "http://schemas.navitaire.com/WebServices/DataContracts/Booking";
    private static String AVAILABILITY_SEGMENT_REGEX = "y:Segments/y:Segment";
    private static String AVAILABILITY_ARRIVAL_TIME_REGEX = "y:STA";
    private static String AVAILABILITY_DEPARTURE_TIME_REGEX = "y:STD";
    private static String AVAILABILITY_ARRIVAL_STATION_REGEX = "y:ArrivalStation";
    private static String AVAILABILITY_DEPARTURE_STATION_REGEX = "y:DepartureStation";
    private static String AVAILABILITY_FLIGHT_LEGS_REGEX = "y:Legs/y:Leg";
    private static String AVAILABILITY_FLIGHT_LEG_REGEX = "y:LegInfo";
    private static String AVAILABILITY_FLIGHT_ARRIVAL_TERMINAL_REGEX = "y:ArrivalTerminal";
    private static String AVAILABILITY_FLIGHT_DEPERTURE_TERMINAL_REGEX = "y:DepartureTerminal";
    private static String AVAILABILITY_FLIGHT_SEGMENT_KEY = "y:SegmentSellKey";
    private static String AVAILABILITY_FLIGHT_JOURNEY_KEY = "y:JourneySellKey";
    private static String AVAILABILITY_FLIGHT_DETAILS_REGEX = "y:FlightDesignator";
    private static String AVAILABILITY_FLIGHT_NUMBER_REGEX = "a:FlightNumber";

    private static String TAX_JOURNEY_REGEX = "/s:Envelope/s:Body/x:PriceItineraryResponse/y:Booking/y:Journeys/y:Journey";
    private static String TAX_BOOK_SERVICE_NAME_SPACE = "http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService";
    private static String TAX_BOOK_SERVICE_COMMON_NAME_SPACE = "http://schemas.navitaire.com/WebServices/DataContracts/Common";
    private static String TAX_BOOKING_NAME_SPACE = "http://schemas.navitaire.com/WebServices/DataContracts/Booking";
    private static String TAX_SEGMENT_REGEX = "y:Segments/y:Segment";

    private static String TAX_FLIGHT_FARES_REGEX = "y:Fares/y:Fare";
    private static String TAX_PAX_AMOUNT_REGEX = "y:Amount";

    private static String TAX_PAX_FARE_PRICE_REGEX = "y:ChargeType";
    private static String TAX_PAX_FARE_BASES_CODE_REGEX = "y:FareBasisCode";

    private static String TAX_FLIGHT_PAX_FARE_REGEX = "y:PaxFares/y:PaxFare";
    private static String TAX_FLIGHT_PAX_FARE_AMOUNT_REGEX = "y:ServiceCharges/y:BookingServiceCharge";
    private static String TAX_FLIGHT_SEGMENT_KEY = "y:SegmentSellKey";
    private static String TAX_FLIGHT_PAX_TYPE = "y:PaxType";
    private static String TAX_FLIGHT_JOURNEY_KEY = "y:JourneySellKey";


    ObjectMapper objectMapper = new ObjectMapper();


    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static void fetchJourneyDetails(String xml, Map<String, Object> journeyDetails) throws Exception {
        Document doc = (Document) new DOMParser().parseXML(new ByteArrayInputStream(xml.getBytes("UTF-8")));
        // Map<String, Object> journeyDetails = new HashMap<>();
        JXPathContext context = JXPathContext.newContext(doc);
        context.registerNamespace("x", TAX_BOOK_SERVICE_NAME_SPACE);
        context.registerNamespace("y", TAX_BOOKING_NAME_SPACE);
        context.registerNamespace("z", TAX_BOOK_SERVICE_COMMON_NAME_SPACE);

        for (Iterator<Pointer> iter = context.iteratePointers(TAX_JOURNEY_REGEX); iter.hasNext(); ) {

            Pointer pointer = iter.next();
            String journeyKey = (String) context.getRelativeContext(pointer).getPointer(TAX_FLIGHT_JOURNEY_KEY).getValue();

            Map<String, Object> segmentInfos = new HashMap<>();

            for (Iterator<Pointer> segmentIterator = context.getRelativeContext(pointer).iteratePointers(TAX_SEGMENT_REGEX); segmentIterator.hasNext(); ) {
                Pointer segmentPointer = segmentIterator.next();
                String segmentKey = (String) context.getRelativeContext(segmentPointer).getPointer(TAX_FLIGHT_SEGMENT_KEY).getValue();
                String arrivalTime = (String) context.getRelativeContext(segmentPointer).getPointer(AVAILABILITY_ARRIVAL_TIME_REGEX).getValue();
                String departureTime = (String) context.getRelativeContext(segmentPointer).getPointer(AVAILABILITY_DEPARTURE_TIME_REGEX).getValue();

                String arrivalStation = (String) context.getRelativeContext(segmentPointer).getPointer(AVAILABILITY_ARRIVAL_STATION_REGEX).getValue();
                String departureStation = (String) context.getRelativeContext(segmentPointer).getPointer(AVAILABILITY_DEPARTURE_STATION_REGEX).getValue();

                List<String> flights = new ArrayList<>();

                for (Iterator<Pointer> iter2 = context.getRelativeContext(segmentPointer).iteratePointers(AVAILABILITY_FLIGHT_DETAILS_REGEX); iter2.hasNext(); ) {
                    Pointer pointer2 = iter2.next();
                    String flightNumber = (String) context.getRelativeContext(pointer2).getPointer(AVAILABILITY_FLIGHT_NUMBER_REGEX).getValue();
                    System.out.println("flightNumber " + flightNumber);
                    flights.add(flightNumber);
                }

                Map<String, Map<String, Map<String, Double> >> fareDetails = fetchFareDatails(context, segmentPointer);
                Map<String, Object> segmentDetailsMap = new HashMap();
                segmentDetailsMap.put("fareDetails", fareDetails);
                segmentDetailsMap.put("ARRIVAL_STATION", arrivalStation);
                segmentDetailsMap.put("ARRIVAL_TIME", arrivalTime);
                segmentDetailsMap.put("DEP_STATION", departureStation);
                segmentDetailsMap.put("DEP_TIME", departureTime);
                segmentDetailsMap.put("flights", StringUtils.join(flights, ","));

                segmentInfos.put(segmentKey, segmentDetailsMap);
            }
            journeyDetails.put(journeyKey, segmentInfos);
        }

    }


    @SuppressWarnings("unchecked")
	private static Map<String, Map<String, Map<String, Double> >> fetchFareDatails(JXPathContext context, Pointer pointer) {
        Map<String, Map<String, Map<String, Double> >> fareDetails = new HashMap<>();

        for (Iterator<Pointer> faresIterator = context.getRelativeContext(pointer).iteratePointers(TAX_FLIGHT_FARES_REGEX); faresIterator.hasNext(); ) {
            Double totalBaseFare = 0.0d;
            Double totalTax = 0.0d;

            Pointer faresPointer = faresIterator.next();
            String fareBases = (String) context.getRelativeContext(faresPointer).getPointer(TAX_PAX_FARE_BASES_CODE_REGEX).getValue();


            Map<String, Map<String, Double> > paxFare = new HashMap<>();

            for (Iterator<Pointer> paxIterator = context.getRelativeContext(faresPointer).iteratePointers(TAX_FLIGHT_PAX_FARE_REGEX); paxIterator.hasNext(); ) {
                Pointer paxPointer = paxIterator.next();
                String paxType = (String) context.getRelativeContext(paxPointer).getPointer(TAX_FLIGHT_PAX_TYPE).getValue();
                Map<String, Double> fare = new HashMap<>();
                for (Iterator<Pointer> fareIterator = context.getRelativeContext(paxPointer).iteratePointers(TAX_FLIGHT_PAX_FARE_AMOUNT_REGEX); fareIterator.hasNext(); ) {
                    Pointer farePointer = fareIterator.next();
                    String amount = (String) context.getRelativeContext(farePointer).getPointer(TAX_PAX_AMOUNT_REGEX).getValue();

                    String fareType = (String) context.getRelativeContext(farePointer).getPointer(TAX_PAX_FARE_PRICE_REGEX).getValue();

                    if (fareType.equalsIgnoreCase("FarePrice")) {
                        totalBaseFare += Double.valueOf(amount);
                    } else {
                        totalTax += Double.valueOf(amount);
                    }
                }
                fare.put("TOTAL_BASE_FARE", totalBaseFare);
                fare.put("TOTAL_TAXES", totalTax);
                paxFare.put(paxType, fare);
            }


            fareDetails.put(fareBases, paxFare);
        }
        return fareDetails;
    }

    @SuppressWarnings({ "unchecked", "unused", "rawtypes" })
	public static void fetchLegInformation(String xml, Map<String, Object> journeyDetails) throws Exception {
        // String xml = IOUtils.toString(new FileReader(new File("/Users/ashishnaidu/ct-automation_17_03_2019/src/test/java/com/cleartrip/core/validators/connectors/response.xml")));
        Document doc = (Document) new DOMParser().parseXML(new ByteArrayInputStream(xml.getBytes("UTF-8")));

        JXPathContext context = JXPathContext.newContext(doc);
        context.registerNamespace("x", AVAILABILITY_BOOK_SERVICE_NAME_SPACE);
        context.registerNamespace("y", AVAILABILITY_BOOKING_NAME_SPACE);

        for (Iterator<Pointer> journeysIterator = context.iteratePointers(AVAILABILITY_JOURNEY_REGEX); journeysIterator.hasNext(); ) {

            Pointer journeyPointer = journeysIterator.next();
            String journeyKey = (String) context.getRelativeContext(journeyPointer).getPointer(AVAILABILITY_FLIGHT_JOURNEY_KEY).getValue();

            for (Iterator<Pointer> segmentIterator = context.getRelativeContext(journeyPointer).iteratePointers(AVAILABILITY_SEGMENT_REGEX); segmentIterator.hasNext(); ) {
                Pointer segmentPointer = segmentIterator.next();
                String segmentKey = (String) context.getRelativeContext(segmentPointer).getPointer(AVAILABILITY_FLIGHT_SEGMENT_KEY).getValue();


                List<Map<String, Object>> legsInfos = new ArrayList<>();
                for (Iterator<Pointer> legsIterator = context.getRelativeContext(segmentPointer).iteratePointers(AVAILABILITY_FLIGHT_LEGS_REGEX); legsIterator.hasNext(); ) {
                    Pointer legPointer = legsIterator.next();
                    String arrivalTime = (String) context.getRelativeContext(legPointer).getPointer(AVAILABILITY_ARRIVAL_TIME_REGEX).getValue();
                    String departureTime = (String) context.getRelativeContext(legPointer).getPointer(AVAILABILITY_DEPARTURE_TIME_REGEX).getValue();
                    Map<String, Object> legsInfo = new HashMap<>();

                    String arrivalStation = (String) context.getRelativeContext(legPointer).getPointer(AVAILABILITY_ARRIVAL_STATION_REGEX).getValue();
                    String departureStation = (String) context.getRelativeContext(legPointer).getPointer(AVAILABILITY_DEPARTURE_STATION_REGEX).getValue();

                    legsInfo.put("ARRIVAL_STATION", arrivalStation);
                    legsInfo.put("ARRIVAL_TIME", arrivalTime);
                    legsInfo.put("DEP_STATION", departureStation);
                    legsInfo.put("DEP_TIME", departureTime);

                    List<String> flights = new ArrayList<>();

                    for (Iterator<Pointer> iter2 = context.getRelativeContext(legPointer).iteratePointers(AVAILABILITY_FLIGHT_DETAILS_REGEX); iter2.hasNext(); ) {
                        Pointer pointer2 = iter2.next();
                        String flightNumber = (String) context.getRelativeContext(pointer2).getPointer(AVAILABILITY_FLIGHT_NUMBER_REGEX).getValue();
                        System.out.println("flightNumber " + flightNumber);
                        flights.add(flightNumber);
                    }

                    legsInfo.put("flights", Arrays.asList(flights));

                    for (Iterator<Pointer> iter3 = context.getRelativeContext(legPointer).iteratePointers(AVAILABILITY_FLIGHT_LEG_REGEX); iter3.hasNext(); ) {
                        Pointer pointer3 = iter3.next();
                        String arrivalTerminal = (String) context.getRelativeContext(pointer3).getPointer(AVAILABILITY_FLIGHT_ARRIVAL_TERMINAL_REGEX).getValue();
                        String depTerminal = (String) context.getRelativeContext(pointer3).getPointer(AVAILABILITY_FLIGHT_DEPERTURE_TERMINAL_REGEX).getValue();

                        legsInfo.put("DEP_TERMINAL", depTerminal);
                        legsInfo.put("ARRIV_TERMINAL", arrivalTerminal);

                    }
                    legsInfos.add(legsInfo);
                }
                for (Iterator<Pointer> faresIterator = context.getRelativeContext(segmentPointer).iteratePointers(TAX_FLIGHT_FARES_REGEX); faresIterator.hasNext(); ) {

                    Pointer faresPointer = faresIterator.next();
                    String fareBases = (String) context.getRelativeContext(faresPointer).getPointer(TAX_PAX_FARE_BASES_CODE_REGEX).getValue();
                    Double totalBaseFare = 0.0d;
                    for (Iterator<Pointer> paxIterator = context.getRelativeContext(faresPointer).iteratePointers(TAX_FLIGHT_PAX_FARE_REGEX); paxIterator.hasNext(); ) {
                        Pointer paxPointer = paxIterator.next();
                        String paxType = (String) context.getRelativeContext(paxPointer).getPointer(TAX_FLIGHT_PAX_TYPE).getValue();
                        Map<String, Double> fare = new HashMap<>();
                        for (Iterator<Pointer> fareIterator = context.getRelativeContext(paxPointer).iteratePointers(TAX_FLIGHT_PAX_FARE_AMOUNT_REGEX); fareIterator.hasNext(); ) {
                            Pointer farePointer = fareIterator.next();
                            String amount = (String) context.getRelativeContext(farePointer).getPointer(TAX_PAX_AMOUNT_REGEX).getValue();

                            String fareType = (String) context.getRelativeContext(farePointer).getPointer(TAX_PAX_FARE_PRICE_REGEX).getValue();

                            if (fareType.equalsIgnoreCase("FarePrice")) {
                                totalBaseFare += Double.valueOf(amount);
                            }
                        }
                    }
                    if (journeyDetails.containsKey(journeyKey) &&
                            ((Map<String, Map>) journeyDetails.get(journeyKey)).containsKey(segmentKey) &&
                            ! ((Map<String, Map<String, Map<String, Double> >>)((Map<String, Map>) journeyDetails.get(journeyKey)).get(segmentKey).get("fareDetails")).containsKey(fareBases)) {
                        Map<String, Double> fare = new HashMap<>();
                        Map<String, Map<String, Double>> fareDetails = (Map<String, Map<String, Double> >)((Map<String, Map>) journeyDetails.get(journeyKey)).get(segmentKey).get("fareDetails");

                        fare.put("TOTAL_BASE_FARE", totalBaseFare);
                        fare.put("TOTAL_TAXES", 0.0d);
                        fareDetails.put(fareBases,fare );
                    }

                }

                if (journeyDetails.containsKey(journeyKey)) {
                    ((Map<String, Map>) journeyDetails.get(journeyKey)).get(segmentKey).put("legsinfo", legsInfos);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
	public void validate(List<APIResponse> responses, HQResponse hqResponse, SoftAssert softAssert) throws Exception {
        Map<String, Object> journeyDetails = new HashMap<>();

        responses.forEach(response -> {
                    if (response.getType().contains("GetItineraryPrice") && StringUtils.isNotBlank(response.getResponse()) && response.getResponse().contains("PriceItineraryResponse")) {
                        try {
                            fetchJourneyDetails(response.getResponse(), journeyDetails);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

        );
        responses.forEach(response -> {
                    if (response.getType().contains("Availability") && StringUtils.isNotBlank(response.getResponse()) && response.getResponse().contains("GetAvailabilityByTripResponse")) {
                        try {
                            fetchLegInformation(response.getResponse(), journeyDetails);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

        );

        PageBase.addLog("journey details : " + objectMapper.writeValueAsString(journeyDetails));
        Map<String, Map<String, Double>> data1response = new HashMap<>();
        int adult = hqResponse.getAdultCount();
        int child = hqResponse.getChildCount();
        int infant = hqResponse.getInfantCount();
        Iterator<Map.Entry<String, Object>> journeyIterator = journeyDetails.entrySet().iterator();
        while (journeyIterator.hasNext()) {
            Map.Entry<String, Object> journeys = journeyIterator.next();
            Map<String, Object> segments = (Map<String, Object>) journeys.getValue();
            Iterator<Map.Entry<String, Object>> segmentsIterator = segments.entrySet().iterator();

            while (segmentsIterator.hasNext()) {
                StringBuilder sb = new StringBuilder();

                Map<String, Map<String, Double>> data1 = new HashMap<>();

                Map.Entry<String, Object> segment = segmentsIterator.next();
                Map<String, Object> segmentData = (Map<String, Object>) segment.getValue();
                String arrival = (String) segmentData.get("ARRIVAL_STATION");
                String arrivalTime = (String) segmentData.get("ARRIVAL_TIME");
                String depat = (String) segmentData.get("DEP_STATION");
                String departTime = (String) segmentData.get("DEP_TIME");
                String flights = (String) segmentData.get("flights");
                sb.append(depat);
                sb.append("_");
                sb.append(departTime);
                sb.append("_");
                sb.append(arrival);
                sb.append("_");
                sb.append(arrivalTime);
                sb.append("_");

                sb.append(flights);
                String lastKey = null;
                Iterator<Map.Entry<String, Object>> fareInfoIterator = ((Map<String, Object>)segmentData.get("fareDetails")).entrySet().iterator();
                while (fareInfoIterator.hasNext()) {

                    Map.Entry<String, Object> fareDetail = fareInfoIterator.next();

                    String fareBasesCode = fareDetail.getKey();
                    Double taxes = 0.0d;
                    Double baseFare = 0.0d;

                    Map<String, Object> data = (Map<String, Object>)fareDetail.getValue();
                    Map<String, Double> adultPricing = (Map<String, Double>)data.get("ADT");
                    Map<String, Double> childPricing = (Map<String, Double>)data.get("CHD");

                    Map<String, Double> infantPricing = (Map<String, Double>)data.get("INF");

                    baseFare += (adultPricing!= null && adultPricing.get("TOTAL_BASE_FARE") != null) ? adultPricing.get("TOTAL_BASE_FARE")*adult : 0 ;
                    taxes += (adultPricing!= null && adultPricing.get("TOTAL_TAXES") != null) ? adultPricing.get("TOTAL_TAXES")*adult : 0 ;

                    baseFare += (childPricing!= null && childPricing.get("TOTAL_BASE_FARE") != null) ? childPricing.get("TOTAL_BASE_FARE")*child : 0 ;
                    taxes += (childPricing!= null && childPricing.get("TOTAL_TAXES") != null) ? childPricing.get("TOTAL_TAXES")*child : 0 ;

                    baseFare += (infantPricing!= null && infantPricing.get("TOTAL_BASE_FARE") != null) ? infantPricing.get("TOTAL_BASE_FARE")*infant : 0 ;
                    taxes += (infantPricing!= null &&  infantPricing.get("TOTAL_TAXES") != null) ? infantPricing.get("TOTAL_TAXES")*infant : 0 ;

                    if (taxes == 0.0D) {
                        Map<String, Double> price1 = new HashMap<>();
                        price1.put("TOTAL_TAXES", data1response.get(lastKey).get("TOTAL_TAXES"));
                        price1.put("TOTAL_BASE_FARE", baseFare);
                        data1.put(fareBasesCode, price1);
                        data1response.put(sb.toString()+"_"+fareBasesCode, price1);

                        continue;
                    }

                        Map<String, Double> price1 = new HashMap<>();
                        price1.put("TOTAL_TAXES", taxes);
                        price1.put("TOTAL_BASE_FARE", baseFare);
                        data1response.put(sb.toString()+"_"+fareBasesCode, price1);
                    lastKey = sb.toString()+"_"+fareBasesCode;

                }

                //data1response.put(sb.toString(), data1);


            }


        }

        PageBase.addLog("data1response :::: " + data1response);

        Map<String, String> responseData = new HashMap<>();
        Map<String, String> mappings = new HashMap<>();

        hqResponse.getFlights().forEach(flight -> {
            try {
                responseData.put(flight.getFrom() + "_" + dateConvertor(flight.getDepartTime()) + "_" + flight.getTo() + "_" + dateConvertor(flight.getArrivalTime()) + "_" + flight.getAirline().replaceAll("6E-", ""), "");
                mappings.put(flight.getFrom() +" - " + flight.getTo(), flight.getFrom() + "_" + dateConvertor(flight.getDepartTime()) + "_" + flight.getTo() + "_" + dateConvertor(flight.getArrivalTime()) + "_" +
                        flight.getAirline().replaceAll("6E-", ""));
            }catch (Exception e) {
                e.printStackTrace();
            }
                }

        );

        PageBase.addLog("responseData :::: " + responseData);
        PageBase.addLog("mappings :::: " + mappings);


        Map<String, String> responseData1 = new HashMap<>();
        hqResponse.getTickets().forEach(ticketDetails -> {
            responseData1.put(ticketDetails.getSector(), ticketDetails.getFareBasesCode());
                }

        );
        Iterator<Map.Entry<String, String>> iterator = mappings.entrySet().iterator();
        PageBase.addLog("responseData1 :::: " + responseData1);

        Map<String, String> responseData21 = new HashMap<>();

        while (iterator.hasNext()) {
            Map.Entry<String, String> data = iterator.next();
            String key = data.getKey();
            responseData21.put(mappings.get(key)+"_"+responseData1.get(key), "");
        }

        PageBase.addLog("MAPPINGS :::: " + objectMapper.writeValueAsString(responseData21));

        Double baseFareTotal = 0.0d;
        Double taxesTotal = 0.0d;

        Iterator<Map.Entry<String, String>> responseIterator = responseData21.entrySet().iterator();
        while (responseIterator.hasNext()) {
            Map.Entry<String, String> response123 = responseIterator.next();

                if (data1response.get(response123.getKey()) != null) {
                    PageBase.addLog("verify2 :::: " + response123.getKey() + "   " + response123.getValue());
                    PageBase.addLog("verify2 :::: " + data1response.get(response123.getKey()));
                    baseFareTotal =  data1response.get(response123.getKey()).get("TOTAL_BASE_FARE");
                    taxesTotal =  data1response.get(response123.getKey()).get("TOTAL_TAXES");

                }
            }

        PageBase.addLog("baseFareTotal :::: " + baseFareTotal);
        PageBase.addLog("taxesTotal :::: " + taxesTotal);


    }

    private String dateConvertor(String date) throws Exception {
        Date date1=new SimpleDateFormat("HH:mm , dd MMM yyyy").parse(date);
        String date2=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date1);
    return date2;
    }

    public static void main(String args[]) throws Exception {
        Map<String, Object> journeyDetails = new HashMap<>();
        String availability = IOUtils.toString(new FileReader(new File("/Users/ashishnaidu/ct-automation_19_3_2019/src/test/java/com/cleartrip/core/validators/connector/response1.xml")));
        String price = IOUtils.toString(new FileReader(new File("/Users/ashishnaidu/ct-automation_19_3_2019/src/test/java/com/cleartrip/core/validators/connector/response.xml")));
        fetchJourneyDetails(price, journeyDetails);
        fetchLegInformation(availability, journeyDetails);
        ObjectMapper objectMapper = new ObjectMapper();
        //Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        System.out.println(objectMapper.writeValueAsString(journeyDetails));
        //System.out.println(journeyDetails);
        String sDate1="19:25 , 11 Apr 2019";
        Date date1=new SimpleDateFormat("HH:mm , dd MMM yyyy").parse(sDate1);
        System.out.println(sDate1+"\t"+date1);
        String date2=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date1);
        System.out.println(sDate1+"\t"+date2);



    }
}
