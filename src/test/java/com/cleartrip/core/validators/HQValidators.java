package com.cleartrip.core.validators;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.FareDetailResponse;
import com.cleartrip.core.model.HQResponse;
import com.cleartrip.core.model.ItineraryResponse;
import com.cleartrip.core.model.PassangerDetail;
import com.cleartrip.core.model.TicketDetails;
import com.cleartrip.core.pages.PageBase;
import com.cleartrip.core.utils.Helper;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HQValidators {

	private ObjectMapper objectMapper = new ObjectMapper();

	public void validateHQDetails(SoftAssert softAssert, HQResponse hqResponse, List<PassangerDetail> passangerDetails,
								  FareDetailResponse fareDetailResponse, BookingRequest bookingRequest, ItineraryResponse itineraryResponse) throws Exception {
		
//		PageBase.addLog("hqResponse : " + hqResponse);
//      PageBase.addLog("passangerDetails : " + passangerDetails);
//      PageBase.addLog("fareDetailResponse : " + fareDetailResponse);

		PageBase.addLog("Validating that hqResponse should not be null.");
		Assert.assertNotNull(hqResponse, "HQ Response cannot be null");

		PageBase.addLog("Validating that HQ Passanger Information should not be null.");
		Assert.assertNotNull(hqResponse.getPassangerDetails(), "HQ Passanger Information cannot be null");

		for (PassangerDetail passangerDetail : hqResponse.getPassangerDetails()) {
			Assert.assertNotNull(passangerDetail.getName(), "Passanger Name cannot be null");
		}

		PageBase.addLog("Validating ticketing details on HQ page.");
		Assert.assertTrue(hqResponse.getTickets().size() > 0 , "HQ Ticket Information cannot be zero");

		int k=0;
		for (TicketDetails ticketDetails : hqResponse.getTickets()) {
			if(k == 0) {
				PageBase.addLog("Airline PNR : "+ticketDetails.getAirlinePNR());
				PageBase.addTripIdLog("Airline PNR : "+ticketDetails.getAirlinePNR());
				PageBase.addLog("GDS PNR : "+ticketDetails.getGdsPNR());
				PageBase.addTripIdLog("GDS PNR : "+ticketDetails.getGdsPNR());
				++k;
			}
			softAssert.assertTrue(StringUtils.isNotBlank(ticketDetails.getAirlinePNR()), "Airline PNR cannot be null");
			softAssert.assertFalse(ticketDetails.getAirlinePNR().toLowerCase().contains("test"), "The word \"Test\" should not be there in Airline PNR.");
			softAssert.assertTrue(StringUtils.isNotBlank(ticketDetails.getPersonName()), "PersonName cannot be null");
			softAssert.assertTrue(StringUtils.isNotBlank(ticketDetails.getSector()), "Sector cannot be null");
			softAssert.assertTrue(StringUtils.isNotBlank(ticketDetails.getStatus()), "Status cannot be null");
			softAssert.assertTrue(StringUtils.isNotBlank(ticketDetails.getClazz()), "Clazz cannot be null");

			if (bookingRequest.isIntl()) {
				softAssert.assertTrue(StringUtils.isNotBlank(ticketDetails.getTicket()), "Ticket cannot be null");
			}
		}

		String hqresp = objectMapper.writeValueAsString(hqResponse.getPassangerDetails()).replaceAll("--", "");
		String Passengerresp = objectMapper.writeValueAsString(passangerDetails).replaceAll("null", "");

		PageBase.addLog("Validating passengers details on HQ based on entered data on traveller/itinerary page.");
//		PageBase.addLog("hqresp: " + hqresp + "  ===== Passengerresp: " + Passengerresp);

		JSONAssert.assertEquals(hqresp, Passengerresp, JSONCompareMode.LENIENT);
//		PageBase.addLog("hqResponse : " + hqResponse);
		PageBase.addLog("Validating Pax count on HQ page.");
		softAssert.assertEquals(hqResponse.getAdultCount(),fareDetailResponse.getAdultCount(), "Adult count mismatch on HQ page. Expected: "+fareDetailResponse.getAdultCount()+" Actual: "+hqResponse.getAdultCount());
		softAssert.assertEquals(hqResponse.getChildCount(),fareDetailResponse.getChildCount(), "Child count mismatch on HQ page. Expected: "+fareDetailResponse.getChildCount()+" Actual: "+hqResponse.getChildCount());
		softAssert.assertEquals(hqResponse.getInfantCount(),fareDetailResponse.getInfantCount(), "Infant count mismatch on HQ page. Expected: "+fareDetailResponse.getInfantCount()+" Actual: "+hqResponse.getInfantCount());
		
		PageBase.addLog("Validating Pax base fare on HQ page.");
		softAssert.assertEquals(Helper.roundAvoid(fareDetailResponse.getBaseFareAdult(),0),Helper.roundAvoid(hqResponse.getAdultAmount(), 0), "Adult base fare mismatch on HQ page. Expected: "+fareDetailResponse.getBaseFareAdult()+" Actual: "+hqResponse.getAdultAmount());
		softAssert.assertEquals(Helper.roundAvoid(fareDetailResponse.getBaseFareChild(),0),Helper.roundAvoid(hqResponse.getChildAmount(), 0), "Child base fare mismatch on HQ page. Expected: "+fareDetailResponse.getBaseFareChild()+" Actual: "+hqResponse.getChildAmount());
		softAssert.assertEquals(Helper.roundAvoid(fareDetailResponse.getBaseFareInfant(),0),Helper.roundAvoid(hqResponse.getInfantAmount(), 0), "Infant base fare mismatch on HQ page. Expected: "+fareDetailResponse.getBaseFareInfant()+" Actual: "+hqResponse.getInfantAmount());
		
		PageBase.addLog("Validating total tax i.e other charges and GST airline amount.");
		double totalTaxDiff=fareDetailResponse.getTotalTax() - hqResponse.getTotalTax();
		softAssert.assertTrue((totalTaxDiff < 3 && totalTaxDiff > -3), "Total tax mismatch on HQ page. Expected: "+fareDetailResponse.getTotalTax()+" <= Actual: "+hqResponse.getTotalTax());
		
		PageBase.addLog("Validating insurace fee on HQ page.");
		softAssert.assertEquals(Helper.roundAvoid(fareDetailResponse.getInsuranceValue(),0), Helper.roundAvoid(hqResponse.getInsurance(), 0), "Insurance fee mismatch on HQ page. Expected: "+fareDetailResponse.getInsuranceValue()+" Actual: "+hqResponse.getInsurance());
		
		PageBase.addLog("Validating meal fee on HQ page.");
		softAssert.assertEquals(Helper.roundAvoid(itineraryResponse.getMealFare(),0), Helper.roundAvoid(hqResponse.getMeal(), 0), "Meal fee mismatch on HQ page. Expected: "+itineraryResponse.getMealFare()+" Actual: "+hqResponse.getMeal());
		
		PageBase.addLog("Validating baggage fee on HQ page.");
		softAssert.assertEquals(Helper.roundAvoid(itineraryResponse.getBaggageFare(),0), Helper.roundAvoid(hqResponse.getBaggage(), 0), "Baggage fee mismatch on HQ page. Expected: "+itineraryResponse.getBaggageFare()+" Actual: "+hqResponse.getBaggage());

		PageBase.addLog("Validating Meal and Baggage fee all together on HQ page.");
		softAssert.assertEquals(Helper.roundAvoid(fareDetailResponse.getMealsAndBaggage_fare(),0), (Helper.roundAvoid(hqResponse.getMeal(), 0) + Helper.roundAvoid(hqResponse.getBaggage(), 0)), "Meal and Baggage fee all together mismatch on HQ page. Expected: "+fareDetailResponse.getMealsAndBaggage_fare()+" Actual: "+(Helper.roundAvoid(hqResponse.getMeal(), 0) + Helper.roundAvoid(hqResponse.getBaggage(), 0)));
		
		PageBase.addLog("Validating seat fee on HQ page.");
		softAssert.assertEquals(Helper.roundAvoid(fareDetailResponse.getSeat_fare(),0), Helper.roundAvoid(hqResponse.getSeat(), 0), "Seat fee mismatch on HQ page. Expected: "+fareDetailResponse.getSeat_fare()+" Actual: "+hqResponse.getSeat());
		
		PageBase.addLog("Validating coupon discount value on HQ page.");
		softAssert.assertEquals(Helper.roundAvoid(fareDetailResponse.getCouponDiscountValue(),0), Helper.roundAvoid(hqResponse.getDiscount(), 0), "Coupon discount fee mismatch on HQ page. Expected: "+fareDetailResponse.getCouponDiscountValue()+" Actual: "+hqResponse.getDiscount());
		
		PageBase.addLog("Validating convience fee on HQ page.");
		double convenienceFeeDiff=fareDetailResponse.getTotalTax() - hqResponse.getTotalTax();
		softAssert.assertTrue((convenienceFeeDiff < 10 && convenienceFeeDiff > -10), "Convenience fee mismatch on HQ page. Expected: "+fareDetailResponse.getConvenienceFee()+" <= Actual: "+hqResponse.getConvenienceFee());
		
		PageBase.addLog("Validating total fare on HQ page.");
		softAssert.assertTrue(Helper.roundAvoid(fareDetailResponse.getTotalAmount(),0) <= Helper.roundAvoid(hqResponse.getTotal(),0), "Total fare should be same. Expected: "+fareDetailResponse.getTotalAmount()+" <= Actual: "+hqResponse.getTotal());
		
		PageBase.addLog("Validating number of passengers on HQ page.");
		softAssert.assertTrue(hqResponse.getPassangerDetails().size() == passangerDetails.size(),
				"HQ Passanger Information should be same");
	}
}
