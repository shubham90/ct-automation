package com.cleartrip.core.validators;

import org.apache.commons.lang.StringUtils;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.ItineraryResponse;

public class SSRValidators {
	
	public void validateSSR(BookingRequest bookingRequest, ItineraryResponse itineraryResponse, SoftAssert softAssert){
		SoftAssert soft_assert=new SoftAssert();
        if (bookingRequest.isBaggageRequested()) {
            String baggageFare="";
            if (itineraryResponse.getBaggageFare() > 0.0){
                baggageFare=itineraryResponse.getBaggageFare().toString();
            }
            soft_assert.assertFalse(StringUtils.isBlank(baggageFare),"Baggage fare can not be null or blank, Since isBaggageRequested is true. actual: "+itineraryResponse.getBaggageFare());
        }
        if (bookingRequest.isMealRequested()) {
            String mealFare="";
            if (itineraryResponse.getMealFare() > 0.0){
                mealFare=itineraryResponse.getMealFare().toString();
            }
            soft_assert.assertFalse(StringUtils.isBlank(mealFare),"Meal fare can not be null or blank, Since isMealRequested is true. Actual: "+itineraryResponse.getMealFare());
        }
        if (bookingRequest.isSeatRequested()) {
            String seatFare="";
            if (itineraryResponse.getSeatFare() > 0.0){
                seatFare=itineraryResponse.getSeatFare().toString();
            }
            softAssert.assertFalse(StringUtils.isBlank(seatFare),"Seat selection fare can not be null or blank, Since isSeatRequested is true. Actual: "+itineraryResponse.getSeatFare());
        }
        
        soft_assert.assertAll();
    }

}
