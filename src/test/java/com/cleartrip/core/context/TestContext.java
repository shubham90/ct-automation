package com.cleartrip.core.context;

import java.util.ArrayList;
import java.util.List;

import com.cleartrip.core.validators.connector.IndigoValidator;

import org.apache.commons.lang.StringUtils;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.manager.ConfigurationManager;
import com.cleartrip.core.manager.PageObjectManager;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.ConnectorResponse;
import com.cleartrip.core.model.FareDetailResponse;
import com.cleartrip.core.model.FlightRecordInfo;
import com.cleartrip.core.model.HQResponse;
import com.cleartrip.core.model.ItineraryResponse;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.PassangerDetail;
import com.cleartrip.core.model.PaymentResponse;
import com.cleartrip.core.model.SRPResponse;
import com.cleartrip.core.model.StatsResponse;
import com.cleartrip.core.model.TripResponse;
import com.cleartrip.core.model.TripXMLResponse;
import com.cleartrip.core.service.ConnectorService;
import com.cleartrip.core.service.StatsService;
import com.cleartrip.core.service.TripXMLService;
import com.cleartrip.core.validators.FareDetailsValidators;
import com.cleartrip.core.validators.HQValidators;
import com.cleartrip.core.validators.ItineraryPageValidators;
import com.cleartrip.core.validators.SRPValidators;
import com.cleartrip.core.validators.SSRValidators;

public class TestContext {

	private PageObjectManager pageObjectManager;
	private ConfigurationManager configurationManager;
	private TripXMLService tripXMLService;
	private StatsService statsService;
	private SSRValidators ssrValidators;
	private HQValidators hqValidators;
	private FareDetailsValidators fareDetailsValidators;

	public StatsService getStatsService() {
		return statsService;
	}

	public TripXMLService getTripXMLService() {
		return tripXMLService;
	}

	public HQValidators getHqValidators() {
		return hqValidators;
	}

	public void setHqValidators(HQValidators hqValidators) {
		this.hqValidators = hqValidators;
	}

	public FareDetailsValidators getFareDetailsValidators() {
		return fareDetailsValidators;
	}

	public void setFareDetailsValidators(FareDetailsValidators fareDetailsValidators) {
		this.fareDetailsValidators = fareDetailsValidators;
	}

	public ItineraryPageValidators getItineraryPageValidators() {
		return itineraryPageValidators;
	}

	public void setItineraryPageValidators(ItineraryPageValidators itineraryPageValidators) {
		this.itineraryPageValidators = itineraryPageValidators;
	}

	public SRPValidators getSrpValidators() {
		return srpValidators;
	}

	public void setSrpValidators(SRPValidators srpValidators) {
		this.srpValidators = srpValidators;
	}

	private ItineraryPageValidators itineraryPageValidators;
	private SRPValidators srpValidators;
	private IndigoValidator indigoValidator;

	public ConnectorService getConnectorService() {
		return connectorService;
	}

	public void setConnectorService(ConnectorService connectorService) {
		this.connectorService = connectorService;
	}

	private ConnectorService connectorService;

	public SSRValidators getSsrValidators() {
		return ssrValidators;
	}

	public void setSsrValidators(SSRValidators ssrValidators) {
		this.ssrValidators = ssrValidators;

	}

	public IndigoValidator getIndigoValidator() {
		return indigoValidator;
	}

	public TestContext() throws Exception {
		configurationManager = new ConfigurationManager("test.properties");

		pageObjectManager = new PageObjectManager(Long.valueOf(configurationManager.getWait()),
				configurationManager.getHomePageURL("qa2.b2c.com.home.page.url"), configurationManager.getHQURL());
		statsService = new StatsService(configurationManager.getUserName(), configurationManager.getPassword(),
				configurationManager.getStatsURL());
		tripXMLService = new TripXMLService(configurationManager.getUserName(), configurationManager.getPassword(),
				configurationManager.getTripXMLURL());
		fareDetailsValidators = new FareDetailsValidators();
		srpValidators = new SRPValidators();
		itineraryPageValidators = new ItineraryPageValidators();
		connectorService = new ConnectorService(configurationManager.getUserName(), configurationManager.getPassword(),
				configurationManager.getConnectorURL());
		ssrValidators = new SSRValidators();
		hqValidators = new HQValidators();
		indigoValidator = new IndigoValidator();
	}

	public void setURLDomainEnv(String channel, String domain, String environment) throws Exception {

		String requestedUrlKey = "";
		if (StringUtils.isEmpty(environment) || environment.equalsIgnoreCase("qa2")) {
			if (StringUtils.isEmpty(channel) || channel.equalsIgnoreCase("b2c")) {
				if (StringUtils.isEmpty(domain) || domain.equalsIgnoreCase("com")) {
					requestedUrlKey = "qa2.b2c.com.home.page.url";
				} else if (domain.equalsIgnoreCase("ae")) {
					requestedUrlKey = "qa2.b2c.ae.home.page.url";
				} else if (domain.equalsIgnoreCase("sa")) {
					requestedUrlKey = "qa2.b2c.sa.home.page.url";
				} else if (domain.equalsIgnoreCase("om")) {
					requestedUrlKey = "b2c.om.home.page.url";
				} else if (domain.equalsIgnoreCase("bh")) {
					requestedUrlKey = "b2c.bh.home.page.url";
				} else if (domain.equalsIgnoreCase("kw")) {
					requestedUrlKey = "b2c.kw.home.page.url";
				} else if (domain.equalsIgnoreCase("qa")) {
					requestedUrlKey = "b2c.qa.home.page.url";
				} else if (domain.equalsIgnoreCase("me")) {
					requestedUrlKey = "b2c.me.home.page.url";
				}
			} else if (channel.equalsIgnoreCase("agency")) {
				requestedUrlKey = "qa2.agency.com.home.page.url";
			} else if (channel.equalsIgnoreCase("corp")) {
				if (domain.equalsIgnoreCase("com")) {
					requestedUrlKey = "qa2.corp.com.home.page.url";
				} else if (domain.equalsIgnoreCase("ae")) {
					requestedUrlKey = "qa2.corp.ae.home.page.url";
				}
			} else if (channel.equalsIgnoreCase("amex")) {
				requestedUrlKey = "qa2.amex.com.home.page.url";
			} else if (channel.equalsIgnoreCase("flightxp")) {
				requestedUrlKey = "qa2.flightxp.com.home.page.url";
			}
		} else if (environment.equalsIgnoreCase("prod")) {
			if (StringUtils.isEmpty(channel) || channel.equalsIgnoreCase("b2c")) {
				if (StringUtils.isEmpty(domain) || domain.equalsIgnoreCase("com")) {
					requestedUrlKey = "prod.b2c.com.home.page.url";
				} else if (domain.equalsIgnoreCase("ae")) {
					requestedUrlKey = "prod.b2c.ae.home.page.url";
				} else if (domain.equalsIgnoreCase("sa")) {
					requestedUrlKey = "prod.b2c.sa.home.page.url";
				} else if (domain.equalsIgnoreCase("om")) {
					requestedUrlKey = "b2c.om.home.page.url";
				} else if (domain.equalsIgnoreCase("bh")) {
					requestedUrlKey = "b2c.bh.home.page.url";
				} else if (domain.equalsIgnoreCase("kw")) {
					requestedUrlKey = "b2c.kw.home.page.url";
				} else if (domain.equalsIgnoreCase("qa")) {
					requestedUrlKey = "b2c.qa.home.page.url";
				} else if (domain.equalsIgnoreCase("me")) {
					requestedUrlKey = "b2c.me.home.page.url";
				}
			} else if (channel.equalsIgnoreCase("agency")) {
				requestedUrlKey = "prod.agency.com.home.page.url";
			} else if (channel.equalsIgnoreCase("corp")) {
				if (domain.equalsIgnoreCase("com")) {
					requestedUrlKey = "prod.corp.com.home.page.url";
				} else if (domain.equalsIgnoreCase("ae")) {
					requestedUrlKey = "prod.corp.ae.home.page.url";
				}
			} else if (channel.equalsIgnoreCase("amex")) {
				requestedUrlKey = "prod.amex.com.home.page.url";
			} else if (channel.equalsIgnoreCase("flightxp")) {
				requestedUrlKey = "prod.flightxp.com.home.page.url";
			}
		}

		pageObjectManager = new PageObjectManager(Long.valueOf(configurationManager.getWait()),
				configurationManager.getHomePageURL(requestedUrlKey), configurationManager.getHQURL());
	}

	public PageObjectManager getPageObjectManager() {
		return pageObjectManager;
	}

	public ConfigurationManager getConfigurationManager() {
		return configurationManager;
	}

}
