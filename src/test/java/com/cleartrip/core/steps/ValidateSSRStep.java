package com.cleartrip.core.steps;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.ItineraryResponse;
import cucumber.api.java.en.Then;
import org.apache.commons.lang.StringUtils;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class ValidateSSRStep {

    private TestContext testContext = null;

    public ValidateSSRStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Validate SSR")
    public void validateSSR() {
        BookingRequest bookingRequest = testContext.getPageObjectManager().getBookingRequest();
        ItineraryResponse itineraryResponse = testContext.getPageObjectManager().getItineraryResponse();
        SoftAssert softAssert=testContext.getPageObjectManager().getSoftAssert();
        testContext.getSsrValidators().validateSSR(bookingRequest,itineraryResponse,softAssert);
    }
}
