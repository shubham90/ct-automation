package com.cleartrip.core.steps;

import org.openqa.selenium.WebElement;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.PaymentResponse;
import com.cleartrip.core.pages.IntermediatePage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PaymentStep extends BaseSteps{

    private TestContext testContext = null;

    public PaymentStep(TestContext testContext) {
        this.testContext = testContext;
    }


    @Then("Payment")
    public void payment() throws Exception {
        int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
        MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
        addLog("Entering payment details");
        PaymentResponse paymentResponse = testContext.getPageObjectManager().getPaymentPage().book(
                testContext.getConfigurationManager().getCardNumber(),
                testContext.getConfigurationManager().getCardExpiryMonth(),
                testContext.getConfigurationManager().getCardExpiryYear(),
                testContext.getConfigurationManager().getCardHolder(),
                testContext.getConfigurationManager().getCardCVV(), wait,miscellaneousResponse);
        testContext.getPageObjectManager().setPaymentResponse(paymentResponse);
    }

    @When("Click Payment")
    public void clickPayment() throws Exception {
    	if(!testContext.getPageObjectManager().getDriver().getCurrentUrl().contains("www")){
    		addLog("Submitting payment details");
    		IntermediatePage intermediatePage=testContext.getPageObjectManager().getIntermediatePage();
    		MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
        	WebElement element_itinerary = testContext.getPageObjectManager().getItineraryResponse().getItineraryBtnElement();
        	WebElement element_payment=testContext.getPageObjectManager().getPaymentResponse().getSubmitButton();
        	testContext.getPageObjectManager().getPaymentPage().clickOnMakePaymentButton(intermediatePage, miscellaneousResponse, element_itinerary,element_payment);
    	}
    }
}
