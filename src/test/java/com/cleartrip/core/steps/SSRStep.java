package com.cleartrip.core.steps;

import org.apache.commons.lang.StringUtils;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.ItineraryResponse;

import cucumber.api.java.en.Then;

public class SSRStep extends BaseSteps{

	private TestContext testContext = null;

	public SSRStep(TestContext testContext) {
		this.testContext = testContext;
	}

	@Then("SELECT SSR \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"")
	public void selectSSR(String seat, String meal, String baggage, String insurance) throws Exception {
		addLog("Selecting SSR>> Seat: "+seat+", Meal: "+meal+", Baggage: "+baggage+", Insurance: "+insurance);
		addTripIdLog("Selecting SSR>> Seat: "+seat+", Meal: "+meal+", Baggage: "+baggage+", Insurance: "+insurance);
		int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
		ItineraryResponse itineraryResponse = testContext.getPageObjectManager().getItineraryResponse();
		BookingRequest bookingRequest=testContext.getPageObjectManager().getBookingRequest();
		SoftAssert softassert=testContext.getPageObjectManager().getSoftAssert();

		if (StringUtils.isNotBlank(meal) && meal.equalsIgnoreCase("True")) {
			testContext.getPageObjectManager().getSSRPage().checkMealSSRAvailability();
		}
		
		if (StringUtils.isNotBlank(baggage) && baggage.equalsIgnoreCase("True")) {
			testContext.getPageObjectManager().getSSRPage().checkBaggageSSRAvailability();
		}
		
		if (StringUtils.isNotBlank(seat) && seat.equalsIgnoreCase("True")) {
			testContext.getPageObjectManager().getSSRPage().checkSeatSSRAvailability();
		}
		
		boolean insuranceRequired=insurance.equalsIgnoreCase("true");
		testContext.getPageObjectManager().getBookingRequest().setInsuranceSelected(insuranceRequired);
		testContext.getPageObjectManager().getSSRPage().checkInsuranceBox(insuranceRequired, itineraryResponse, wait,softassert);

		if (StringUtils.isNotBlank(meal) && meal.equalsIgnoreCase("True")) {
			testContext.getPageObjectManager().getSSRPage().selectMeal(wait, itineraryResponse,bookingRequest);
			testContext.getPageObjectManager().getBookingRequest().setMealRequested(true);
		}
		if (StringUtils.isNotBlank(baggage) && baggage.equalsIgnoreCase("True")) {
			testContext.getPageObjectManager().getSSRPage().selectBaggage(wait, itineraryResponse,bookingRequest);
			testContext.getPageObjectManager().getBookingRequest().setBaggageRequested(true);
		}

		if (StringUtils.isNotBlank(seat) && seat.equalsIgnoreCase("True")) {
			testContext.getPageObjectManager().getSSRPage().selectSeat(wait, itineraryResponse);
			testContext.getPageObjectManager().getBookingRequest().setSeatRequested(true);
		}
	}
}
