package com.cleartrip.core.steps;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.PassangerDetail;
import com.cleartrip.core.model.TravellerDetail;
import com.cleartrip.core.pages.IntermediatePage;

import cucumber.api.java.en.And;

public class TravellersPageStep extends BaseSteps{

    private TestContext testContext = null;

    public TravellersPageStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @And("Update Traveller")
    public void updateTraveller() throws Exception {
    	addLog("Updating Traveller");
        int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
        List<TravellerDetail> travellerDetailsAdult = testContext.getConfigurationManager().getTravellerDetailsAdult();
        List<TravellerDetail> travellerDetailsInfant = testContext.getConfigurationManager().getTravellerDetailsInfant();
        List<TravellerDetail> travellerDetailsChild = testContext.getConfigurationManager().getTravellerDetailsChild();
        MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();

        List<PassangerDetail> passangerDetails = testContext.getPageObjectManager().getTravellerPage()
                .submit(testContext.getConfigurationManager().getMobileNumber(),
                        testContext.getPageObjectManager().getBookingRequest().getAdults(),
                        testContext.getPageObjectManager().getBookingRequest().getChilds(),
                        testContext.getPageObjectManager().getBookingRequest().getInfants(), wait,
                        travellerDetailsAdult, travellerDetailsChild, travellerDetailsInfant,miscellaneousResponse);
        testContext.getPageObjectManager().setPassangerDetails(passangerDetails);
        
        IntermediatePage intermediatePage=testContext.getPageObjectManager().getIntermediatePage();
    	WebElement element_itinerary = testContext.getPageObjectManager().getItineraryResponse().getItineraryBtnElement();
        testContext.getPageObjectManager().getTravellerPage().clickOnTravellerContinueButton(intermediatePage, miscellaneousResponse,element_itinerary);
    }
}
