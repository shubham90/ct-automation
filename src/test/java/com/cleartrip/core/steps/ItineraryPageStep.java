package com.cleartrip.core.steps;

import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.ItineraryResponse;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.pages.IntermediatePage;

import cucumber.api.java.en.When;

public class ItineraryPageStep extends BaseSteps{

    private TestContext testContext = null;

    public ItineraryPageStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @When("^SELECT Itinarary")
    public void selectItinary() throws Exception {
    	//addLog("Doing Itinerary Selection.");
    	SoftAssert softAssert = testContext.getPageObjectManager().getSoftAssert();
        int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
        ItineraryResponse itineraryResponse = testContext.getPageObjectManager().getItineraryPage().continueBooking(wait,softAssert);
        
        String url = testContext.getPageObjectManager().getDriver().getCurrentUrl();
        String itineraryId = url.substring(url.indexOf("itinerary")+10, url.lastIndexOf("/"));
        itineraryResponse.setItinaryId(itineraryId);
        testContext.getPageObjectManager().setItineraryResponse(itineraryResponse);
        addLog("Itinerary Id : "+itineraryId);
        addTripIdLog("Itinerary Id : "+itineraryId);
    }

    @When("^Click Book on Itinarary")
    public void continueFromItinarary( ) throws Exception {
    	addLog("Clicking Book button in SRP page");
    	IntermediatePage intermediatePage=testContext.getPageObjectManager().getIntermediatePage();
    	MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
    	WebElement element = testContext.getPageObjectManager().getItineraryResponse().getItineraryBtnElement();
        testContext.getPageObjectManager().getItineraryPage().clickOnItineraryContinueButton(intermediatePage,miscellaneousResponse,element);
    }
}
