package com.cleartrip.core.steps;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.ConnectorResponse;
import com.cleartrip.core.pages.PageBase;

import cucumber.api.java.en.Then;
import org.apache.log4j.Logger;
import org.testng.Assert;

public class ValidateConnectorStep extends BaseSteps{

    private final Logger log = org.apache.log4j.Logger.getLogger(ValidateConnectorStep.class);

    private TestContext testContext = null;

    public ValidateConnectorStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Validate Connector Result")
    public void validateConnectorResult() {
    	addLog("Validating Connector Search Result");
        ConnectorResponse connectorResponse = testContext.getPageObjectManager().getConnectorResponse();
//        Assert.assertNotNull(connectorResponse, "Connector Response cannot be null");
//        Assert.assertTrue(connectorResponse.isSuccess(),
//                "Avaliable Result cannot be false");

    }
}
