package com.cleartrip.core.steps;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.TripResponse;

import cucumber.api.java.en.Then;

public class ConfirmationPageStep extends BaseSteps{

    private TestContext testContext = null;

    public ConfirmationPageStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Confirmation Page")
    public void confirmationPage() throws Exception {
    	addLog("In Confirmation Page");
    	String itineraryId=testContext.getPageObjectManager().getItineraryResponse().getItinaryId();
    	MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
        TripResponse tripResponse = testContext.getPageObjectManager().getConfirmationPage().confirm(itineraryId,miscellaneousResponse);
        testContext.getPageObjectManager().setTripResponse(tripResponse);
    }

}
