package com.cleartrip.core.steps;

import java.util.List;

import com.cleartrip.core.model.BookingRequest;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.FareDetailResponse;
import com.cleartrip.core.model.HQResponse;
import com.cleartrip.core.model.ItineraryResponse;
import com.cleartrip.core.model.PassangerDetail;

import cucumber.api.java.en.Then;

public class ValidateHQStep extends BaseSteps{

    private TestContext testContext = null;

    public ValidateHQStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Validate HQ STEP")
    public void validateHQ() throws Exception {
    	addLog("Validating HQ Details");
        HQResponse hqResponse = testContext.getPageObjectManager().getHqResponse();
        List<PassangerDetail> passangerDetails = testContext.getPageObjectManager().getPassangerDetails();
        BookingRequest bookingRequest = testContext.getPageObjectManager().getBookingRequest();

        FareDetailResponse fareDetailResponse = testContext.getPageObjectManager().getFareDetailResponse();
        SoftAssert softAssert=testContext.getPageObjectManager().getSoftAssert();
        ItineraryResponse itineraryResponse=testContext.getPageObjectManager().getItineraryResponse();

        testContext.getHqValidators().validateHQDetails(softAssert, hqResponse, passangerDetails,
                fareDetailResponse, bookingRequest,itineraryResponse);
    }
}
