package com.cleartrip.core.steps;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.MiscellaneousResponse;

import cucumber.api.java.en.Then;

public class IntermediatePopUpStep extends BaseSteps {


    private TestContext testContext = null;

    public IntermediatePopUpStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Flight Change Popup")
    public void flightChangePopup()  throws Exception {
    	addLog("Handling Flight Change Popup");
        testContext.getPageObjectManager().getIntermediatePage().validateFlightChangePopUpAndMove();
    }

    @Then("Check Login Page")
    public void checkLoginPage()  throws Exception {
    	addLog("Verifying Login Page");
    	MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
        testContext.getPageObjectManager().getIntermediatePage()
                .loginVerification(testContext.getConfigurationManager().getUserName(),miscellaneousResponse);
    }

    @Then("Flight Price Change Popup")
    public void flightPriceChangePopup()  throws Exception {
    	addLog("Handling Flight Price Change Popup");
    	MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
        testContext.getPageObjectManager().getIntermediatePage().flightPriceChangeAlertHandling(miscellaneousResponse);
    }

    @Then("Domain Handling")
    public void domainHandling()  throws Exception {
    	addLog("Performing Domain Handling");
        testContext.getPageObjectManager().getIntermediatePage().domainHandling();
    }
}
