package com.cleartrip.core.steps;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebElement;

import com.cleartrip.core.constants.BookingType;
import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.SRPResponse;
import com.cleartrip.core.pages.IntermediatePage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SRPPageStep extends BaseSteps{

    private TestContext testContext = null;
    
    public SRPPageStep(TestContext testContext) {
        this.testContext = testContext;
    }


    @Then("^SELECT CONNECTED FLIGHT \"([^\"]*)\" OW CONFIG \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" RT CONFIG \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" nearby \"([^\"]*)\" isSpecialRT \"([^\"]*)\"$")
    public SRPResponse selectFlight(String connected, String owSupplier,
    		String owAirline,String owConnected, 
    		String rtSupplier, String rtAirline, String rtConnected, String isNearbyFiltring,String isSpecialRT) throws Exception {
    	
    	addLog("Selected Connected Flight : "+connected+" , onwards Supplier : "+owSupplier+" , onwards airline : "+owAirline+" , onwards 2nd airline : "+owConnected+" , return supplier : "+rtSupplier+" , return airline : "+rtAirline+" , return 2nd airline : "+rtConnected+" , nearby required : "+isNearbyFiltring+" , isSplecialRT : "+isSpecialRT);
    	addTripIdLog("Selected Connected Flight : "+connected+" , onwards Supplier : "+owSupplier+" , onwards airline : "+owAirline+" , onwards 2nd airline : "+owConnected+" , return supplier : "+rtSupplier+" , return airline : "+rtAirline+" , return 2nd airline : "+rtConnected+" , nearby required : "+isNearbyFiltring+" , isSplecialRT : "+isSpecialRT);
        boolean isConnected = StringUtils.isNotEmpty(connected) && connected.equalsIgnoreCase("true") ? true: false;
        boolean isSplRT = StringUtils.isNotEmpty(isSpecialRT) && isSpecialRT.equalsIgnoreCase("true") ? true: false;
        int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
        BookingType bookingType = testContext.getPageObjectManager().getBookingRequest().getBookingType();
        
        SRPResponse srpResponse = testContext.getPageObjectManager().getSRPPage().book(wait, 
        		testContext.getPageObjectManager().getBookingRequest().isSplitRT(), isConnected,
                owSupplier, owAirline, rtSupplier, rtAirline, owConnected, rtConnected, bookingType, isNearbyFiltring,
                testContext.getPageObjectManager().getBookingRequest(), testContext.getConfigurationManager().isNoMealFeatureTesting(),
                testContext.getConfigurationManager().isNoMealTestCase(),isSplRT);
        testContext.getPageObjectManager().setSrpResponse(srpResponse);
        MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
        miscellaneousResponse.setSpecialRT(isSplRT);
        miscellaneousResponse.setOwSupplier(owSupplier);
        miscellaneousResponse.setRtSupplier(rtSupplier);
        return srpResponse;
    }
    
    
    @Then("^SELECT WL FLIGHT \"([^\"]*)\" OW CONFIG \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" RT CONFIG \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" nearby \"([^\"]*)\" isSpecialRT \"([^\"]*)\"$")
    public SRPResponse selectFlight1(String connected, String owSupplier,
    		String owAirline,String owConnected, 
    		String rtSupplier, String rtAirline, String rtConnected, String isNearbyFiltring,String isSpecialRT) throws Exception {
    	addLog("Selected Connected Flight"+connected+","+owSupplier+","+owAirline+","+owConnected+","+rtSupplier+","+rtSupplier+","+
    		rtAirline+","+rtConnected+","+isNearbyFiltring+","+isSpecialRT);
        boolean isConnected = StringUtils.isNotEmpty(connected) && connected.equalsIgnoreCase("true") ? true: false;
        boolean isSplRT = StringUtils.isNotEmpty(isSpecialRT) && isSpecialRT.equalsIgnoreCase("true") ? true: false;
        int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
        BookingType bookingType = testContext.getPageObjectManager().getBookingRequest().getBookingType();
        
        SRPResponse srpResponse = testContext.getPageObjectManager().getSRPPage().book(wait, 
        		testContext.getPageObjectManager().getBookingRequest().isSplitRT(), isConnected,
                owSupplier, owAirline, rtSupplier, rtAirline, owConnected, rtConnected, bookingType, isNearbyFiltring,
                testContext.getPageObjectManager().getBookingRequest(), testContext.getConfigurationManager().isNoMealFeatureTesting(),
                testContext.getConfigurationManager().isNoMealTestCase(),isSplRT);
        testContext.getPageObjectManager().setSrpResponse(srpResponse);
        return srpResponse;
    }
    

    @When("^Click Book on SRP")
    public void selectFlightOnSrp() throws Exception {
    	addLog("Clicking Book button in SRP page");
    	IntermediatePage intermediatePage=testContext.getPageObjectManager().getIntermediatePage();
    	MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
    	WebElement element = testContext.getPageObjectManager().getSrpResponse().getSelectionBookElement();
        testContext.getPageObjectManager().getSRPPage().clickOnBookButton(intermediatePage,miscellaneousResponse,element);
    }

}
