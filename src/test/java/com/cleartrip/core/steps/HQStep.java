package com.cleartrip.core.steps;

import org.testng.asserts.SoftAssert;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.HQResponse;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HQStep extends BaseSteps{

    private TestContext testContext = null;

    public HQStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @When("HQOpen")
    public void hqOpen()  throws Exception {
    	addLog("Opening HQ page");
        testContext.getPageObjectManager().getHQPage().open(testContext.getPageObjectManager().getTripResponse().getTripId());
    }

    @Then("HQLogin")
    public void hqLogin()  throws Exception {
    	addLog("Logging into HQ");
        int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
        if(testContext.getPageObjectManager().getDriver().getCurrentUrl().contains("signin")){
        testContext.getPageObjectManager().getHQPage().login(testContext.getConfigurationManager().getUserName(),
                testContext.getConfigurationManager().getPassword(), wait);
        }
    }

    @Then("Get Booking Information")
    public void getBookingInformation() throws Exception {
    	addLog("Retrieveing Booking Information Details From HQ");
        int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
        SoftAssert softAssert=testContext.getPageObjectManager().getSoftAssert();
        BookingRequest bookingRequest=testContext.getPageObjectManager().getBookingRequest();
        HQResponse hqResponse = testContext.getPageObjectManager().getHQPage().fetchDetails(wait, bookingRequest,softAssert);
        testContext.getPageObjectManager().setHqResponse(hqResponse);
    }
}
