package com.cleartrip.core.steps;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.pages.PageBase;

import cucumber.api.java.en.Then;
import org.apache.log4j.Logger;
import org.testng.Assert;

public class ValidateLoginPageStep extends BaseSteps{

    private final Logger log = org.apache.log4j.Logger.getLogger(ValidateLoginPageStep.class);

    private TestContext testContext = null;

    public ValidateLoginPageStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Validate Login")
    public void validateLoginPage() throws Exception {
    	addLog("Validating Login Details");
        String login = testContext.getPageObjectManager().getHomePage().fetchLoginDetails();
        PageBase.addLog("information passed from property : " + testContext.getConfigurationManager().getUserName());
        PageBase.addLog("information got after login : " + login);

        Assert.assertEquals(login, testContext.getConfigurationManager().getUserName(),"Login mismatch");
    }
}
