package com.cleartrip.core.steps;

import org.testng.asserts.SoftAssert;

import com.cleartrip.core.context.TestContext;

import cucumber.api.Scenario;
import cucumber.api.java.en.And;

public class AssertAll {
	
	private TestContext testContext = null;

    public AssertAll(TestContext testContext) {
        this.testContext = testContext;
    }

    @And("AssertAll")
    public void validateItineraryPage( ) throws Exception {
        SoftAssert softAssert = testContext.getPageObjectManager().getSoftAssert();
        softAssert.assertAll();
    }

}
