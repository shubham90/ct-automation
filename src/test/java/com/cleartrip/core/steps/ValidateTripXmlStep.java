package com.cleartrip.core.steps;

import org.apache.log4j.Logger;
import org.testng.Assert;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.TripXMLResponse;
import com.cleartrip.core.pages.PageBase;

import cucumber.api.java.en.Then;

public class ValidateTripXmlStep extends BaseSteps{

    private TestContext testContext = null;
    
    private final Logger log = org.apache.log4j.Logger.getLogger(ValidateTripXmlStep.class);

    public ValidateTripXmlStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Validate TripXML")
    public void validateStats() {
    	addLog("Validating TripXML Details");
        TripXMLResponse tripXMLResponse = testContext.getPageObjectManager().getTripXMLResponse();
        testContext.getPageObjectManager().getSoftAssert().assertNotNull(tripXMLResponse, "TripXML Response cannot be null");

        testContext.getPageObjectManager().getSoftAssert().assertTrue(tripXMLResponse.getIndividualBookingStatus().size() > 0,
                "Status cannot be "+tripXMLResponse.getIndividualBookingStatus().size()+" , Expected : >0");

        PageBase.addLog("Validating the presence of PNRs.");
        testContext.getPageObjectManager().getSoftAssert().assertTrue(tripXMLResponse.getPnrs().size() > 0,
                "Pnr cannot be "+tripXMLResponse.getPnrs().size()+" ,Expected : >0");
        
        PageBase.addLog("Validating overall booking status which should be P.");
        testContext.getPageObjectManager().getSoftAssert().assertTrue(tripXMLResponse.getOverAllBookingStatus().equalsIgnoreCase("P"),
                "OverAll status cannot be other than P, Actual : "+tripXMLResponse.getOverAllBookingStatus());
        
        PageBase.addLog("Validating individual booking status shouldnot be other than P.");
        testContext.getPageObjectManager().getSoftAssert().assertFalse(tripXMLResponse.getIndividualBookingStatus().contains("H"),
                "All individual booking status cannot be other than P, Currently it is containing H.");

        testContext.getPageObjectManager().getSoftAssert().assertFalse(tripXMLResponse.getIndividualBookingStatus().contains("Z"),
                "All individual booking status cannot be other than P, Currently it is containing Z.");

    }

}
