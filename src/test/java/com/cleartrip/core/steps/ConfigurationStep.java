package com.cleartrip.core.steps;


import org.openqa.selenium.WebDriver;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.factory.WebDriverFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class ConfigurationStep extends BaseSteps {

    private TestContext testContext = null;

    public ConfigurationStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @When("^Open the browser$")
    public void init() throws Exception {
    	addLog("Opening the browser");
        String browser = testContext.getConfigurationManager().getBrowser();
        String wait = testContext.getConfigurationManager().getWait();
        testContext.getPageObjectManager().reset();

        WebDriver webDriver =  WebDriverFactory.getInstance().getWebDriver(browser, wait);
        testContext.getPageObjectManager().setDriver(webDriver);
        
        writeLoadTimeInFile("====================This load time is recorded at and on "+getCurrentDateTime("dd-MM-yyyy HH:mm:ss")+" ======================");
    }

    @And("^Close browser$")
    public void close()
    {
        addLog("Closing browsers");
        testContext.getPageObjectManager().getDriver().quit();
    }

}
