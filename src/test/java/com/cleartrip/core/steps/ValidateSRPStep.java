package com.cleartrip.core.steps;

import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.SRPResponse;
import org.testng.asserts.SoftAssert;
import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.FlightRecordInfo;
import cucumber.api.java.en.Then;

public class ValidateSRPStep extends BaseSteps{
	
	private TestContext testContext = null;

    public ValidateSRPStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Validate SRP")
    public void validateSRPPage() throws Exception {
    	addLog("Validating SRP Details");
        SRPResponse srpResponse = testContext.getPageObjectManager().getSrpResponse();
        BookingRequest bookingRequest = testContext.getPageObjectManager().getBookingRequest();
        SoftAssert softAssert = testContext.getPageObjectManager().getSoftAssert();
        testContext.getSrpValidators()
                    .validateSRPDetails(softAssert, bookingRequest, srpResponse);
    }

    @Then("Validate Pre SRP")
    public void validatePreSRPPage() throws Exception {
    	//addLog("Validating Pre-SRP Details");
        //testContext.getPageObjectManager().getSRPPage().
    }

}
