package com.cleartrip.core.steps;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.FareDetailResponse;
import com.cleartrip.core.model.ItineraryResponse;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.SRPResponse;

import cucumber.api.java.en.Then;
import org.testng.asserts.SoftAssert;

public class ValidateFareDetailStep extends BaseSteps{

    private TestContext testContext = null;

    public ValidateFareDetailStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Validate Fare Detail")
    public void validateFareDetailsPage() throws Exception {
    	addLog("Validating Fare Details");
        FareDetailResponse fareDetailResponse = testContext.getPageObjectManager().getFareDetailResponse();
        SoftAssert softAssert = testContext.getPageObjectManager().getSoftAssert();
        BookingRequest bookingRequest = testContext.getPageObjectManager().getBookingRequest();
        ItineraryResponse itineraryResponse = testContext.getPageObjectManager().getItineraryResponse();
        SRPResponse srpResponse = testContext.getPageObjectManager().getSrpResponse();
        MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();

        testContext.getFareDetailsValidators().validate(softAssert, bookingRequest,fareDetailResponse);
        testContext.getFareDetailsValidators().validatePaxCountAndBaseFare(softAssert,bookingRequest,srpResponse.getFareBreakupInfo(),itineraryResponse,fareDetailResponse);
        testContext.getFareDetailsValidators().validateSSRFare(softAssert,itineraryResponse,miscellaneousResponse,fareDetailResponse);
        testContext.getFareDetailsValidators().validateTotalFeeAndTotalTax(softAssert,srpResponse.getFareBreakupInfo(),fareDetailResponse);
        testContext.getFareDetailsValidators().validateMisc(softAssert,fareDetailResponse);
    }
}
