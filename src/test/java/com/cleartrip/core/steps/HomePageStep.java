package com.cleartrip.core.steps;

import org.apache.commons.lang.StringUtils;
import org.testng.Assert;

import com.cleartrip.core.constants.BookingType;
import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.MiscellaneousResponse;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageStep extends BaseSteps{

    private TestContext testContext = null;
  
    public HomePageStep(TestContext testContext){
        this.testContext = testContext;
    }

    @And("Open HomePage for Channel \"([^\"]*)\" Domain \"([^\"]*)\" On \"([^\"]*)\"")
    public void openHomePage(String channel,String domain,String environment)  throws Exception {
    	MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
    	miscellaneousResponse.setChannel(channel);
    	miscellaneousResponse.setDomain(domain);
    	miscellaneousResponse.setEnvironment(environment);
        testContext.setURLDomainEnv(channel,domain,environment);
    	testContext.getPageObjectManager().getHomePage().open(miscellaneousResponse);
    }

    @Then("Login")
    public void login()  throws Exception {
        int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());

        testContext.getPageObjectManager().getHomePage().login(testContext.getConfigurationManager().getUserName(),
                testContext.getConfigurationManager().getPassword(), wait);
    }

    @When("^Search from \"([^\"]*)\" to \"([^\"]*)\" date \"([^\"]*)\" adult (\\d+) " +
            "child (\\d+) infant (\\d+) preferedAirLine \"([^\"]*)\" returnDate \"([^\"]*)\" isSplitRT \"([^\"]*)\" travelclass \"([^\"]*)\" isTicketingEnabled \"([^\"]*)\" isIntl \"([^\"]*)\"")
    public void search_OW_from_to_date_adult_child_infant(String from, String to, String departDateNumber, int adult,
                                                          int child, int infant, String preferedAirLine,
                                                          String returnDateNumber, String isSplitRT, String travelClass, String isTicketing,String intl) throws Exception {
    	int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
        String dateFormat="dd-MM-yyyy";

		String departDate = getDateAfterAddingGivenNumberOfDays(departDateNumber, dateFormat);
		
		int departYear=getYearFromGivenDate(departDate, dateFormat);
        int departMonth=getMonthNumberFromGivenDate(departDate,dateFormat);
        String departMonthName=getMonthNameOfGivenDate(departDate, dateFormat);
        int departDay=getDayNumberOfTheMonthOfGivenDate(departDate, dateFormat);
        int departWeekOfTheMonth=getWeekNumberOfTheMonthOfGivenDate(departDate, dateFormat);
        
        if (getDayNameOfGivenDate(departDate,dateFormat).equalsIgnoreCase("Sunday")) {
        	departWeekOfTheMonth=departWeekOfTheMonth-1;
		}
        
        int returnYear=-1;
        int returnMonth=-1;
        String returnMonthName="";
        int returnDay=-1;
        int returnWeekOfTheMonth=-1;
        
        BookingType bookingType = BookingType.ONE_WAY;
        String returnDate="";
        if (StringUtils.isNotBlank(returnDateNumber)) {
            bookingType = BookingType.ROUND_TRIP;
            returnDate = getDateAfterAddingGivenNumberOfDays(returnDateNumber, dateFormat);
			Assert.assertTrue(Integer.valueOf(departDateNumber.trim()) < Integer.valueOf(returnDateNumber.trim()), "The return date can not be less than depart date. Depart Date: "+departDate+" Return Date: "+returnDate);
        
			returnYear=getYearFromGivenDate(returnDate, dateFormat);
	        returnMonth=getMonthNumberFromGivenDate(returnDate, dateFormat);
	        returnMonthName=getMonthNameOfGivenDate(returnDate, dateFormat);
	        returnDay=getDayNumberOfTheMonthOfGivenDate(returnDate, dateFormat);
	        returnWeekOfTheMonth=getWeekNumberOfTheMonthOfGivenDate(returnDate, dateFormat);
	        
	        if (getDayNameOfGivenDate(returnDate,dateFormat).equalsIgnoreCase("Sunday")) {
	        	returnWeekOfTheMonth=returnWeekOfTheMonth-1;
			}
        }
        
        addLog("Doing Flight Search for From >>"+from+", To >>"+to+", From Date >>"+departDate+", To Date>>"+returnDate+", Pax>> "+adult+" ADT + "+child+" CHD + "+infant+" INF, preferedAirLine>> "+preferedAirLine+",isSplitScreen>> "+isSplitRT+", travelclass>>"+travelClass+", isTicketingEnabled>>"+isTicketing);
        addTripIdLog("Doing Flight Search for From >>"+from+", To >>"+to+", From Date >>"+departDate+", To Date>>"+returnDate+", Pax>> "+adult+" ADT + "+child+" CHD + "+infant+" INF, preferedAirLine>> "+preferedAirLine+",isSplitScreen>> "+isSplitRT+", travelclass>>"+travelClass+", isTicketingEnabled>>"+isTicketing);
        boolean splitRT = ("true".equalsIgnoreCase(isSplitRT.trim())) ? true : false;
		boolean isIntl = ("true".equalsIgnoreCase(intl.trim())) ? true : false;
        BookingRequest bookingRequest = new BookingRequest(adult, infant, child, from, to, departDate, splitRT , travelClass, bookingType, preferedAirLine, returnDate, Boolean.valueOf(isTicketing),departYear,departMonth,departMonthName,departDay,departWeekOfTheMonth,returnYear,returnMonth,returnMonthName,returnDay,returnWeekOfTheMonth,isIntl);
        testContext.getPageObjectManager().setBookingRequest(bookingRequest);
        testContext.getPageObjectManager().getHomePage().search(bookingRequest, wait);
    }

}
