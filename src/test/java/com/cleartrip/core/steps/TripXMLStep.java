package com.cleartrip.core.steps;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.TripXMLResponse;
import com.cleartrip.core.pages.PageBase;

import cucumber.api.java.en.When;

public class TripXMLStep extends BaseSteps{

    private TestContext testContext = null;

    public TripXMLStep(TestContext testContext) {
        this.testContext = testContext;
    }


    @When("Get TripXML Information")
    public void getTripXMLInformation() throws Exception {
    	addLog("Getting TripXML Information");
    	String tripId = testContext.getPageObjectManager().getTripResponse().getTripId();
        TripXMLResponse tripXMLResponse = testContext.getTripXMLService().fetchTripXML(tripId);
        testContext.getPageObjectManager().setTripXMLResponse(tripXMLResponse);
        PageBase.addLog("tripeXML : " + tripXMLResponse);
       // PageBase.addTripIdLog("tripeXML : " + tripXMLResponse);
    }
}

