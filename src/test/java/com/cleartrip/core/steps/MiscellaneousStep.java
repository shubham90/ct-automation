package com.cleartrip.core.steps;

import java.util.Map;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.MiscellaneousResponse;

import cucumber.api.java.en.And;

public class MiscellaneousStep extends BaseSteps{

    private TestContext testContext = null;

    public MiscellaneousStep(TestContext testContext) {
        this.testContext = testContext;
    }


    @And("^Apply Coupon \"([^\"]*)\"")
    public boolean applyCoupon(String coupon) throws Exception {
    	addLog("Applying coupon >>"+coupon);
    	int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
        Map<String, String> coupons = testContext.getConfigurationManager().getCoupons();
        MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
        return testContext.getPageObjectManager().getMiscellaneousPage().applyCoupon(wait,coupon.toLowerCase(), coupons,miscellaneousResponse);
    }
}
