package com.cleartrip.core.steps;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.cleartrip.core.pages.PageBase;
import com.cleartrip.core.utils.Helper;

public class BaseSteps extends Helper{
	
	public String getDateAfterAddingGivenNumberOfDays(String numberOfDays,String date_format) {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat(date_format);
		c.add(Calendar.DATE, Integer.parseInt(numberOfDays));
		String convertedDate = dateFormat.format(c.getTime());
		return convertedDate;
	}
	
	public void addLog(String message){
		PageBase.addLog(message);
	}
	
	public void addTripIdLog(String message){
		PageBase.addTripIdLog(message);
	}
	
	public static String getCurrentDateTime(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		String currenctDateTime=dateFormat.format(date);
		return currenctDateTime;
	}
	
	public int getWeekNumberOfTheMonthOfGivenDate(String completeDate, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = df.parse(completeDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int weekMonth = cal.get(Calendar.WEEK_OF_MONTH);
		return weekMonth;
	}
	
	public int getDayNumberOfTheMonthOfGivenDate(String completeDate, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = df.parse(completeDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int dayNumber = cal.get(Calendar.DAY_OF_MONTH);
		return dayNumber;
	}
	
	public int getMonthNumberFromGivenDate(String completeDate, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = df.parse(completeDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int monthNumber = cal.get(Calendar.MONTH) + 1;
		return monthNumber;
	}
	
	public int getYearFromGivenDate(String completeDate, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = df.parse(completeDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		return year;
	}
	
	public String getMonthNameOfGivenDate(String completeDate, String format) throws ParseException {
		int monthNumber=getMonthNumberFromGivenDate(completeDate,format);
		String monthName = "";
		DateFormatSymbols dfs = new DateFormatSymbols();
		String[] months = dfs.getMonths();
		if (monthNumber >= 0 && monthNumber <= 11) {
			monthName = months[monthNumber-1];
		}else {
			addLog("The given month number "+monthNumber+" is not valid.");
		}
		return monthName;
	}
	
	public String GetDateTimeDiff(String dateStart, String dateStop) {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");

		Date d1 = null;
		Date d2 = null;
		String diffTimeMessage;
		long diffMilliSeconds = -1;
		long diffSeconds = -1;
		long diffMinutes = -1;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);

			// in milliseconds
			long diff = d2.getTime() - d1.getTime();

			diffMilliSeconds = diff % 1000;
			diffSeconds = diff / 1000 % 60;
			diffMinutes = diff / (60 * 1000) % 60;
		} catch (Exception e) {
			e.printStackTrace();
		}
		diffTimeMessage = diffMinutes + " minutes, " + diffSeconds + " seconds and " + diffMilliSeconds
				+ " milliSeconds";
		return diffTimeMessage;
	}

	public void writeLoadTimeInFile(String str) throws IOException {
		//addLog(str);
		String cdate = getCurrentDateTime("dd_MM_yyyy");
		String fileName = System.getProperty("user.dir") + "/target/load-time-report";
		File file=new File(fileName);
		if (!file.exists()) {
			file.mkdir();
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName+"/loadTime_" + cdate + ".txt", true));
		writer.append('\n');
		writer.append('\n');
		writer.append(str);
		writer.close();
	}
	
	public String getDayNameOfGivenDate(String dateValue,String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = df.parse(dateValue);
		SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
		String dayName=simpleDateformat.format(date);
		return dayName;
	}
}
