package com.cleartrip.core.steps;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.FareDetailResponse;
import com.cleartrip.core.model.MiscellaneousResponse;

import cucumber.api.java.en.Then;

public class FareDetailsStep extends BaseSteps{

    private TestContext testContext = null;

    public FareDetailsStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Fetch Fare Details")
    public void fetchFareDetails() throws Exception {
    	addLog("Fetching Fare Details");
        int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
        MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
        FareDetailResponse fareDetailResponse = testContext.getPageObjectManager().
                getFareDetailsPage().fetchFareDetails(wait,miscellaneousResponse);
        testContext.getPageObjectManager().setFareDetailResponse(fareDetailResponse);
    }
}
