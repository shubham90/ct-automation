package com.cleartrip.core.steps;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.testng.Assert;

import com.cleartrip.core.constants.BookingType;
import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.ConnectorResponse;

import cucumber.api.java.en.When;

public class ConnectorStep extends BaseSteps {

	private TestContext testContext = null;
	String dateStart="";
    String dateStop="";

	public ConnectorStep(TestContext testContext) {
		this.testContext = testContext;
	}

	@When("^Get Connector Result from \"([^\"]*)\" to \"([^\"]*)\" date \"([^\"]*)\" adult \"([^\"]*)\" child \"([^\"]*)\" infant \"([^\"]*)\" preferedAirLine \"([^\"]*)\" returnDate \"([^\"]*)\" isSplitRT \"([^\"]*)\" travelclass \"([^\"]*)\" OW CONFIG \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" RT CONFIG \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" Channel \"([^\"]*)\" Domain \"([^\"]*)\" isIntl \"([^\"]*)\"$")
	public void getConnectorResult(String from, String to, String departDateNumber, int adult, int child, int infant,
			String preferedAirLine, String returnDateNumber, String isSplitRT, String travelClass, String owSupplier,
			String owAirline, String owConnected, String rtSupplier, String rtAirline, String rtConnected,String channel,String domain, String intl)
			throws Exception {
		addLog(":::::: Fetching Connector response ::::: ");
		BookingType bookingType = BookingType.ONE_WAY;
		String dateFormat = "dd-MM-yyyy";

		String departDate = getDateAfterAddingGivenNumberOfDays(departDateNumber, dateFormat);

		int departYear = getYearFromGivenDate(departDate, dateFormat);
		int departMonth = getMonthNumberFromGivenDate(departDate, dateFormat);
		String departMonthName = getMonthNameOfGivenDate(departDate, dateFormat);
		int departDay = getDayNumberOfTheMonthOfGivenDate(departDate, dateFormat);
		int departWeekOfTheMonth = getWeekNumberOfTheMonthOfGivenDate(departDate, dateFormat);

		int returnYear = -1;
		int returnMonth = -1;
		String returnMonthName = "";
		int returnDay = -1;
		int returnWeekOfTheMonth = -1;

		String returnDate = "";
		if (StringUtils.isNotBlank(returnDateNumber)) {
			bookingType = BookingType.ROUND_TRIP;
			returnDate = getDateAfterAddingGivenNumberOfDays(returnDateNumber, dateFormat);
			Assert.assertTrue(Integer.valueOf(departDateNumber.trim()) < Integer.valueOf(returnDateNumber.trim()),
					"The return date can not be less than depart date. Depart Date: " + departDate + " Return Date: "
							+ returnDate);

			returnYear = getYearFromGivenDate(returnDate, dateFormat);
			returnMonth = getMonthNumberFromGivenDate(returnDate, dateFormat);
			returnMonthName = getMonthNameOfGivenDate(returnDate, dateFormat);
			returnDay = getDayNumberOfTheMonthOfGivenDate(returnDate, dateFormat);
			returnWeekOfTheMonth = getWeekNumberOfTheMonthOfGivenDate(returnDate, dateFormat);
		}
		boolean splitRT = ("true".equalsIgnoreCase(isSplitRT.trim())) ? true : false;
		boolean isIntl = ("true".equalsIgnoreCase(intl.trim())) ? true : false;

		BookingRequest bookingRequest = new BookingRequest(adult, infant, child, from, to, departDate, splitRT,
				travelClass, bookingType, preferedAirLine, returnDate, false, departYear, departMonth, departMonthName,
				departDay, departWeekOfTheMonth, returnYear, returnMonth, returnMonthName, returnDay,
				returnWeekOfTheMonth,isIntl);

		Map<String, Set<String>> mappings = prepareMapping(owSupplier, owAirline, owConnected, rtSupplier, rtAirline,
				rtConnected);
		dateStart=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
		ConnectorResponse connectorResponse = testContext.getConnectorService().fetch(bookingRequest, mappings,channel,domain,isIntl);
		dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
		writeLoadTimeInFile("Time taken to get connector search result is : " +GetDateTimeDiff(dateStart, dateStop));
		testContext.getPageObjectManager().setConnectorResponse(connectorResponse);
	}

	private Map<String, Set<String>> prepareMapping(String owSupplier, String owAirline, String owConnected,
			String rtSupplier, String rtAirline, String rtConnected) {

		Map<String, Set<String>> mappings = new HashMap<>();
		mappings.put(owSupplier, new HashSet<>());
		mappings.put(rtSupplier, new HashSet<>());

		if (!owConnected.equalsIgnoreCase("X")) {
			mappings.get(owSupplier).add(owConnected);
		}
		if (!owAirline.equalsIgnoreCase("X")) {
			mappings.get(owSupplier).add(owAirline);
		}
		if (!rtAirline.equalsIgnoreCase("X")) {
			mappings.get(rtSupplier).add(rtAirline);
		}
		if (!rtConnected.equalsIgnoreCase("X")) {
			mappings.get(rtSupplier).add(rtConnected);
		}
		return mappings;
	}
}
