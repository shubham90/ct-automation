package com.cleartrip.core.steps;

import org.testng.asserts.SoftAssert;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.ItineraryResponse;
import com.cleartrip.core.model.SRPResponse;

import cucumber.api.java.en.Then;

public class ValidateItineraryStep extends BaseSteps{
	
	private TestContext testContext = null;

    public ValidateItineraryStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Validate Itinerary")
    public void validateItineraryPage() throws Exception {
    	addLog("Validating Itinerary Details");
        SRPResponse srpResponse = testContext.getPageObjectManager().getSrpResponse();
        ItineraryResponse itineraryResponse = testContext.getPageObjectManager().getItineraryResponse();
        BookingRequest bookingRequest = testContext.getPageObjectManager().getBookingRequest();
        SoftAssert softAssert = testContext.getPageObjectManager().getSoftAssert();


        testContext.getItineraryPageValidators()
                .validateItineraryResponse(srpResponse, itineraryResponse, bookingRequest,softAssert);
    }

    @Then("Validate Pre Itinarary")
    public void preItinarary()  throws Exception {
//    	addLog("Validating Pre-Itinerary Details");
       // testContext.getSrpResponse().getSelectionBookElement().click();
    }

}
