package com.cleartrip.core.steps;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.StatsResponse;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StatsStep extends BaseSteps{

    private TestContext testContext = null;

    public StatsStep(TestContext testContext) {
        this.testContext = testContext;
    }


    @When("Get Stats Information")
    public void getStatsInformation() throws Exception {
    	addLog("Getting stats information");
        int wait = Integer.valueOf(testContext.getConfigurationManager().getWait());
        StatsResponse statsResponse = testContext.
                getStatsService().fetchStats(testContext.getPageObjectManager().getTripResponse().getTripId());
        testContext.getPageObjectManager().setStatsResponse(statsResponse);

    }
}
