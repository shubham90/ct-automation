package com.cleartrip.core.steps;

import java.io.File;

import javax.imageio.ImageIO;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.utils.Helper;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class Hookable extends BaseSteps {

	private TestContext testContext = null;

	public Hookable(TestContext testContext) {
		this.testContext = testContext;
	}

	static ExtentReports extent;
	static ExtentHtmlReporter htmlReporter;
	static ExtentTest reporter;
	static int i = 1;
	String testCaseName = "";

	/*@After(order = 1)
	public void afterScenario(Scenario scenario) {
		if (scenario.isFailed()) {

			String date = getCurrentDateTime("yyyy-MM-dd_HH_mm_ss");
			String screenshotName = (scenario.getName() + date).replaceAll(" ", "_");
			try {
				File sourcePath = ((TakesScreenshot) testContext.getPageObjectManager().getDriver())
						.getScreenshotAs(OutputType.FILE);

				String directoryName = System.getProperty("user.dir") + "/target/cucumber-reports/screenshots/";

				File directory = new File(directoryName);
				if (!directory.exists()) {
					directory.mkdir();
				}

				File destinationPath = new File(directoryName + screenshotName + ".png");

				Files.copy(sourcePath, destinationPath);
				Reporter.addScreenCaptureFromPath(destinationPath.toString());
				AfterSteps();
			} catch (Exception e) {
				e.toString();
			}
		}
	}*/

	public void AfterSteps() {
//		testContext.getPageObjectManager().getDriver().quit();;
	}

	@Before
	public void setup(Scenario scenario) {
		String dateTime = getCurrentDateTime("yyyy-MM-dd_HH_mm_ss");
		String featureName = scenario.getName().replaceAll(" ", "_");

		Helper helper = new Helper();
		if (extent == null && htmlReporter == null && reporter == null) {
			extent = new ExtentReports();
			String filePath = System.getProperty("user.dir") + "/target/report_tripIds/";
			File file = new File(filePath);
			if (!file.exists()) {
				file.mkdir();
			}
			htmlReporter = new ExtentHtmlReporter(
					filePath + "/" + featureName + "_tripd_id_details_" + dateTime + ".html");
			extent = new ExtentReports();
			extent.attachReporter(htmlReporter);
		}
		testCaseName = featureName + " : Test Case " + i;
		reporter = extent.createTest(testCaseName);
		extent.getStats();
		scenario.getStatus();
		helper.setExtentReportObject(extent, reporter);
		++i;
	}

	@After
	public void getResult(Scenario scenario) {

		if (scenario.isFailed()) {
//			reporter.info(MarkupHelper.createLabel(scenario.getClass() + " - Test Case Failed", ExtentColor.RED));
			reporter.log(Status.FAIL, MarkupHelper.createLabel(testCaseName + " - Test Case Failed", ExtentColor.RED));
			captureScreenshot(scenario);
		}
		extent.flush();
	}

	public void captureScreenshot(Scenario scenario) {
		try {
			WebDriver driver = testContext.getPageObjectManager().getDriver();
			String date = getCurrentDateTime("yyyy-MM-dd_HH_mm_ss");
			String path = System.getProperty("user.dir");
			String directoryName = path + "/target/report_tripIds/screenshots";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			String screenshotName = (scenario.getName() + date).replaceAll(" ", "_");
			String screehshotPath = directoryName + "/" + screenshotName + ".png";
			Screenshot fpScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000))
					.takeScreenshot(driver);
			ImageIO.write(fpScreenshot.getImage(), "PNG", new File(screehshotPath));

			method_ScreenShot(screehshotPath);
		} catch (Exception e) {

		}
	}

	public void method_ScreenShot(String path) throws Exception {
		reporter.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(path).build());
	}

}
