package com.cleartrip.core.steps;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.StatsResponse;
import cucumber.api.java.en.Then;

public class ConnectorValidatorStep extends BaseSteps {

    private TestContext testContext = null;

    public ConnectorValidatorStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("Validate Connectors Detail")
    public void validateConnector() throws Exception {
    	addLog("Validating Connectors Detail");
        StatsResponse statsResponse = testContext.getPageObjectManager().getStatsResponse();
        testContext.getIndigoValidator().validate(statsResponse.getApiResponses(),
                testContext.getPageObjectManager().getHqResponse(), testContext.getPageObjectManager().getSoftAssert());
    }
}