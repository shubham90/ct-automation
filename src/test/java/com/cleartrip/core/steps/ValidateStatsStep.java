package com.cleartrip.core.steps;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.StatsResponse;

import cucumber.api.java.en.Then;

public class ValidateStatsStep extends BaseSteps{

    private TestContext testContext = null;

    public ValidateStatsStep(TestContext testContext) {
        this.testContext = testContext;
    }
    
    String smsOWValue="";
    String smsRTValue="";

    @Then("Validate Stats Information")
    public void validateStats() throws FileNotFoundException, IOException, ParseException {
    	addLog("Validating Stats Information");
    	StatsResponse statsResponse = testContext.getPageObjectManager().getStatsResponse();
        testContext.getPageObjectManager().getSoftAssert().assertNotNull(statsResponse, "Stats cannot be zero");
        testContext.getPageObjectManager().getSoftAssert().assertTrue(statsResponse.getFailures().size() == 0,
                "Stats CT Failure Should be Zero");
        testContext.getPageObjectManager().getSoftAssert().assertTrue(statsResponse.getApiCalls().size() > 0,
                "Stats Api Failure cannot be Zero");
        SoftAssert softAssert=testContext.getPageObjectManager().getSoftAssert();
        
        boolean isSMSFlow=statsResponse.isSmsFlow();
        verifyIsSMSFlow(isSMSFlow,softAssert);
    }
    
    public void verifyIsSMSFlow(boolean isSMSFlow,SoftAssert softAssert) throws FileNotFoundException, IOException, ParseException {
    	try {
    	Map<String, String> sms_properties=getQA2Property();
        HashSet<String> unionKeys = new HashSet<>(sms_properties.keySet());
        Iterator value = unionKeys.iterator();
		while (value.hasNext()) {
			String key = value.next().toString();
			if(key.contains("oneway")) {
				smsOWValue=sms_properties.get(key).toLowerCase();
				System.out.println("smsOWValue : "+smsOWValue);
			}else if(key.contains("roundtrip")) {
				smsRTValue=sms_properties.get(key).toLowerCase();
				System.out.println("smsRTValue : "+smsRTValue);
			}
		}
		
		BookingRequest bookingRequest=testContext.getPageObjectManager().getBookingRequest();
		MiscellaneousResponse miscellaneousResponse=testContext.getPageObjectManager().getMiscellaneousResponse();
		String from=bookingRequest.getFrom().toLowerCase();
		String to=bookingRequest.getTo().toLowerCase();
		boolean isSpecialRT=miscellaneousResponse.isSpecialRT();
		String returnDate=bookingRequest.getReturnDate();
		boolean isSplitRT=bookingRequest.isSplitRT();
		String owSupplier=miscellaneousResponse.getOwSupplier().toLowerCase();
		String rtSupplier=miscellaneousResponse.getRtSupplier().toLowerCase();
		
		boolean smsOWEnabled=isSMSEnabled(smsOWValue,owSupplier,from,to);
		System.out.println("smsOWEnabled : "+smsOWEnabled);
		boolean smsRTEnabled=isSMSEnabled(smsRTValue,rtSupplier,from,to);
		System.out.println("smsRTEnabled : "+smsRTEnabled);
		
		if(smsOWValue.contains(owSupplier)) {
			addTripIdLog("Validating SMS Flow.");
			if(StringUtils.isEmpty(returnDate)) {
				System.out.println("smsRTEnabled1");
				softAssert.assertEquals(isSMSFlow, smsOWEnabled,"SMS OW is enabled, eventhough this booking won't went through SMS Flow.");
			}
			
			if(smsOWValue.contains(rtSupplier)) {
				if(!isSpecialRT && isSplitRT && StringUtils.isNotEmpty(returnDate)) {
					System.out.println("smsRTEnabled2");
					softAssert.assertEquals(isSMSFlow, smsOWEnabled,"SMS OW is enabled, eventhough this booking won't went through SMS Flow.");
				}
			}
			
			if(owSupplier.equals(rtSupplier) || smsRTValue.contains(rtSupplier)) {
				if(isSpecialRT && StringUtils.isNotEmpty(returnDate)) {
					System.out.println("smsRTEnabled3");
					softAssert.assertEquals(isSMSFlow, smsRTEnabled,"SMS OW is enabled, eventhough this booking won't went through SMS Flow.");
				}else if(!isSpecialRT && !isSplitRT && StringUtils.isNotEmpty(returnDate)) {
					System.out.println("smsRTEnabled4");
					softAssert.assertEquals(isSMSFlow, smsRTEnabled,"SMS OW is enabled, eventhough this booking won't went through SMS Flow.");
				}
			}
		}
    	}catch(Exception e) {
    		e.getMessage();
    	}
    }
    
    public static Map<String, String> getQA2Property() {
		Map<String, String> map_qa2 = new HashMap<>();
		try {
			URL url = new URL(
					"http://172.17.26.160:8097/common/inspect_resource?beanName=commonCachedProperties&json=false");

			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			String str;
			while ((str = in.readLine()) != null) {
				str = in.readLine().toString();
				if (str.toLowerCase().contains("ct.air.sms.oneway.enabled.connectors") || str.toLowerCase().contains("ct.air.sms.roundtrip.enabled.connectors")) {
					List<String> keyValue = Arrays.asList(str.split("="));
					if (!(keyValue.size() < 2)) {
						String key = keyValue.get(0);
						String value = keyValue.get(1);
//						System.out.println("QA2 : Key : "+key+" , value : "+value);
						map_qa2.put(key, value);
					}
				}
			}
			in.close();
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		}
		return map_qa2;
	}
    
    public boolean isSMSEnabled(String smsJson,String supplier,String from,String to) throws FileNotFoundException, IOException, ParseException {
    	JSONParser parser = new JSONParser(); 
    	JSONObject json = (JSONObject) parser.parse(smsJson);
    	String sectorList=(String) json.get(supplier.toUpperCase());
    	System.out.println("sectorList : "+sectorList);
    	if(sectorList.contains("*")) {
    		return true;
    	}else if(sectorList.contains(from) && sectorList.contains(to)) {
    		return true;
    	}
		return false; 
    }
}
