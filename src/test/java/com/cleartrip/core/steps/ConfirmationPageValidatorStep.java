package com.cleartrip.core.steps;

import com.cleartrip.core.context.TestContext;
import com.cleartrip.core.model.TripResponse;
import cucumber.api.java.en.Then;
import org.testng.Assert;

public class ConfirmationPageValidatorStep extends BaseSteps {

    private TestContext testContext = null;

    public ConfirmationPageValidatorStep(TestContext testContext) {
        this.testContext = testContext;
    }

    @Then("validate Pre Confirmation")
    public void preConfirmationPage() {
    	addLog("Validating Pre Confirmation Page");
    }

    @Then("Validate Confirmation Page")
    public void postConfirmationPage() {
    	addLog("Validating Confirmation Page");
        TripResponse tripResponse = testContext.getPageObjectManager().getTripResponse();
        Assert.assertNotNull(tripResponse, "Trip Response cannot be null");
        Assert.assertNotNull(tripResponse.getTripId(), "Trip Id cannot be null");

    }
}
