package com.cleartrip.core.model;

public class FareBreakupInfo {

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getBaseFare() {
        return baseFare;
    }

    @Override
    public String toString() {
        return "FareBreakupInfo{" +
                "tax='" + tax + '\'' +
                ", baseFare='" + baseFare + '\'' +
                ", totalFare=" + totalFare +
                '}';
    }

    public void setBaseFare(Double baseFare) {
        this.baseFare = baseFare;
    }
    
    private Double tax;
    private Double baseFare;
    private Double totalFare;
    
	public Double getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(Double totalFare) {
		this.totalFare = totalFare;
	}



}
