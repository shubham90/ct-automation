package com.cleartrip.core.model;

public class ConnectorResponse {

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    private boolean success;

    @Override
    public String toString() {
        return "ConnectorResponse{" +
                "success=" + success +
                '}';
    }
}
