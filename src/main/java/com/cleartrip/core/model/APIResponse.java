package com.cleartrip.core.model;

public class APIResponse {

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public APIResponse(String response, String type, String url) {
        this.response = response;
        this.type = type;
        this.url = url;
    }

    @Override
    public String toString() {
        return "APIResponse{" +
                "response='" + "showing " + '\'' +
                ", type='" + type + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String response;
    private String type;
    private String url;


}
