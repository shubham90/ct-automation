package com.cleartrip.core.model;

import com.cleartrip.core.constants.BookingType;

public class BookingRequest {
    private int adults;
    private int infants;
    private int childs;
    private String from;
    private String to;
    private String departDate;
    private String returnDate;
    private boolean splitRT;
    private String preferredAirLine;
    private boolean BaggageRequested;
    private boolean mealRequested;
    private boolean seatRequested;
    private String travelClass;
    private BookingType bookingType;
    private boolean insuranceSelected;
    private boolean couponBooking;
    private boolean isTicketing;
    private int departYear;
    private int departMonth;
    private String departMonthName;
    private int departDay;
    private int departWeekOfTheMonth;
    private int returnYear;
    private int returnMonth;
    private String returnMonthName;
    private int returnDay;
    private int returnWeekOfTheMonth;
    private boolean isIntl;

    public BookingRequest(int adults, int infants, int childs, String from, String to, String departDate, boolean srt,
                          String travelClass, BookingType bookingType, String preferredAirLine, String returnDate,
                          boolean isTicketing,int departYear,int departMonth,String departMonthName,int departDay,int departWeekOfTheMonth,int returnYear,int returnMonth, String returnMonthName,int returnDay,int returnWeekOfTheMonth, boolean isIntl) {
        this.adults = adults;
        this.infants = infants;
        this.childs = childs;
        this.from = from;
        this.to = to;
        this.departDate = departDate;
        this.splitRT = srt;
        this.travelClass = travelClass;
        this.bookingType = bookingType;
        this.preferredAirLine = preferredAirLine;
        this.returnDate = returnDate;
        this.isTicketing = isTicketing;
        this.departYear=departYear;
        this.departMonth=departMonth;
        this.departMonthName=departMonthName;
        this.departDay=departDay;
        this.departWeekOfTheMonth=departWeekOfTheMonth;
        this.returnYear=returnYear;
        this.returnMonth=returnMonth;
        this.returnMonthName=returnMonthName;
        this.returnDay=returnDay;
        this.returnWeekOfTheMonth=returnWeekOfTheMonth;
        this.isIntl=isIntl;
    }

    @Override
	public String toString() {
		return "BookingRequest [adults=" + adults + ", infants=" + infants + ", childs=" + childs + ", from=" + from
				+ ", to=" + to + ", departDate=" + departDate + ", returnDate=" + returnDate + ", splitRT=" + splitRT
				+ ", preferredAirLine=" + preferredAirLine + ", BaggageRequested=" + BaggageRequested
				+ ", mealRequested=" + mealRequested + ", seatRequested=" + seatRequested + ", travelClass="
				+ travelClass + ", bookingType=" + bookingType + ", insuranceSelected=" + insuranceSelected
				+ ", couponBooking=" + couponBooking + ", isTicketing=" + isTicketing + ", departYear=" + departYear
				+ ", departMonth=" + departMonth + ", departMonthName=" + departMonthName + ", departDay=" + departDay
				+ ", departWeekOfTheMonth=" + departWeekOfTheMonth + ", returnYear=" + returnYear + ", returnMonth="
				+ returnMonth + ", returnMonthName=" + returnMonthName + ", returnDay=" + returnDay
				+ ", returnWeekOfTheMonth=" + returnWeekOfTheMonth + ", isIntl=" + isIntl + "]";
	}

	public int getAdults() {
		return adults;
	}

	public void setAdults(int adults) {
		this.adults = adults;
	}

	public int getInfants() {
		return infants;
	}

	public void setInfants(int infants) {
		this.infants = infants;
	}

	public int getChilds() {
		return childs;
	}

	public void setChilds(int childs) {
		this.childs = childs;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getDepartDate() {
		return departDate;
	}

	public void setDepartDate(String departDate) {
		this.departDate = departDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public boolean isSplitRT() {
		return splitRT;
	}

	public void setSplitRT(boolean srt) {
		this.splitRT = srt;
	}

	public String getPreferredAirLine() {
		return preferredAirLine;
	}

	public void setPreferredAirLine(String preferredAirLine) {
		this.preferredAirLine = preferredAirLine;
	}

	public boolean isBaggageRequested() {
		return BaggageRequested;
	}

	public void setBaggageRequested(boolean baggageRequested) {
		BaggageRequested = baggageRequested;
	}

	public boolean isMealRequested() {
		return mealRequested;
	}

	public void setMealRequested(boolean mealRequested) {
		this.mealRequested = mealRequested;
	}

	public boolean isSeatRequested() {
		return seatRequested;
	}

	public void setSeatRequested(boolean seatRequested) {
		this.seatRequested = seatRequested;
	}

	public String getTravelClass() {
		return travelClass;
	}

	public void setTravelClass(String travelClass) {
		this.travelClass = travelClass;
	}

	public BookingType getBookingType() {
		return bookingType;
	}

	public void setBookingType(BookingType bookingType) {
		this.bookingType = bookingType;
	}

	public boolean isInsuranceSelected() {
		return insuranceSelected;
	}

	public void setInsuranceSelected(boolean insuranceSelected) {
		this.insuranceSelected = insuranceSelected;
	}

	public boolean isCouponBooking() {
		return couponBooking;
	}

	public void setCouponBooking(boolean couponBooking) {
		this.couponBooking = couponBooking;
	}

	public boolean isIntl() {
		return isTicketing;
	}

	public void setIntl(boolean isIntl) {
		this.isTicketing = isIntl;
	}

	public int getDepartYear() {
		return departYear;
	}

	public void setDepartYear(int departYear) {
		this.departYear = departYear;
	}

	public int getDepartMonth() {
		return departMonth;
	}

	public void setDepartMonth(int departMonth) {
		this.departMonth = departMonth;
	}

	public String getDepartMonthName() {
		return departMonthName;
	}

	public void setDepartMonthName(String departMonthName) {
		this.departMonthName = departMonthName;
	}

	public int getDepartDay() {
		return departDay;
	}

	public void setDepartDay(int departDay) {
		this.departDay = departDay;
	}
	
	public int getDepartWeekOfTheMonth() {
		return departWeekOfTheMonth;
	}

	public void setDepartWeekOfTheMonth(int departWeekOfTheMonth) {
		this.departWeekOfTheMonth = departWeekOfTheMonth;
	}

	public int getReturnYear() {
		return returnYear;
	}

	public void setReturnYear(int returnYear) {
		this.returnYear = returnYear;
	}

	public int getReturnMonth() {
		return returnMonth;
	}

	public void setReturnMonth(int returnMonth) {
		this.returnMonth = returnMonth;
	}

	public String getReturnMonthName() {
		return returnMonthName;
	}

	public void setReturnMonthName(String returnMonthName) {
		this.returnMonthName = returnMonthName;
	}

	public int getReturnDay() {
		return returnDay;
	}

	public void setReturnDay(int returnDay) {
		this.returnDay = returnDay;
	}

	public int getReturnWeekOfTheMonth() {
		return returnWeekOfTheMonth;
	}

	public void setReturnWeekOfTheMonth(int returnWeekOfTheMonth) {
		this.returnWeekOfTheMonth = returnWeekOfTheMonth;
	}
}
