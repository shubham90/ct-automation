package com.cleartrip.core.model;

import java.util.ArrayList;
import java.util.List;

public class TripXMLResponse {

    private List<String> pnrs = new ArrayList<>();
    private String overAllBookingStatus;
    private List<String> individualBookingStatus = new ArrayList<>();


    public List<String> getPnrs() {
        return pnrs;
    }

    public String getOverAllBookingStatus() {
        return overAllBookingStatus;
    }

    public void setOverAllBookingStatus(String overAllBookingStatus) {
        this.overAllBookingStatus = overAllBookingStatus;
    }

    public List<String> getIndividualBookingStatus() {
        return individualBookingStatus;
    }

    public void setIndividualBookingStatus(List<String> individualBookingStatus) {
        this.individualBookingStatus = individualBookingStatus;
    }

    public void setPnrs(List<String> pnrs) {
        this.pnrs = pnrs;
    }


    public void addPNR(String pnr) {
        this.pnrs.add(pnr);
    }

    public void addIndividualBookingStatus(String bookingStatus) {
        this.individualBookingStatus.add(bookingStatus);
    }

    @Override
    public String toString() {
        return "TripXMLResponse{" +
                "pnrs=" + pnrs +
                ", overAllBookingStatus='" + overAllBookingStatus + '\'' +
                ", individualBookingStatus=" + individualBookingStatus +
                '}';
    }
}
