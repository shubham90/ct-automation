package com.cleartrip.core.model;

public class FareDetailResponse {

	private String currency;
	private int adultCount;
	private int childCount;
	private int infantCount;
	private Double insuranceValue;
	private Double seat_fare;
	private Double convenienceFeePerPerson;
	private Double convenienceFee;
	private Double emiProcessingFee;
	private Double cleartripCash;
	private Double giftVoucherValue;
	private Double totalTax;
	private Double totalAmount;
	private Double totalAmountOnPaymentPage;
	private Double mealsAndBaggage_fare;
	private Double baseFareAdult;
	private Double baseFareChild;
	private Double baseFareInfant;
    private Double couponDiscountValue;
	@Override
	public String toString() {
		return "FareDetailResponse [currency=" + currency + ", adultCount=" + adultCount + ", childCount=" + childCount
				+ ", infantCount=" + infantCount + ", insuranceValue=" + insuranceValue + ", seat_fare=" + seat_fare
				+ ", convenienceFeePerPerson=" + convenienceFeePerPerson + ", convenienceFee=" + convenienceFee
				+ ", emiProcessingFee=" + emiProcessingFee + ", cleartripCash=" + cleartripCash + ", giftVoucherValue="
				+ giftVoucherValue + ", totalTax=" + totalTax + ", totalAmount=" + totalAmount
				+ ", totalAmountOnPaymentPage=" + totalAmountOnPaymentPage + ", mealsAndBaggage_fare="
				+ mealsAndBaggage_fare + ", baseFareAdult=" + baseFareAdult + ", baseFareChild=" + baseFareChild
				+ ", baseFareInfant=" + baseFareInfant + ", couponDiscountValue=" + couponDiscountValue + "]";
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public int getAdultCount() {
		return adultCount;
	}
	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}
	public int getChildCount() {
		return childCount;
	}
	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}
	public int getInfantCount() {
		return infantCount;
	}
	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}
	public Double getInsuranceValue() {
		return insuranceValue;
	}
	public void setInsuranceValue(Double insuranceValue) {
		this.insuranceValue = insuranceValue;
	}
	public Double getSeat_fare() {
		return seat_fare;
	}
	public void setSeat_fare(Double seat_fare) {
		this.seat_fare = seat_fare;
	}
	public Double getConvenienceFeePerPerson() {
		return convenienceFeePerPerson;
	}
	public void setConvenienceFeePerPerson(Double convenienceFeePerPerson) {
		this.convenienceFeePerPerson = convenienceFeePerPerson;
	}
	public Double getConvenienceFee() {
		return convenienceFee;
	}
	public void setConvenienceFee(Double convenienceFee) {
		this.convenienceFee = convenienceFee;
	}
	public Double getEmiProcessingFee() {
		return emiProcessingFee;
	}
	public void setEmiProcessingFee(Double emiProcessingFee) {
		this.emiProcessingFee = emiProcessingFee;
	}
	public Double getCleartripCash() {
		return cleartripCash;
	}
	public void setCleartripCash(Double cleartripCash) {
		this.cleartripCash = cleartripCash;
	}
	public Double getGiftVoucherValue() {
		return giftVoucherValue;
	}
	public void setGiftVoucherValue(Double giftVoucherValue) {
		this.giftVoucherValue = giftVoucherValue;
	}
	public Double getTotalTax() {
		return totalTax;
	}
	public void setTotalTax(Double totalTax) {
		this.totalTax = totalTax;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Double getTotalAmountOnPaymentPage() {
		return totalAmountOnPaymentPage;
	}
	public void setTotalAmountOnPaymentPage(Double totalAmountOnPaymentPage) {
		this.totalAmountOnPaymentPage = totalAmountOnPaymentPage;
	}
	public Double getMealsAndBaggage_fare() {
		return mealsAndBaggage_fare;
	}
	public void setMealsAndBaggage_fare(Double mealsAndBaggage_fare) {
		this.mealsAndBaggage_fare = mealsAndBaggage_fare;
	}
	public Double getBaseFareAdult() {
		return baseFareAdult;
	}
	public void setBaseFareAdult(Double baseFareAdult) {
		this.baseFareAdult = baseFareAdult;
	}
	public Double getBaseFareChild() {
		return baseFareChild;
	}
	public void setBaseFareChild(Double baseFareChild) {
		this.baseFareChild = baseFareChild;
	}
	public Double getBaseFareInfant() {
		return baseFareInfant;
	}
	public void setBaseFareInfant(Double baseFareInfant) {
		this.baseFareInfant = baseFareInfant;
	}
	public Double getCouponDiscountValue() {
		return couponDiscountValue;
	}
	public void setCouponDiscountValue(Double couponDiscountValue) {
		this.couponDiscountValue = couponDiscountValue;
	}

	
}
