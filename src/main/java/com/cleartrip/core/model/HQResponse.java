package com.cleartrip.core.model;

import java.util.ArrayList;
import java.util.List;

public class HQResponse {

    private Double adultAmount;
    private Double childAmount;
    private Double infantAmount;
    private int childCount;
    private int infantCount;
    private int adultCount;
    private Double insurance;
    private Double seat;
    private Double convenienceFee;
    private Double total;
    private Double otherCharges;
    private Double gstCharges;
    private Double totalTax;
    private Double meal;
    private Double baggage;
    private Double discount;
    
    List<TicketDetails> tickets = new ArrayList<>();
    private List<Flight> flights = new ArrayList<>();
    private List<PassangerDetail> passangerDetails = new ArrayList<>();

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    public List<PassangerDetail> getPassangerDetails() {
        return passangerDetails;
    }

    public void setPassangerDetails(List<PassangerDetail> passangerDetails) {
        this.passangerDetails = passangerDetails;
    }

    public Double getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(Double otherCharges) {
        this.otherCharges = otherCharges;
    }


    public Double getAdultAmount() {
        return adultAmount;
    }

    public void setAdultAmount(Double adultAmount) {
        this.adultAmount = adultAmount;
    }

    public Double getChildAmount() {
        return childAmount;
    }

    public void setChildAmount(Double childAmount) {
        this.childAmount = childAmount;
    }

    public Double getInfantAmount() {
        return infantAmount;
    }

    public void setInfantAmount(Double infantAmount) {
        this.infantAmount = infantAmount;
    }

    public int getChildCount() {
        return childCount;
    }

    public void setChildCount(int childCount) {
        this.childCount = childCount;
    }

    public int getInfantCount() {
        return infantCount;
    }

    public void setInfantCount(int infantCount) {
        this.infantCount = infantCount;
    }

    public int getAdultCount() {
        return adultCount;
    }

    public void setAdultCount(int adultCount) {
        this.adultCount = adultCount;
    }

    public Double getInsurance() {
        return insurance;
    }

    public void setInsurance(Double insurance) {
        this.insurance = insurance;
    }

    public Double getSeat() {
        return seat;
    }

    public void setSeat(Double seat) {
        this.seat = seat;
    }

    public Double getConvenienceFee() {
        return convenienceFee;
    }

    public void setConvenienceFee(Double convenienceFee) {
        this.convenienceFee = convenienceFee;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<TicketDetails> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketDetails> tickets) {
        this.tickets = tickets;
    }

    public void addTicket(TicketDetails ticketDetails) {
        tickets.add(ticketDetails);
    }

    public void addFlight(Flight flight) {
        flights.add(flight);
    }
    public void addPassangerDetail(PassangerDetail passangerDetail) {
        passangerDetails.add(passangerDetail);
    }

    public Double getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(Double totalTax) {
		this.totalTax = totalTax;
	}

	public Double getGstCharges() {
		return gstCharges;
	}

	public void setGstCharges(Double gstCharges) {
		this.gstCharges = gstCharges;
	}

	public Double getMeal() {
		return meal;
	}

	public void setMeal(Double meal) {
		this.meal = meal;
	}

	public Double getBaggage() {
		return baggage;
	}

	public void setBaggage(Double baggage) {
		this.baggage = baggage;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	@Override
	public String toString() {
		return "HQResponse [adultAmount=" + adultAmount + ", childAmount=" + childAmount + ", infantAmount="
				+ infantAmount + ", childCount=" + childCount + ", infantCount=" + infantCount + ", adultCount="
				+ adultCount + ", insurance=" + insurance + ", seat=" + seat + ", convenienceFee=" + convenienceFee
				+ ", total=" + total + ", otherCharges=" + otherCharges + ", gstCharges=" + gstCharges + ", totalTax="
				+ totalTax + ", meal=" + meal + ", baggage=" + baggage + ", discount=" + discount + ", tickets="
				+ tickets + ", flights=" + flights + ", passangerDetails=" + passangerDetails + "]";
	}
}