package com.cleartrip.core.model;

public class TravellerDetail {

	private String birth;
	private String nationality;
	private String firstName;
	private String lastName;
	private String day;
	private String month;
	private String year;
	private String passport;
	private String title;
	private String passexpiryday;
	private String passExpiryMonth;
	private String passExpiryYear;

	@Override
	public String toString() {
		return "TravellerDetail [birth=" + birth + ", nationality=" + nationality + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", day=" + day + ", month=" + month + ", year=" + year + ", passport="
				+ passport + ", title=" + title + ", passexpiryday=" + passexpiryday + ", passExpiryMonth="
				+ passExpiryMonth + ", passExpiryYear=" + passExpiryYear + ", birth='" + birth
				+ ", nationality='" + nationality + '\'' + "]";
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}


	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPassexpiryday() {
		return passexpiryday;
	}

	public void setPassexpiryday(String passexpiryday) {
		this.passexpiryday = passexpiryday;
	}

	public String getPassExpiryMonth() {
		return passExpiryMonth;
	}

	public void setPassExpiryMonth(String passExpiryMonth) {
		this.passExpiryMonth = passExpiryMonth;
	}

	public String getPassExpiryYear() {
		return passExpiryYear;
	}

	public void setPassExpiryYear(String passExpiryYear) {
		this.passExpiryYear = passExpiryYear;
	}

}
