package com.cleartrip.core.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatsResponse {

    public boolean isSmsFlow() {
        return smsFlow;
    }

    public void setSmsFlow(boolean smsFlow) {
        this.smsFlow = smsFlow;
    }

    public boolean isSffFlow() {
        return sffFlow;
    }

    public void setSffFlow(boolean sffFlow) {
        this.sffFlow = sffFlow;
    }

    public Map<String, String> getFailures() {
        return failures;
    }

    public void setFailures(Map<String, String> failures) {
        this.failures = failures;
    }

    public List<APIResponse> getApiResponses() {
        return apiResponses;
    }

    public void setApiResponses(List<APIResponse> apiResponses) {
        this.apiResponses = apiResponses;
    }

    private List<APIResponse> apiResponses = new ArrayList<>();

    private boolean smsFlow;
    private boolean sffFlow;

    private Map<String, String> failures = new HashMap<>();

    @Override
    public String toString() {
        return "StatsResponse{" +
                "apiResponses=" + apiResponses +
                ", smsFlow=" + smsFlow +
                ", sffFlow=" + sffFlow +
                ", failures=" + failures +
                ", apiCalls=" + apiCalls +
                '}';
    }

    public List<String> getApiCalls() {
        return apiCalls;
    }

    public void setApiCalls(List<String> apiCalls) {
        this.apiCalls = apiCalls;
    }

    List<String> apiCalls = new ArrayList<>();



}
