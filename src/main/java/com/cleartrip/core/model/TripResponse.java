package com.cleartrip.core.model;

public class TripResponse {

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    @Override
    public String toString() {
        return "TripResponse{" +
                "tripId='" + tripId + '\'' +
                '}';
    }

    private String tripId;

}
