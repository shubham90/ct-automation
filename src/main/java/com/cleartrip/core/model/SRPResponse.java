package com.cleartrip.core.model;

import org.openqa.selenium.WebElement;

import java.util.List;

public class SRPResponse {
    private List<FlightRecordInfo> flightRecordInfos;
    private FareBreakupInfo fareBreakupInfo;

    public WebElement getSelectionBookElement() {
        return selectionBookElement;
    }

    public void setSelectionBookElement(WebElement selectionBookElement) {
        this.selectionBookElement = selectionBookElement;
    }

    private WebElement selectionBookElement;

    public List<FlightRecordInfo> getFlightRecordInfos() {
        return flightRecordInfos;
    }

    public void setFlightRecordInfos(List<FlightRecordInfo> flightRecordInfos) {
        this.flightRecordInfos = flightRecordInfos;
    }

    public FareBreakupInfo getFareBreakupInfo() {
        return fareBreakupInfo;
    }

    public void setFareBreakupInfo(FareBreakupInfo fareBreakupInfo) {
        this.fareBreakupInfo = fareBreakupInfo;
    }

    @Override
    public String toString() {
        return "SRPResponse{" +
                "flightRecordInfos=" + flightRecordInfos +
                ", fareBreakupInfo=" + fareBreakupInfo +
                '}';
    }
}
