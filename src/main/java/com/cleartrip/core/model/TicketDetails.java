package com.cleartrip.core.model;

public class TicketDetails {

    private String airlinePNR;
    private String sector;
    private String status;
    private String gdsPNR;
    private String clazz;
    private String ticket;

    public String getFareBasesCode() {
        return fareBasesCode;
    }

    public void setFareBasesCode(String fareBasesCode) {
        this.fareBasesCode = fareBasesCode;
    }

    private String fareBasesCode;

    public String getAirlinePNR() {
        return airlinePNR;
    }

    public void setAirlinePNR(String airlinePNR) {
        this.airlinePNR = airlinePNR;
    }

    @Override
    public String toString() {
        return "TicketDetails{" +
                "airlinePNR='" + airlinePNR + '\'' +
                ", sector='" + sector + '\'' +
                ", status='" + status + '\'' +
                ", gdsPNR='" + gdsPNR + '\'' +
                ", clazz='" + clazz + '\'' +
                ", ticket='" + ticket + '\'' +
                ", personName='" + personName + '\'' +
                '}';
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGdsPNR() {
        return gdsPNR;
    }

    public void setGdsPNR(String gdsPNR) {
        this.gdsPNR = gdsPNR;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    private String personName;



}
