package com.cleartrip.core.model;

import org.openqa.selenium.WebElement;

import java.util.List;

public class ItineraryResponse {


	private Double mealAndBaggageFare = 0.0d;
	private Double insuranceValue = 0.0d;

	@Override
	public String toString() {
		return "ItineraryResponse{" + "itinaryId='" + itinaryId + '\'' + ", itineraryBtnElement=" + itineraryBtnElement
				+ ", flightRecordInfos=" + flightRecordInfos + ", seatFare=" + seatFare + ", mealFare=" + mealFare
				+ ", baggageFare=" + baggageFare + ", mealAndBaggageFare=" + mealAndBaggageFare + ", insuranceValue="
				+ insuranceValue + ", insuranceSelected=" + insuranceSelected + '}';
	}

	public String getItinaryId() {
		return itinaryId;
	}

	public void setItinaryId(String itinaryId) {
		this.itinaryId = itinaryId;
	}

	private String itinaryId;

	public boolean isInsuranceSelected() {
		return insuranceSelected;
	}

	public void setInsuranceSelected(boolean insuranceSelected) {
		this.insuranceSelected = insuranceSelected;
	}

	private boolean insuranceSelected;

	public WebElement getItineraryBtnElement() {
		return itineraryBtnElement;
	}

	public void setItineraryBtnElement(WebElement itineraryBtnElement) {
		this.itineraryBtnElement = itineraryBtnElement;
	}

	private WebElement itineraryBtnElement;

	public List<FlightRecordInfo> getFlightRecordInfos() {
		return flightRecordInfos;
	}

	public void setFlightRecordInfos(List<FlightRecordInfo> flightRecordInfos) {
		this.flightRecordInfos = flightRecordInfos;
	}

	private List<FlightRecordInfo> flightRecordInfos;

	public Double getSeatFare() {
		return seatFare;
	}

	public void setSeatFare(Double seatFare) {
		this.seatFare = seatFare;
	}

	public Double getMealFare() {
		return mealFare;
	}

	public void setMealFare(Double mealFare) {
		this.mealFare = mealFare;
	}

	public Double getBaggageFare() {
		return baggageFare;
	}

	public void setBaggageFare(Double baggageFare) {
		this.baggageFare = baggageFare;
	}

	private Double seatFare = 0.0d;
	private Double mealFare = 0.0d;

	private Double baggageFare = 0.0d;

	public boolean isCouponApplied() {
		return couponApplied;
	}

	public void setCouponApplied(boolean couponApplied) {
		this.couponApplied = couponApplied;
	}

	private boolean couponApplied;

	public Double getInsuranceValue() {
		return insuranceValue;

	}

	public void setInsuranceValue(Double insuranceValue) {
		this.insuranceValue = insuranceValue;
	}

	public Double getMealAndBaggageFare() {
		return mealAndBaggageFare;

	}

	public void setMealAndBaggageFare(Double mealAndBaggageFare) {
		this.mealAndBaggageFare = mealAndBaggageFare;
	}

}
