package com.cleartrip.core.model;

public class PassangerDetail {

    private String dob="";
    private String passportNumber="";
    private String country="";
    private String expiry="";
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPassportNumber() {
        return passportNumber;
    }



    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return "PassangerDetail{" +
                "dob='" + dob + '\'' +
                ", passportNumber='" + passportNumber + '\'' +
                ", country='" + country + '\'' +
                ", expiry='" + expiry + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }
}
