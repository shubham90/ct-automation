package com.cleartrip.core.model;

public class MiscellaneousResponse {

	private Double couponDiscountValue;
	private boolean couponApplied;
	private Double fareAfterFareChangeAlert;
	private Double fareChangedDiff;
	private String dateStart;
	private String channel;
	private String domain;
	private String environment;
	private boolean isSpecialRT;
	private String owSupplier;
	private String rtSupplier;

	@Override
	public String toString() {
		return "MiscellaneousResponse [couponDiscountValue=" + couponDiscountValue + ", couponApplied=" + couponApplied
				+ ", fareAfterFareChangeAlert=" + fareAfterFareChangeAlert + ", fareChangedDiff=" + fareChangedDiff
				+ ", dateStart=" + dateStart + ", channel=" + channel + ", domain=" + domain + ", environment="
				+ environment + ", isSpecialRT=" + isSpecialRT + ", owSupplier=" + owSupplier + ", rtSupplier="
				+ rtSupplier + "]";
	}

	public Double getCouponDiscountValue() {
		return couponDiscountValue;
	}

	public void setCouponDiscountValue(Double couponDiscountValue) {
		this.couponDiscountValue = couponDiscountValue;
	}

	public boolean isCouponApplied() {
		return couponApplied;
	}

	public void setCouponApplied(boolean couponApplied) {
		this.couponApplied = couponApplied;
	}

	public Double getFareAfterFareChangeAlert() {
		return fareAfterFareChangeAlert;
	}

	public void setFareAfterFareChangeAlert(Double fareAfterFareChangeAlert) {
		this.fareAfterFareChangeAlert = fareAfterFareChangeAlert;
	}

	public Double getFareChangedDiff() {
		return fareChangedDiff;
	}

	public void setFareChangedDiff(Double fareChangedDiff) {
		this.fareChangedDiff = fareChangedDiff;
	}

	public String getDateStart() {
		return dateStart;
	}

	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public boolean isSpecialRT() {
		return isSpecialRT;
	}

	public void setSpecialRT(boolean isSpecialRT) {
		this.isSpecialRT = isSpecialRT;
	}

	public String getOwSupplier() {
		return owSupplier;
	}

	public void setOwSupplier(String owSupplier) {
		this.owSupplier = owSupplier;
	}

	public String getRtSupplier() {
		return rtSupplier;
	}

	public void setRtSupplier(String rtSupplier) {
		this.rtSupplier = rtSupplier;
	}
}
