package com.cleartrip.core.model;

public class FlightRecordInfo {
	
    private String flightCodeAndNumber;
    private String travelClass;
    private String from;
    private String to;
    private String airlineName;
    private String fromTime;
    private String fromDate;
    private String fromAirportTerminalName;
    private String toTime;
    private String toDate;
    private String toAirportTerminalName;

    public String getFareBasesCode() {
        return fareBasesCode;
    }

    public void setFareBasesCode(String fareBasesCode) {
        this.fareBasesCode = fareBasesCode;
    }

    private String fareBasesCode;

    public String getCheckinBaggage() {
        return checkinBaggage;
    }

    public void setCheckinBaggage(String checkinBaggage) {
        this.checkinBaggage = checkinBaggage;
    }

    public String getCabinBaggage() {
        return cabinBaggage;
    }

    public void setCabinBaggage(String cabinBaggage) {
        this.cabinBaggage = cabinBaggage;
    }

    private String checkinBaggage;
    private String cabinBaggage;

    public String getFlightCodeAndNumber() {
        return flightCodeAndNumber;
    }

    public void setFlightCodeAndNumber(String flightCodeAndNumber) {
        this.flightCodeAndNumber = flightCodeAndNumber;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromAirportTerminalName() {
        return fromAirportTerminalName;
    }

    public void setFromAirportTerminalName(String fromAirportTerminalName) {
        this.fromAirportTerminalName = fromAirportTerminalName;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToAirportTerminalName() {
        return toAirportTerminalName;
    }

    public void setToAirportTerminalName(String toAirportTerminalName) {
        this.toAirportTerminalName = toAirportTerminalName;
    }

    public FlightRecordInfo(String flightCodeAndNumber, String travelClass, String from, String to, String airlineName,
                            String fromTime, String fromDate, String fromAirportTerminalName, String toTime, String toDate, String toAirportTerminalName) {
        this.flightCodeAndNumber = flightCodeAndNumber;
        this.travelClass = travelClass;
        this.from = from;
        this.to = to;
        this.airlineName = airlineName;
        this.fromTime = fromTime;
        this.fromDate = fromDate;
        this.fromAirportTerminalName = fromAirportTerminalName;
        this.toTime = toTime;
        this.toDate = toDate;
        this.toAirportTerminalName = toAirportTerminalName;
    }

    public FlightRecordInfo() {
    }

    @Override
    public String toString() {
        return "FlightRecordInfo{" +
                "flightCodeAndNumber='" + flightCodeAndNumber + '\'' +
                ", travelClass='" + travelClass + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", airlineName='" + airlineName + '\'' +
                ", fromTime='" + fromTime + '\'' +
                ", fromDate='" + fromDate + '\'' +
                ", fromAirportTerminalName='" + fromAirportTerminalName + '\'' +
                ", toTime='" + toTime + '\'' +
                ", toDate='" + toDate + '\'' +
                ", toAirportTerminalName='" + toAirportTerminalName + '\'' +
                ", checkinBaggage='" + checkinBaggage + '\'' +
                ", cabinBaggage='" + cabinBaggage + '\'' +
                '}';
    }
}
