package com.cleartrip.core.model;

import org.openqa.selenium.WebElement;

public class PaymentResponse {

    public WebElement getSubmitButton() {
        return submitButton;
    }

    public void setSubmitButton(WebElement submitButton) {
        this.submitButton = submitButton;
    }

    private WebElement submitButton;

}
