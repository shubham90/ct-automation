package com.cleartrip.core.model;

public class Flight {

    private String from;
    private String to;
    private String airline;
    private String number;
    private String stop;
    private String supplier;

    private String arrivalTime;
    private String departTime;

    public boolean isOw() {
        return ow;
    }

    public void setOw(boolean ow) {
        this.ow = ow;
    }

    private boolean ow;


    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }


    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    private String clazz;

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartTime() {
        return departTime;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", airline='" + airline + '\'' +
                ", number='" + number + '\'' +
                ", stop='" + stop + '\'' +
                ", supplier='" + supplier + '\'' +
                ", arrivalTime='" + arrivalTime + '\'' +
                ", departTime='" + departTime + '\'' +
                ", ow=" + ow +
                ", clazz='" + clazz + '\'' +
                '}';
    }
}
