package com.cleartrip.core.constants;

public enum BookingType {
    ONE_WAY,
    ROUND_TRIP,
    MULTI_CITY

}
