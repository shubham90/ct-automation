package com.cleartrip.core.constants;

public enum DriverType {
    FIREFOX,
    CHROME
}
