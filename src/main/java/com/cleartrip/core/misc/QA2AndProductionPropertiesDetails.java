package com.cleartrip.core.misc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class QA2AndProductionPropertiesDetails {

	public static void main(String args[]) throws IOException {
		Map<String, String> map_prod = getProdProperty();
		Map<String, String> map_qa2 = getQA2Property();
		writeLogInFile("Total Number of properties on production : " + map_prod.size() + "\n");
		writeLogInFile("Total number of properties on QA2 : " + map_qa2.size() + "\n\n");
		writeLogInFile(
				"========================================================================================= \n\n");
		extraPropertyOnProd(map_prod, map_qa2);
		writeLogInFile(
				"========================================================================================= \n\n");
		extraPropertyOnQA2(map_prod, map_qa2);
		writeLogInFile(
				"========================================================================================= \n\n");
		compareMatchedPropertiesValue(map_prod, map_qa2);
	}

	public static Map<String, String> getProdProperty() {
		Map<String, String> map_prod = new HashMap<>();
		try {
			URL url = new URL(
					"http://172.21.48.21:9010/common/inspect_resource?beanName=commonCachedProperties&json=false");

			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			String str;
			while ((str = in.readLine()) != null) {
				str = in.readLine().toString();
				if (str.toLowerCase().contains("indigo") || str.toLowerCase().contains("6e")) {
					List<String> keyValue = Arrays.asList(str.split("="));
					if (!(keyValue.size() < 2)) {
						String key = keyValue.get(0);
						String value = keyValue.get(1);
//						System.out.println("Prod : Key : "+key+" , value : "+value);
						map_prod.put(key, value);
					}
				}
			}
			in.close();
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		}
		return map_prod;
	}

	public static Map<String, String> getQA2Property() {
		Map<String, String> map_qa2 = new HashMap<>();
		try {
			URL url = new URL(
					"http://172.17.26.160:8097/common/inspect_resource?beanName=commonCachedProperties&json=false");

			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			String str;
			while ((str = in.readLine()) != null) {
				str = in.readLine().toString();
				if (str.toLowerCase().contains(".indigo.")) {
					List<String> keyValue = Arrays.asList(str.split("="));
					if (!(keyValue.size() < 2)) {
						String key = keyValue.get(0);
						String value = keyValue.get(1);
//						System.out.println("QA2 : Key : "+key+" , value : "+value);
						map_qa2.put(key, value);
					}
				}
			}
			in.close();
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		}
		return map_qa2;
	}

	@SuppressWarnings("rawtypes")
	public static void extraPropertyOnProd(Map<String, String> ext_map_prod, Map<String, String> ext_map_qa2) throws IOException {

		HashSet<String> unionKeys = new HashSet<>(ext_map_prod.keySet());
		unionKeys.addAll(ext_map_qa2.keySet());

		unionKeys.removeAll(ext_map_qa2.keySet());

		writeLogInFile(
				"Number of extra properties on production which are not there on qa2 : " + unionKeys.size() + "\n");
		writeLogInFile("Extra properties present on production are below in key value format.\n");
		Iterator value = unionKeys.iterator();
		while (value.hasNext()) {
			String key = value.next().toString();
			writeLogInFile(key + "=" + ext_map_prod.get(key) + "\n");
		}
	}

	@SuppressWarnings("rawtypes")
	public static void extraPropertyOnQA2(Map<String, String> ext_map_prod, Map<String, String> ext_map_qa2) throws IOException {

		HashSet<String> unionKeys = new HashSet<>(ext_map_prod.keySet());
		unionKeys.addAll(ext_map_qa2.keySet());

		unionKeys.removeAll(ext_map_prod.keySet());

		writeLogInFile(
				"Number of extra properties on qa2 which are not there on production : " + unionKeys.size() + "\n");
		writeLogInFile("Extra properties present on qa2 are below in key value format.\n");
		Iterator value = unionKeys.iterator();
		while (value.hasNext()) {
			String key = value.next().toString();
			writeLogInFile(key + "=" + ext_map_prod.get(key) + "\n");
		}
	}

	@SuppressWarnings("rawtypes")
	public static void compareMatchedPropertiesValue(Map<String, String> ext_map_prod,
			Map<String, String> ext_map_qa2) throws IOException {
		HashSet<String> keysProd = new HashSet<>(ext_map_prod.keySet());
		HashSet<String> keysQA2 = new HashSet<>(ext_map_qa2.keySet());
		writeLogInFile("Following are the qa2 properties and values which are not matching with production.\n");
		Iterator valueQA2 = keysQA2.iterator();
		while (valueQA2.hasNext()) {
			String key = valueQA2.next().toString();
			if (keysProd.contains(key)) {
				if (!(ext_map_qa2.get(key).equalsIgnoreCase(ext_map_prod.get(key)))) {
					writeLogInFile("Not matched property : " + key + " , Prod Value : " + ext_map_prod.get(key)
							+ " , QA2 Value : " + ext_map_qa2.get(key) + "\n");
				}
			}
		}
	}
	
	public static void writeLogInFile(String str) throws IOException {
		System.out.println(str);
		String cdate = getCurrentDateTime("dd_MM_yyyy");
		String fileName = System.getProperty("user.dir") + "/property-report";
		File file=new File(fileName);
		if (!file.exists()) {
			file.mkdir();
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName+"/propertyDetails_"+cdate+".txt", true));
		writer.append('\n');
		writer.append('\n');
		writer.append(str);
		writer.close();
	}
	
	public static String getCurrentDateTime(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		String currenctDateTime = dateFormat.format(date);
		return currenctDateTime;
	}

}
