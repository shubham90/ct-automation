package com.cleartrip.core.utils;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryTest implements IRetryAnalyzer {
	
	private int retrycount = 0;
	private int max_count = 3;
	
	@Override
	public boolean retry(ITestResult result) {
		if(retrycount<max_count){
			System.out.println("Retrying test " + result.getName() + " with status "
                    + getStatus(result.getStatus()) + " for the " + (retrycount+1) + " time(s).");
			retrycount++;
			return true;
		}
		return false;
	}
	
	public String getStatus(int status) {
    	String resultName = null;
    	if(status==1)
    		resultName = "SUCCESS";
    	if(status==2)
    		resultName = "FAILURE";
    	if(status==3)
    		resultName = "SKIP";
		return resultName;
    }

}
