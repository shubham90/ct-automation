package com.cleartrip.core.utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

public class Helper{
	
	static ExtentReports extent;
	static ExtentTest reporter;

    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }
    
    @SuppressWarnings("static-access")
	public void setExtentReportObject(ExtentReports extent,ExtentTest reporter) {
    	this.extent=extent;
    	this.reporter=reporter;
    }
    
    public static ExtentReports getExtentReports() {
    	return extent;
    }
    
    public static ExtentTest getExtentTestr() {
    	return reporter;
    }
}
