package com.cleartrip.core.factory;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ChromeDriverFactory {

	    private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
	    private static final String USER_DIRECTORY = System.getProperty("user.dir");
	    private String CHROME_DRIVER_PATH;

	    private static ChromeDriverFactory chromeDriverFactory = null;

	    public static ChromeDriverFactory getInstance() {
	        if (null == chromeDriverFactory) {
	            chromeDriverFactory = new ChromeDriverFactory();
	        }
	        return chromeDriverFactory;
	    }


	    private Capabilities getCapabilities() {
	        Map<String, Object> prefs = new HashMap<>();
	        prefs.put("profile.default_content_setting_values.notifications", 2);
	        ChromeOptions options = new ChromeOptions();
	        options.setExperimentalOption("prefs", prefs);
	        options.addArguments("--start-maximized");
	        options.addArguments("disable-infobars");
	        DesiredCapabilities chrome = DesiredCapabilities.chrome();
	        chrome.setJavascriptEnabled(true);
	        chrome.setCapability(ChromeOptions.CAPABILITY, options);
	        return chrome;
	    }

	    @SuppressWarnings("deprecation")
		public WebDriver getChromeDriver() {
	        if(System.getProperty("os.name").toLowerCase().contains("windows")){
	            CHROME_DRIVER_PATH=USER_DIRECTORY+"\\src\\test\\resources\\jars\\drivers\\chromedriver.exe";
	        }else if (System.getProperty("os.name").toLowerCase().contains("linux")){
	            CHROME_DRIVER_PATH=USER_DIRECTORY+"/src/test/resources/jars/drivers/chromedriver";
	        }else if (System.getProperty("os.name").toLowerCase().contains("mac")){
	            CHROME_DRIVER_PATH=USER_DIRECTORY+"/src/test/resources/jars/drivers/chromedriver.dmg";
	        }else{
	            CHROME_DRIVER_PATH=USER_DIRECTORY+"\\src\\test\\resources\\jars\\drivers\\chromedriver.exe";
	        }
	        System.setProperty(CHROME_DRIVER_PROPERTY, CHROME_DRIVER_PATH);
	        WebDriver driver = new ChromeDriver(getCapabilities());
	        driver.manage().window().maximize();
	        return driver;
	    }




}
