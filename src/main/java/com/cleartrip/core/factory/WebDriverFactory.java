package com.cleartrip.core.factory;

import org.openqa.selenium.WebDriver;

import com.cleartrip.core.constants.DriverType;

public class WebDriverFactory {

    private static WebDriverFactory webDriverFactory = null;



    public static WebDriverFactory getInstance() {
        if (null == webDriverFactory) {
            webDriverFactory = new WebDriverFactory();
        }
        return webDriverFactory;
    }

    public WebDriver getWebDriver(String browser, String wait) {
        DriverType driverType = DriverType.valueOf(browser.toUpperCase());
        WebDriver driver = null;
        switch (driverType) {
            case CHROME:
                driver = ChromeDriverFactory.getInstance().getChromeDriver();
                break;
            case FIREFOX :
                driver = FirefoxDriverFactory.getInstance().getFirefoxDriver();
                break;
        }
       //driver.manage().timeouts().implicitlyWait(Integer.valueOf(wait), TimeUnit.SECONDS);

        return driver;

    }


}
