package com.cleartrip.core.factory;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

public class FirefoxDriverFactory {

    private static FirefoxDriverFactory firefoxDriverFactory = null;

    public static FirefoxDriverFactory getInstance() {
        if (null == firefoxDriverFactory) {
            firefoxDriverFactory = new FirefoxDriverFactory();
        }
        return firefoxDriverFactory;
    }


    private Capabilities getCapabilities() {
        DesiredCapabilities firefox = DesiredCapabilities.firefox();
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAcceptUntrustedCertificates(true);
        profile.setAssumeUntrustedCertificateIssuer(true);
        firefox.setCapability(FirefoxDriver.PROFILE, profile);
        firefox.setCapability("marionette", true);
        return firefox;
    }

    public WebDriver getFirefoxDriver() {
        return new FirefoxDriver(getCapabilities());
    }
    

}
