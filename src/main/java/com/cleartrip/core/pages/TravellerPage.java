package com.cleartrip.core.pages;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.PassangerDetail;
import com.cleartrip.core.model.TravellerDetail;

public class TravellerPage extends PageBase {

	public TravellerPage(WebDriver driver, long timeout, String url) throws Exception {
		super(driver, timeout, url);
		this.driver = driver;
	}

	@FindBy(how = How.ID, using = "mobileNumber")
	public WebElement mobileNumberElement;

	@FindBy(how = How.ID, using = "travellerBtn")
	public WebElement travellerBtn;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'itineraryDone')]/div[1]/span[contains(@class,'steps')]")
	public WebElement stepsImg;

	@FindBy(how = How.XPATH, using = "//button[@id='priceChangeUpBtn']")
	public WebElement priceChange;

	@FindBy(how = How.ID, using = "whiteWashCont")
	public WebElement LoaderInTraveller;

	private String ADULT_TITLE = "AdultTitle";
	private String ADULT_FIRSTNAME = "AdultFname";
	private String ADULT_LASTNAME = "AdultLname";

	private String ADULT_DOB_DAY = "AdultDobDay";
	private String ADULT_DOB_MONTH = "AdultDobMonth";
	private String ADULT_DOB_YEAR = "AdultDobYear";
	private String ADULT_PASSPORT = "AdultPassport";
	private String ADULT_PASS_COUNTRY = "//input[@data-idfield='adultPPIssuingCountryCode";
	private String ADULT_PASS_ISSUE_COUNTRY = "adultPPIssuingCountry";
	private String ADULT_PASS_EXPIRY_DAY = "AdultPPDay";
	private String ADULT_PASS_EXPIRY_MONTH = "AdultPPMonth";
	private String ADULT_PASS_EXPIRY_YEAR = "AdultPPYear";
	private String ADULT_BIRTH = "//input[@data-idfield='adultBirthCountryCode";
	private String ADULT_NATIONALITY = "//input[@data-idfield='adultNationality";

	private String CHILD_TITLE = "ChildTitle";
	private String CHILD_FIRSTNAME = "ChildFname";
	private String CHILD_LASTNAME = "ChildLname";

	private String INFANT_TITLE = "InfantTitle";
	private String INFANT_FIRSTNAME = "InfantFname";
	private String INFANT_LASTNAME = "InfantLname";

	private String CHILD_DOB_DAY = "ChildDobDay";
	private String CHILD_DOB_MONTH = "ChildDobMonth";
	private String CHILD_DOB_YEAR = "ChildDobYear";
	private String CHILD_PASSPORT = "ChildPassport";
	private String CHILD_PASS_COUNTRY = "//input[@data-idfield='childPPIssuingCountryCode";
	private String CHILD_BIRTH = "//input[@data-idfield='childBirthCountryCode";
	private String CHILD_NATIONALITY = "//input[@data-idfield='childNationality";
	private String CHILD_PASS_ISSUE_COUNTRY = "ChildPPIssuingCountry";


	private String CHILD_PASS_EXPIRY_DAY = "ChildPPDay";
	private String CHILD_PASS_EXPIRY_MONTH = "ChildPPMonth";
	private String CHILD_PASS_EXPIRY_YEAR = "ChildPPYear";

	private String INFANT_DOB_DAY = "InfantDobDay";
	private String INFANT_DOB_MONTH = "InfantDobMonth";
	private String INFANT_DOB_YEAR = "InfantDobYear";
	private String INFANT_PASSPORT = "InfantPassport";
	private String INFANT_PASS_COUNTRY = "//input[@data-idfield='infantPPIssuingCountryCode";
	private String INFANT_BIRTH = "//input[@data-idfield='infantBirthCountryCode";
	private String INFANT_NATIONALITY = "//input[@data-idfield='infantNationality";

	private String INFANT_PASS_EXPIRY_DAY = "InfantPPDay";
	private String INFANT_PASS_EXPIRY_MONTH = "InfantPPMonth";
	private String INFANT_PASS_EXPIRY_YEAR = "InfantPPYear";
	private String INFANT_PASS_ISSUE_COUNTRY = "InfantPPIssuingCountry";
	
	String dateStop="";
	
	@FindBy(how = How.XPATH, using = "//p[@id='payBlockTotal']//strong[contains(@id,'totalFare')]/span[contains(@id,'counter')]")
    public WebElement we_totalAmountOnPaymentPage;
															 
	private Random rnd = new Random();
	private String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public List<PassangerDetail> submit(String mobileNumber, int adults, int child, int infant, int wait,
			List<TravellerDetail> adultDetails, List<TravellerDetail> childDetails, List<TravellerDetail> infantDetails, MiscellaneousResponse miscellaneousResponse)
			throws InterruptedException, IOException {

		waitForURL(travellerPageURL,70);
		waitForElement(stepsImg, wait);
		dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
		writeLoadTimeInFile("Time taken to load traveller section on itinerary page is : " +GetDateTimeDiff(miscellaneousResponse.getDateStart(), dateStop));
		
		Thread.sleep(2000);
		List<PassangerDetail> passangerDetails = submitTravellers(adults, child, infant, adultDetails, childDetails,
				infantDetails);
		mobileNumberElement.clear();
		sendKeys(driver,mobileNumberElement,mobileNumber);
		return passangerDetails;
	}
	
	public void clickOnTravellerContinueButton(IntermediatePage intermediatePage,MiscellaneousResponse miscellaneousResponse, WebElement element_itinerary) throws Exception {
		applyExplicitWaitByWebElementForVisibilityOfElement(driver, travellerBtn, 20);
		clickByJseBasedOnWebElement(driver, travellerBtn);
		 miscellaneousResponse.setDateStart(getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS"));
		
    	intermediatePage.validateFlightChangePopUpAndMove();
    	intermediatePage.flightPriceChangeAlertHandling(miscellaneousResponse);
    	
    	if(element_itinerary.isDisplayed()) {
        	clickByJseBasedOnWebElement(driver, element_itinerary);
        }
    	
    	if(travellerBtn.isDisplayed()) {
        	clickByJseBasedOnWebElement(driver, travellerBtn);
        }
    }

	private Character generateRandomCharacter() {
		return alphabet.charAt(rnd.nextInt(alphabet.length()));

	}

	private String travellerPageURL = "traveller";



	private String getMonthNumber(String monthName) {
		DateTimeFormatter parser = DateTimeFormatter.ofPattern("MMM").withLocale(Locale.ENGLISH);
		TemporalAccessor accessor = parser.parse(monthName);
		String monthNumber=String.valueOf(accessor.get(ChronoField.MONTH_OF_YEAR));
		if(Integer.valueOf(monthNumber) < 10) {
			monthNumber="0"+monthNumber;
		}
		return monthNumber;
	}

	private List<PassangerDetail> submitTravellers(int adults, int child, int infant,
			List<TravellerDetail> adultDetails, List<TravellerDetail> childDetails, List<TravellerDetail> infantDetails)
			throws InterruptedException {
		
		List<PassangerDetail> passangerDetails = new ArrayList<>();
		applyExplicitWaitBybyClassForVisibilityOfElement(driver, By.id(ADULT_FIRSTNAME+1), 30);
		for (int count = 0; count < adults; count++) {
			PassangerDetail passangerDetail = new PassangerDetail();
			Character randomChar=generateRandomCharacter();
			String adultTitle=adultDetails.get(count).getTitle();
			String adultFirstName=adultDetails.get(count).getFirstName() + randomChar;
			String adultLastName=adultDetails.get(count).getLastName();
			
			selectWithVisibleText(driver.findElement(By.name(ADULT_TITLE + (count + 1))),capatilizedFirstLetterOfaWord(adultTitle));
			sendKeys(driver,driver.findElement(By.name(ADULT_FIRSTNAME + (count + 1))),adultFirstName);
			sendKeys(driver,driver.findElement(By.name(ADULT_LASTNAME + (count + 1))),adultLastName);
			passangerDetail.setName(replaceTravellersTitle(adultTitle) + ". " + adultFirstName + " " + adultLastName);

			// For Birth Country
			if (fluentWait(By.xpath( ADULT_BIRTH+ (count + 1) + "']")) != null) {

				sendKeysByKeyboard1(driver,
						By.xpath(ADULT_BIRTH + (count + 1) + "']"),

						"Indi");

				pressEnterKey(driver);
			}

			// For Nationality
			if (fluentWait(By.xpath( ADULT_NATIONALITY+ (count + 1) + "']")) != null) {

				sendKeysByKeyboard1(driver, By.xpath(ADULT_NATIONALITY + (count + 1) + "']"),
						"Indi");

				pressEnterKey(driver);

			}

			if (fluentWait(By.id(ADULT_DOB_DAY + (count + 1))) != null) {
				WebElement adultDay = driver.findElement(By.name(ADULT_DOB_DAY + (count + 1)));
				WebElement adultMonth = driver.findElement(By.name(ADULT_DOB_MONTH + (count + 1)));
				WebElement adultYear = driver.findElement(By.name(ADULT_DOB_YEAR + (count + 1)));

				selectWithVisibleText(adultDay, adultDetails.get(count).getDay());
				selectWithVisibleText(adultMonth, adultDetails.get(count).getMonth());
				selectWithVisibleText(adultYear, adultDetails.get(count).getYear());
				
				passangerDetail.setDob(adultDetails.get(count).getDay()+"/"+getMonthNumber(adultDetails.get(count).getMonth())+"/"+adultDetails.get(count).getYear());

			}

			if (fluentWait(By.name(ADULT_PASSPORT + (count + 1))) != null) {
				WebElement adultPassport = driver.findElement(By.name(ADULT_PASSPORT + (count + 1)));
				sendKeys(driver, adultPassport, adultDetails.get(count).getPassport());
				passangerDetail.setPassportNumber(adultDetails.get(count).getPassport());

				if (fluentWait(By.name(ADULT_PASS_COUNTRY + (count + 1))) != null) {
					sendKeysByKeyboard1(driver,
							By.xpath(ADULT_PASS_COUNTRY + (count + 1) + "']"),

							"Indi");
					pressEnterKey(driver);

				}

				if (fluentWait(By.id(ADULT_PASS_ISSUE_COUNTRY + (count + 1))) != null) {
					passangerDetail.setCountry("IN");

					sendKeysByKeyboard1(driver,
							By.id(ADULT_PASS_ISSUE_COUNTRY + (count + 1) ),

							"Indi");
					pressEnterKey(driver);
				}

				if (fluentWait(By.name(ADULT_PASS_EXPIRY_DAY + (count + 1))) != null) {
					sendKeys(driver, driver.findElement(By.name(ADULT_PASS_EXPIRY_DAY + (count + 1))), adultDetails.get(count).getPassexpiryday());
					sendKeys(driver, driver.findElement(By.name(ADULT_PASS_EXPIRY_MONTH + (count + 1))), adultDetails.get(count).getPassExpiryMonth());
					sendKeys(driver, driver.findElement(By.name(ADULT_PASS_EXPIRY_YEAR + (count + 1))), adultDetails.get(count).getPassExpiryYear());
					passangerDetail.setExpiry(adultDetails.get(count).getPassexpiryday()+"/"+getMonthNumber(adultDetails.get(count).getPassExpiryMonth())+"/"+adultDetails.get(count).getPassExpiryYear());
				}
			}
			passangerDetails.add(passangerDetail);
		}

		for (int ccount = 0; ccount < child; ccount++) {
			PassangerDetail passangerDetail = new PassangerDetail();
			Character randomChar=generateRandomCharacter();
			String childTitle=childDetails.get(ccount).getTitle();
			String childFirstName=childDetails.get(ccount).getFirstName() + randomChar;
			String childLastName=childDetails.get(ccount).getLastName();

			selectWithVisibleText(driver.findElement(By.name(CHILD_TITLE + (ccount + 1))),capatilizedFirstLetterOfaWord(childTitle));
			sendKeys(driver, driver.findElement(By.name(CHILD_FIRSTNAME + (ccount + 1))), childFirstName);
			sendKeys(driver, driver.findElement(By.name(CHILD_LASTNAME + (ccount + 1))), childLastName);
			passangerDetail.setName(childTitle + ". " + childFirstName + " " + childLastName);
			
			selectWithVisibleText(driver.findElement(By.name(CHILD_DOB_DAY + (ccount + 1))), childDetails.get(ccount).getDay());
			selectWithVisibleText(driver.findElement(By.name(CHILD_DOB_MONTH + (ccount + 1))),
					childDetails.get(ccount).getMonth());
			selectWithVisibleText(driver.findElement(By.name(CHILD_DOB_YEAR + (ccount + 1))),
					childDetails.get(ccount).getYear());
			
			passangerDetail.setDob(childDetails.get(ccount).getDay()+"/"+getMonthNumber(childDetails.get(ccount).getMonth())+"/"+childDetails.get(ccount).getYear());

			// For Birth Country
			if (fluentWait(By.xpath(CHILD_BIRTH + (ccount + 1) + "']")) != null) {

				sendKeysByKeyboard1(driver,
						By.xpath(CHILD_BIRTH + (ccount + 1) + "']"),

						"Indi");

				pressEnterKey(driver);

			}

			// For Nationality
			if (fluentWait(By.xpath(CHILD_NATIONALITY + (ccount + 1) + "']")) != null) {

				sendKeysByKeyboard1(driver, By.xpath(CHILD_NATIONALITY + (ccount + 1) + "']"),

						"Indi");

				pressEnterKey(driver);
			}

			if (fluentWait(By.name(CHILD_PASSPORT + (ccount + 1))) != null) {
				WebElement childPassport = driver.findElement(By.name(CHILD_PASSPORT + (ccount + 1)));
				sendKeys(driver, childPassport, childDetails.get(ccount).getPassport());
				passangerDetail.setPassportNumber(childDetails.get(ccount).getPassport());

				if (fluentWait(By.name("childPPIssuingCountry" + (ccount + 1))) != null) {
					passangerDetail.setCountry("IN");

					sendKeysByKeyboard1(driver,
							By.xpath(CHILD_PASS_COUNTRY + (ccount + 1) + "']"),

							"Indi");
					pressEnterKey(driver);
				}

				if (fluentWait(By.id(CHILD_PASS_ISSUE_COUNTRY + (ccount + 1))) != null) {
					passangerDetail.setCountry("IN");

					sendKeysByKeyboard1(driver,
							By.id(CHILD_PASS_ISSUE_COUNTRY + (ccount + 1)),

							"Indi");
					pressEnterKey(driver);

				}

				if (fluentWait(By.name(CHILD_PASS_EXPIRY_DAY + (ccount + 1))) != null) {
					sendKeys(driver, driver.findElement(By.name(CHILD_PASS_EXPIRY_DAY + (ccount + 1))), childDetails.get(ccount).getPassexpiryday());
					sendKeys(driver, driver.findElement(By.name(CHILD_PASS_EXPIRY_MONTH + (ccount + 1))), childDetails.get(ccount).getPassExpiryMonth());
					sendKeys(driver, driver.findElement(By.name(CHILD_PASS_EXPIRY_YEAR + (ccount + 1))), childDetails.get(ccount).getPassExpiryYear());
					passangerDetail.setExpiry(Integer.parseInt(childDetails.get(ccount).getPassexpiryday())-1+"/"+getMonthNumber(childDetails.get(ccount).getPassExpiryMonth())+"/"+childDetails.get(ccount).getPassExpiryYear());
				}
			}
			passangerDetails.add(passangerDetail);
		}

		for (int icount = 0; icount < infant; icount++) {
			PassangerDetail passangerDetail = new PassangerDetail();
			Character randomChar=generateRandomCharacter();
			String infantTitle=infantDetails.get(icount).getTitle();
			String infantFirstName=infantDetails.get(icount).getFirstName() + randomChar;
			String infantLastName=infantDetails.get(icount).getLastName();

			selectWithVisibleText(driver.findElement(By.name(INFANT_TITLE + (icount + 1))),capatilizedFirstLetterOfaWord(infantTitle));
			sendKeys(driver, driver.findElement(By.name(INFANT_FIRSTNAME + (icount + 1))), infantFirstName);
			sendKeys(driver, driver.findElement(By.name(INFANT_LASTNAME + (icount + 1))), infantLastName);
			passangerDetail.setName(infantTitle + ". " + infantFirstName + " " + infantLastName);
			
			selectWithVisibleText(driver.findElement(By.name(INFANT_DOB_DAY + (icount + 1))),
					infantDetails.get(icount).getDay());
			selectWithVisibleText(driver.findElement(By.name(INFANT_DOB_MONTH + (icount + 1))),
					infantDetails.get(icount).getMonth());
			selectWithVisibleText(driver.findElement(By.name(INFANT_DOB_YEAR + (icount + 1))),
					infantDetails.get(icount).getYear());
			passangerDetail.setDob(infantDetails.get(icount).getDay()+"/"+getMonthNumber(infantDetails.get(icount).getMonth())+"/"+infantDetails.get(icount).getYear());

			// For Birth icountry
			if (fluentWait(By.xpath(INFANT_BIRTH + (icount + 1) + "']")) != null) {

				sendKeysByKeyboard1(driver,
						By.xpath(INFANT_BIRTH + (icount + 1) + "']"),

						"Indi");

				pressEnterKey(driver);
			}

			// For Nationality
			if (fluentWait(By.xpath(INFANT_NATIONALITY + (icount + 1) + "']")) != null) {

				sendKeysByKeyboard1(driver, By.xpath(INFANT_NATIONALITY + (icount + 1) + "']"),

						"Indi");

				pressEnterKey(driver);
			}

			if (fluentWait(By.name(INFANT_PASSPORT + (icount + 1))) != null) {
				WebElement infantPassport = driver.findElement(By.id(INFANT_PASSPORT + (icount + 1)));
				infantPassport.sendKeys(infantDetails.get(icount).getPassport());
				passangerDetail.setPassportNumber(infantDetails.get(icount).getPassport());

				if (fluentWait(By.id("infantPPIssuingCountry" + (icount + 1))) != null) {
					passangerDetail.setCountry("IN");

					sendKeysByKeyboard1(driver,
							By.id("infantPPIssuingCountry" + (icount + 1)),
							"Indi");
					pressEnterKey(driver);

				}

				if (fluentWait(By.name(INFANT_PASS_ISSUE_COUNTRY + (icount + 1))) != null) {
					sendKeysByKeyboard1(driver,
							By.xpath(INFANT_PASS_ISSUE_COUNTRY + (icount + 1) + "']"),

							"Indi");
					passangerDetail.setCountry("IN");
					pressEnterKey(driver);

				}

				if (fluentWait(By.name(INFANT_PASS_EXPIRY_DAY + (icount + 1))) != null) {
					sendKeys(driver, driver.findElement(By.name(INFANT_PASS_EXPIRY_DAY + (icount + 1))), infantDetails.get(icount).getPassexpiryday());
					sendKeys(driver, driver.findElement(By.name(INFANT_PASS_EXPIRY_MONTH + (icount + 1))), infantDetails.get(icount).getPassExpiryMonth());
					sendKeys(driver, driver.findElement(By.name(INFANT_PASS_EXPIRY_YEAR + (icount + 1))), infantDetails.get(icount).getPassExpiryYear());
					passangerDetail.setExpiry(Integer.parseInt(infantDetails.get(icount).getPassexpiryday())-1+"/"+getMonthNumber(infantDetails.get(icount).getPassExpiryMonth())+"/"+infantDetails.get(icount).getPassExpiryYear());
				}
			}
			passangerDetails.add(passangerDetail);
		}
		return passangerDetails;
	}
}
