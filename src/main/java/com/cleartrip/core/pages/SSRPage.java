package com.cleartrip.core.pages;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.ItineraryResponse;

public class SSRPage extends PageBase {

	private final Logger log = org.apache.log4j.Logger.getLogger(ItineraryPage.class);

	public SSRPage(WebDriver driver, long timeout) throws Exception {
		super(driver, timeout, null);
		this.driver = driver;
	}

	Double mealFare = 0.0;
	Double seatfare = 0.0;
	Double baggageFare = 0.0;
	Double mealAndBaggageFare = 0.0;
	
	String dateStart="";
    String dateStop="";

	@FindBy(how = How.XPATH, using = "//div[@class='addonSelectedSeats row']//button[@class='action selectAddonButton']")
	public WebElement seatSelection;

	@FindBy(how = How.XPATH, using = "//div[@id='beforeBaggae']//button")
	public WebElement baggageSelection;

	@FindBy(how = How.XPATH, using = "//button[@id='MealButton']")
	public WebElement mealSelection;

	@FindBy(how = How.CSS, using = "a.row.selectAddonListItem")
	public WebElement mealFirstSelection;

	@FindBy(how = How.XPATH, using = "//input[@value='Done']")
	public WebElement mealSelectionDone;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Confirm your')]")
	public WebElement confirmSeat;

	@FindBy(how = How.XPATH, using = "//span[@class='seat occupied']")
	public WebElement seatSelectionImg;

	@FindBy(how = How.XPATH, using = "//ul[@class='hTabs modalTabs']/li/a")
	public List<WebElement> seatSelectionAllDestination;

	private String availableLuxaryXPATH = "//tbody[INDEX]//*[contains(@class,'available luxury')]";

	@FindBy(how = How.ID, using = "seatPrice")
	public WebElement seatFareInPopup;

	@FindBy(how = How.XPATH, using = "//span[@class='addonPriceNumer']")
	public WebElement baggageMealFareInPopup;

	@FindBy(how = How.XPATH, using = "//strong[@id='seatPrice']")
	public WebElement seatPrice;

	@FindBy(how = How.XPATH, using = "//a[@id='close']")
	public WebElement closeSeatPopUp;

	@FindBy(how = How.XPATH, using = "//ul[@class='addonList']/li")
	public List<WebElement> we_baggageSelectionSectorList;

	@FindBy(how = How.ID, using = "insurancePrice")
	public WebElement Insurance;
	
	@FindBy(how = How.XPATH, using="//ul[contains(@class,'travellerList')]/li/a")
	public List<WebElement> we_travellerListForSeatSelection;

	public void selectSeat(int waitTime, ItineraryResponse itineraryResponse) throws InterruptedException {
		applyExplicitWaitByWebElementForVisibilityOfElement(driver, seatSelection, 60);

		if (seatSelection.isDisplayed()) {
			dateStart=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
			seatSelection.click();
			try {
				applyExplicitWaitByWebElementForVisibilityOfElement(driver, seatSelectionImg, 180);

				if (seatSelectionImg.isDisplayed()) {
					dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
					writeLoadTimeInFile("Time taken to load seat selection UI is : " +GetDateTimeDiff(dateStart, dateStop));
					for (int index = 0; index < seatSelectionAllDestination.size(); index++) {
						seatSelectionAllDestination.get(index).click();
						for (int i = 0; i < we_travellerListForSeatSelection.size(); i++) {
							clickByJseBasedOnWebElement(driver, we_travellerListForSeatSelection.get(i));
							driver.findElement(By.xpath(availableLuxaryXPATH.replaceAll("INDEX", (index + 1) + "")))
							.click();
						}
					}
					addLog("Fetching fare from seat map pop-up");

					applyExplicitWaitByWebElementForVisibilityOfElement(driver, seatFareInPopup, 20);
					seatfare = Double.valueOf(replaceCurrencySymbolAndCommas(getText(seatFareInPopup)));
					addLog("Seat Fare is " + seatfare);

					applyExplicitWaitByWebElementForVisibilityOfElement(driver, seatPrice, 20);
					FetchFareInWindowPopup("Seat", seatPrice);
					applyExplicitWaitByWebElementForVisibilityOfElement(driver, confirmSeat, 20);
					confirmSeat.click();
					itineraryResponse.setSeatFare(seatfare);
					driver.switchTo().defaultContent();
				}
			} catch (Exception e) {
				addLog(e.getStackTrace().toString());
				addLog("It was required to select the seat but it is not selected. Since the seat selection UI did not appear on seat selection pop up window.");
				clickByJseBasedOnWebElement(driver, closeSeatPopUp);
			}
		}
		itineraryResponse.setSeatFare(seatfare);
	}

	public void checkSeatSSRAvailability() throws InterruptedException {
		Thread.sleep(5000);
		if (!seatSelection.isDisplayed()) {
			addLog("Refreshing itinerary page to get seat ssr button.");
			driver.navigate().refresh();
			waitForJStoLoad(driver);
		}
	}

	public void selectBaggage(int waitTime, ItineraryResponse itineraryResponse, BookingRequest bookingRequest)
			throws InterruptedException, IOException {
		applyExplicitWaitByWebElementForVisibilityOfElement(driver, baggageSelection, 60);

		if (baggageSelection.isDisplayed()) {
			dateStart=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
			baggageSelection.click();
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("modal_window"));
			waitForJStoLoad(driver);
			if (mealFirstSelection.isDisplayed()) {
				dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
				writeLoadTimeInFile("Time taken to load Baggage selection UI is : " +GetDateTimeDiff(dateStart, dateStop));
				int paxCount = bookingRequest.getAdults() + bookingRequest.getChilds();

				for (int i = 2; i < we_baggageSelectionSectorList.size(); i++) {
					if (driver.findElement(By.xpath("//ul[@class='addonList']/li["+i+"]/ul[contains(@style,'block')]")).isDisplayed()) {
						WebElement we_baggage = driver.findElement(By.xpath("//ul[@class='addonList']/li["+i+"]/ul/li[2]/a[contains(@class,'selectAddonListItem')]"));
						getExplicitWaitObject(driver, 5).until(ExpectedConditions.elementToBeClickable(we_baggage));
						clickByJseBasedOnWebElement(driver, we_baggage);
						addLog("For pax  count "+paxCount+" baggage is selected for the sector : " + driver.findElement(By.xpath("//ul[@class='addonList']/li["+i+"]/ul")).getAttribute("data-sector-info"));
						addLog("Selected baggage info : " + we_baggage.getAttribute("data-name"));
						WebElement we_addbutton = driver.findElement(By.xpath("//ul[@class='addonList']/li["+i+"]/ul/li[2]/div/a[contains(@class,'AddButton')]"));
						for (int j = 0; j < paxCount - 1; j++) {
							clickByJseBasedOnWebElement(driver, we_addbutton);
							Thread.sleep(1000);
						}
						Thread.sleep(2000);
					}
				}
				waitForJStoLoad(driver);
				applyExplicitWaitByWebElementForVisibilityOfElement(driver, baggageMealFareInPopup, 20);
				baggageFare = Double.valueOf(
						replaceCurrencySymbolAndCommas(FetchFareInWindowPopup("Baggage", baggageMealFareInPopup)));
				itineraryResponse.setBaggageFare(baggageFare);
				applyExplicitWaitByWebElementForVisibilityOfElement(driver, mealSelectionDone, 20);
				mealSelectionDone.click();
				System.out.println("Baggage Selection Done....");
				driver.switchTo().defaultContent();

			}
		}
		itineraryResponse.setMealAndBaggageFare(mealFare + baggageFare);
	}

	public void checkBaggageSSRAvailability() throws InterruptedException {
		Thread.sleep(5000);
		if (!baggageSelection.isDisplayed()) {
			addLog("Refreshing itinerary page to get baggage ssr button.");
			driver.navigate().refresh();
			waitForJStoLoad(driver);
		}
	}

	public void selectMeal(int waitTime, ItineraryResponse itineraryResponse, BookingRequest bookingRequest)
			throws InterruptedException, IOException {
		applyExplicitWaitByWebElementForVisibilityOfElement(driver, mealSelection, 60);
		if (mealSelection.isDisplayed()) {
			dateStart=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
			mealSelection.click();
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("modal_window"));
			waitForElement(mealFirstSelection, waitTime);
			waitForJStoLoad(driver);
			if (mealFirstSelection.isDisplayed()) {
				dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
				writeLoadTimeInFile("Time taken to load meal selection UI is : " +GetDateTimeDiff(dateStart, dateStop));
				int paxCount = bookingRequest.getAdults() + bookingRequest.getChilds();

				for (int i = 2; i < we_baggageSelectionSectorList.size(); i++) {
					if (driver.findElement(By.xpath("//ul[@class='addonList']/li["+i+"]/ul[contains(@style,'block')]")).isDisplayed()) {
						WebElement we_meal = driver.findElement(By.xpath("//ul[@class='addonList']/li["+i+"]/ul/li[2]/a[contains(@class,'selectAddonListItem')]"));
						getExplicitWaitObject(driver, 5).until(ExpectedConditions.elementToBeClickable(we_meal));
						clickByJseBasedOnWebElement(driver, we_meal);
						addLog("For pax  count "+paxCount+" meal is selected for the sector : " + driver.findElement(By.xpath("//ul[@class='addonList']/li["+i+"]/ul")).getAttribute("data-sector-info"));
						addLog("Selected meal info : " + we_meal.getAttribute("data-name"));
						WebElement we_addbutton = driver.findElement(By.xpath("//ul[@class='addonList']/li["+i+"]/ul/li[2]/div/a[contains(@class,'AddButton')]"));
						for (int j = 0; j < paxCount - 1; j++) {
							clickByJseBasedOnWebElement(driver, we_addbutton);
							Thread.sleep(1000);
						}
						Thread.sleep(2000);
					}
				}
				waitForJStoLoad(driver);
				applyExplicitWaitByWebElementForVisibilityOfElement(driver, baggageMealFareInPopup, 20);
				mealFare = Double.valueOf(
						replaceCurrencySymbolAndCommas(FetchFareInWindowPopup("Meal", baggageMealFareInPopup)));
				itineraryResponse.setMealFare(mealFare);

				applyExplicitWaitByWebElementForVisibilityOfElement(driver, mealSelectionDone, 20);
				mealSelectionDone.click();
				driver.switchTo().defaultContent();
			}
		}
		itineraryResponse.setMealAndBaggageFare(mealFare + baggageFare);
	}

	public void checkMealSSRAvailability() throws InterruptedException {
		Thread.sleep(5000);
		if (!mealSelection.isDisplayed()) {
			addLog("Refreshing itinerary page to get meal ssr button.");
			driver.navigate().refresh();
			waitForJStoLoad(driver);
		}
	}

	public String FetchFareInWindowPopup(String type, WebElement priceElement) {
		addLog("Fetching Fare for " + type + " from fare details step 1 Window Pop-up ");

		String Fare = "";
		try {
			Fare = getText(priceElement);
			System.out.println("Fare for " + type + " is " + Fare);
		} catch (Exception e) {
			log.error(e.toString());
		}

		return Fare;
	}

	@FindBy(how = How.ID, using = "insurance_box")
	public WebElement insuranceBox;

	@FindBy(how = How.ID, using = "insurance_confirm")
	public WebElement insuranceconfirm;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'fare details')]")
	public WebElement fareDetailsLink;

	@FindBy(how = How.ID, using = "close")
	public WebElement fareDetailsPopupClose;

	@SuppressWarnings("unused")
	public void checkInsuranceBox(boolean isInsuranceRequired, ItineraryResponse itineraryResponse, int waitTime,
			SoftAssert softassert) {
		try {
			if (isInsuranceRequired) {
				applyExplicitWaitByWebElementForVisibilityOfElement(driver, insuranceBox, 40);
				if (insuranceBox.isDisplayed()) {
					applyExplicitWaitByWebElementForVisibilityOfElement(driver, insuranceconfirm, 20);
					if (insuranceBox.isEnabled()) {
						clickByJseBasedOnWebElement(driver, insuranceconfirm);
					} else {
						clickByJseBasedOnWebElement(driver, insuranceBox);
						clickByJseBasedOnWebElement(driver, insuranceconfirm);
					}
					getInsuranceFare(itineraryResponse, waitTime);
					itineraryResponse.setInsuranceSelected(true);
				} else {
					itineraryResponse.setInsuranceSelected(false);
					softassert.assertTrue(insuranceBox.isDisplayed(),
							"The isInsurace required is true, Since insurance box not appeared, the same is not selected.");
				}
			} else {
				if (insuranceBox.isDisplayed()) {
					clickByJseBasedOnWebElement(driver, insuranceBox);
				}
				itineraryResponse.setInsuranceSelected(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getInsuranceFare(ItineraryResponse itineraryResponse, int waitTime) {
		fareDetailsLink.click();
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("modal_window"));
		Double insuranceFare = Double.valueOf(replaceCurrencySymbolAndCommas(getText(this.Insurance)));
		itineraryResponse.setInsuranceValue(insuranceFare);
		addLog("Insurance amount is " + insuranceFare);
		driver.switchTo().defaultContent();
		fareDetailsPopupClose.click();
	}
}
