package com.cleartrip.core.pages;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cleartrip.core.utils.Helper;
import com.vimalselvam.cucumber.listener.Reporter;

public class PageBase extends Helper{

	private static final Logger log = Logger.getLogger(PageBase.class);
	protected WebDriver driver;
	protected WebDriverWait wait = null;
	protected long waitTimeout;
	protected String homePage;
	protected WebDriverWait waiter = null;
	public By Flight_Number = By.xpath("//div[@class='airlineName']/small[@class='flightNumber']");
	public By Source = By.xpath("//li[@class='start']/time//span[@rel='tTooltip']");
	public By Destination = By.xpath("//li[@class='end']/time//span[@rel='tTooltip']");

	@FindBy(how = How.XPATH, using = "//span[@id='step1TotalFare']//span[@id='counter']")
	public WebElement Total_Fare;

	private By getFindByAnno(FindBy anno) {
		//addLog(anno.toString());
		switch (anno.how()) {

		case CLASS_NAME:
			return new By.ByClassName(anno.using());
		case CSS:
			return new By.ByCssSelector(anno.using());
		case ID:
			return new By.ById(anno.using());
		case LINK_TEXT:
			return new By.ByLinkText(anno.using());
		case NAME:
			return new By.ByName(anno.using());
		case PARTIAL_LINK_TEXT:
			return new By.ByPartialLinkText(anno.using());
		case XPATH:
			return new By.ByXPath(anno.using());
		default:
			throw new IllegalArgumentException("Locator not Found : " + anno.how() + " : " + anno.using());
		}
	}

	@SuppressWarnings("rawtypes")
	protected By getElemetLocator(Object obj, String element) throws SecurityException, NoSuchFieldException {
		Class childClass = obj.getClass();
		By locator = null;
		try {
			locator = getFindByAnno(childClass.getDeclaredField(element).getAnnotation(FindBy.class));
		} catch (SecurityException | NoSuchFieldException e) {
			log.equals(e);
			throw e;
		}
		log.debug(locator);
		return locator;
	}

	@SuppressWarnings("deprecation")
	public boolean waitForElement(WebElement element, int timeOutInSeconds) {
		try {
			wait.ignoring(NoSuchElementException.class);
			wait.ignoring(ElementNotVisibleException.class);
			wait.ignoring(StaleElementReferenceException.class);
			// wait.ignoring(ElementNotFoundException.class);
			wait.pollingEvery(1000, TimeUnit.MILLISECONDS);
			wait.until(elementLocated(element));
			wait.withTimeout(Duration.ofSeconds(timeOutInSeconds));
		} catch (Exception e) {
			addLog("Element not found" + e.getMessage());
		}
		return true;
	}

	@SuppressWarnings("deprecation")
	public boolean waitForElement(By by, int timeOutInSeconds) {
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(ElementNotVisibleException.class);
		wait.ignoring(StaleElementReferenceException.class);
		// wait.ignoring(ElementNotFoundException.class);
		wait.pollingEvery(1000, TimeUnit.MILLISECONDS);
		wait.until(elementLocated(by));
		return true;
	}

	private Function<WebDriver, Boolean> elementLocated(final WebElement element) {
		return new Function<WebDriver, Boolean>() {

			@Override
			public Boolean apply(WebDriver driver) {
				log.debug("Waiting for Element : " + element);
				return element.isDisplayed();
			}
		};
	}

	private Function<WebDriver, Boolean> elementLocated(final By by) {
		return new Function<WebDriver, Boolean>() {

			@Override
			public Boolean apply(WebDriver driver) {
				log.debug("Waiting for Element : " + by);
				return ((WebElement) by).isDisplayed();
			}
		};
	}

	public PageBase(WebDriver driver, long waitTimeout, String homePageURL) throws Exception {
		if (driver == null)
			throw new IllegalArgumentException("Driver object is null");

		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
		this.driver = driver;
		wait = new WebDriverWait(driver, waitTimeout);
		waiter = new WebDriverWait(driver, 1);
		this.homePage = homePageURL;
		this.waitTimeout = waitTimeout;

	}

	public boolean checkForTitle(String title) {
		addLog(title);
		if (title == null || title.isEmpty())
			throw new IllegalArgumentException(title);
		return driver.getTitle().trim().contains(title);
	}

	public void selectOptionWithText(WebElement element, String textToSelect, String id, boolean containsMatch) {
		try {
			sendKeysByKeyboard(element, textToSelect);
			Thread.sleep(3000);
			applyExplicitWaitBybyClassForVisibilityOfElement(driver, By.xpath("//*[@id='" + id + "']/li"), 10);
			driver.findElement(By.xpath("//*[@id='" + id + "']/li")).click();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}

	private boolean isMatching(String text, String matchingText, boolean containsMatch) {
		if (containsMatch) {
			return text.toLowerCase().contains(matchingText.toLowerCase());
		}
		return text.toLowerCase().equalsIgnoreCase(matchingText.toLowerCase());
	}

	@SuppressWarnings("deprecation")
	public void wait(WebElement element, int timeOutInSeconds) {
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(ElementNotVisibleException.class);
		wait.ignoring(StaleElementReferenceException.class);
		wait.pollingEvery(250, TimeUnit.MILLISECONDS);
		wait.until(elementLocated(element));
	}

	public void selectWithVisibleText(WebElement webElement, String text) {
		Select oSelect = new Select(webElement);
		oSelect.selectByVisibleText(text);
	}

	@SuppressWarnings("deprecation")
	protected boolean isPresent(String by) {
		try {
			waiter.pollingEvery(250, TimeUnit.MILLISECONDS);
			waiter.until(ExpectedConditions.presenceOfElementLocated(By.xpath(by)));
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return false;

	}

	@SuppressWarnings("deprecation")
	public boolean waitForURL(String url, long timeOut) {
		Wait<WebDriver> waitNew = new FluentWait<WebDriver>(driver).withTimeout(timeOut, TimeUnit.SECONDS)
				.pollingEvery(250, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);
		waitNew.until(ExpectedConditions.urlContains(url));
		return true;
	}

	public String getText(WebElement we) {
		return we.getText().toString().trim().replaceAll("\\<.*?\\>", "");
	}

	public String getTextByInnerHtml(WebElement we) {
		return we.getAttribute("innerHTML").replaceAll("\\<.*?\\>", "");
	}

	public String getAttribute(WebElement we, String attributeName) {
		return we.getAttribute("attributeName").toString().trim().replaceAll("\\<.*?\\>", "");
	}

	public String replaceMultipleSpacesWithOneSpace(String str) {
		return str.replaceAll("\\s{2,}", " ").replaceAll("\n", "").trim();
	}

	public String removeAllTheSpacesFromString(String str) {
		return str.replaceAll("\\s", "").replaceAll("\n", "").trim();
	}

	public void sendKeysByKeyboard(WebElement el, String text) throws InterruptedException {
		el.clear();
		String val = text;
		for (int i = 0; i < val.length(); i++) {
			char c = val.charAt(i);
			String s = new StringBuilder().append(c).toString();
			el.sendKeys(s);
			Thread.sleep(300);
		}
	}

	public void applyExplicitWaitBybyClassForVisibilityOfElement(WebDriver driver, By by, int timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public void pressEnterKey(WebDriver driver2) throws InterruptedException {
		Thread.sleep(4000);
		Actions builder = new Actions(driver2);
		builder.sendKeys(Keys.ENTER).build().perform();
	}

	@SuppressWarnings("unused")
	public boolean elementPresent(WebDriver driver2, By by, int time) {

		boolean elementPresentFlag = false;

		for (int i = 0; i < time; i++) {

			try {

				Thread.sleep(100);

				WebElement we = null;

				if ((we = driver2.findElement(by)) != null) {

					// System.out.println("Element Present" + by);

					elementPresentFlag = true;

					break;

				}

			} catch (Exception e) {

				// System.out.println("Element : " + by + " not present");

			}

		}

		return elementPresentFlag;

	}

	public void sendKeysByKeyboard1(WebDriver driver2, By by, String text) throws InterruptedException {
		WebElement el = driver2.findElement(by);
		el.clear();
		String val = text;
		for (int i = 0; i < val.length(); i++) {
			char c = val.charAt(i);
			String s = new StringBuilder().append(c).toString();
			el.sendKeys(s);
			Thread.sleep(200);
		}
	}

	public void waitForLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}

	@SuppressWarnings("deprecation")
	public WebElement fluentWait(final By locator) {
		try {
			Wait<WebDriver> waitNew = new FluentWait<WebDriver>(driver).withTimeout(300, TimeUnit.MILLISECONDS)
					.pollingEvery(100, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);

			WebElement foo = waitNew.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(locator);
				}
			});
			return foo;

			// return driver.findElement(locator);
		} catch (Exception e) {
			return null;
		}
	}

	public String replaceCurrencySymbolAndCommas(String text) {
		return removeAllTheSpacesFromString(text.trim().replaceAll("Rs.", "").replaceAll(",", "").replaceAll("SAR", "")
				.replaceAll("AED", "").replaceAll("QAR", "").replaceAll("KWD", "").replaceAll("OMR", "")
				.replaceAll("BHD", "").replaceAll("USD", "").replaceAll("-", "").replaceAll("\"", "")
				.replaceAll("\\<.*?\\>", "").replace("₹", ""));
	}

	public void applyExplicitWaitByWebElementForVisibilityOfElement(WebDriver driver, WebElement element, int timeout) {
		//waitForJStoLoad(driver);
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void applyExplicitWaitByListWebElementForVisibilityOfElement(WebDriver driver, List<WebElement> elements,
			int timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.visibilityOfAllElements(elements));
	}

	public void applyExplicitWaitByXpathForVisibilityOfElement(WebDriver driver, String xpath, int timeout) {
		WebElement element = driver.findElement(By.xpath(xpath));
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public WebDriverWait getExplicitWaitObject(WebDriver driver, int timeout) {
		return wait = new WebDriverWait(driver, timeout);
	}

	public void clickByJseBasedOnXpath(WebDriver driver, String xpath) {
		WebElement element = driver.findElement(By.xpath(xpath));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	public void clickByJseBasedOnWebElement(WebDriver driver, WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	public void clickByJseBasedOnBy(WebDriver driver, By by) {
		WebElement element = driver.findElement(by);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	public void sendKeys(WebDriver driver, WebElement element, String text) {
		if (element.isDisplayed()) {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.sendKeys(Keys.CONTROL + "a");
			element.sendKeys(Keys.DELETE);
			element.sendKeys(text);
		}
	}

	public void scrollDownTillEndOfThePage() throws InterruptedException {
		Actions act = new Actions(driver);
		for (int i = 0; i <= 5; i++) {
			Thread.sleep(1000);
			act.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
		}
		Thread.sleep(2000);
	}

	public void scrollUPTillStartOfThePage() throws InterruptedException {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("window.scrollTo(document.body.scrollHeight,0)");
		Thread.sleep(1000);
	}

	public boolean isElementVisible(WebElement element) {
		try {
			return element.isSelected();
		} catch (Exception e) {
			//log.error("Element not found ::: " + e.getMessage());
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	public WebElement fluentWait(final By locator, int timeout) {
		try {
			Wait<WebDriver> waitNew = new FluentWait<WebDriver>(driver).withTimeout(timeout, TimeUnit.SECONDS)
					.pollingEvery(100, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);

			WebElement element = waitNew.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(locator);
				}
			});
			return element;

			// return driver.findElement(locator);
		} catch (Exception e) {
			return null;
		}
	}

	public String replaceTravellersTitle(String title) {
		return title.replaceAll("MR", "Mr").replaceAll("MS", "Ms").replaceAll("MRS", "Mrs");
	}

	public String getDateAfterAddingGivenNumberOfDays(String numberOfDays, String date_format) {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat(date_format);
		c.add(Calendar.DATE, Integer.parseInt(numberOfDays));
		String convertedDate = dateFormat.format(c.getTime());
		return convertedDate;
	}

	public static void addLog(String message) {
		log.info(message);
		Reporter.addStepLog(message);
		// Reporter.addScenarioLog(message);
	}
	
	public static void addTripIdLog(String message) {
		ExtentReports extent=getExtentReports();
		ExtentTest reporter=getExtentTestr();
		reporter.info(message.toString());
//		reporter.info(MarkupHelper.createLabel(message, ExtentColor.BLUE));
		extent.flush();
	}

	public String capatilizedFirstLetterOfaWord(String word) {
		String cap = word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
		return cap;
	}

	public boolean waitForJStoLoad(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 100);

		// wait for jQuery to load
		ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				try {
					return ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
				} catch (Exception e) {
					return true;
				}
			}
		};

		// wait for Javascript to load
		ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};
		return wait.until(jQueryLoad) && wait.until(jsLoad);
	}

	public int getWeekNumberOfTheMonthOfGivenDate(String completeDate, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = df.parse(completeDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int weekMonth = cal.get(Calendar.WEEK_OF_MONTH);
		return weekMonth;
	}

	public int getDayNumberOfTheMonthOfGivenDate(String completeDate, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = df.parse(completeDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int dayNumber = cal.get(Calendar.DAY_OF_MONTH);
		return dayNumber;
	}

	public int getMonthNumberFromGivenDate(String completeDate, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = df.parse(completeDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int monthNumber = cal.get(Calendar.MONTH) + 1;
		return monthNumber;
	}

	public int getYearFromGivenDate(String completeDate, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = df.parse(completeDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		return year;
	}

	public String getMonthNameOfGivenDate(String completeDate, String format) throws ParseException {
		int monthNumber = getMonthNumberFromGivenDate(completeDate, format);
		String monthName = "";
		DateFormatSymbols dfs = new DateFormatSymbols();
		String[] months = dfs.getMonths();
		if (monthNumber >= 0 && monthNumber <= 11) {
			monthName = months[monthNumber - 1];
		} else {
			addLog("The given month number " + monthNumber + " is not valid.");
		}
		return monthName;
	}

	public static String getCurrentDateTime(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		String currenctDateTime = dateFormat.format(date);
		return currenctDateTime;
	}

	public String GetDateTimeDiff(String dateStart, String dateStop) {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");

		Date d1 = null;
		Date d2 = null;
		String diffTimeMessage;
		long diffMilliSeconds = -1;
		long diffSeconds = -1;
		long diffMinutes = -1;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);

			// in milliseconds
			long diff = d2.getTime() - d1.getTime();

			diffMilliSeconds = diff % 1000;
			diffSeconds = diff / 1000 % 60;
			diffMinutes = diff / (60 * 1000) % 60;
		} catch (Exception e) {
			e.printStackTrace();
		}
		diffTimeMessage = diffMinutes + " minutes, " + diffSeconds + " seconds and " + diffMilliSeconds
				+ " milliSeconds";
		return diffTimeMessage;
	}

	public void writeLoadTimeInFile(String str) throws IOException {
		//addLog(str);
		String cdate = getCurrentDateTime("dd_MM_yyyy");
		String fileName = System.getProperty("user.dir") + "/target/load-time-report";
		File file=new File(fileName);
		if (!file.exists()) {
			file.mkdir();
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName+"/loadTime_" + cdate + ".txt", true));
		writer.append('\n');
		writer.append('\n');
		writer.append(str);
		writer.close();
	}
	
	public String getDayNameOfGivenDate(String dateValue,String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		Date date = df.parse(dateValue);
		SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
		String dayName=simpleDateformat.format(date);
		return dayName;
	}
	
	public void enterCardNumberSlowly(WebElement el, String text) throws InterruptedException {
		el.clear();
		String val = text;
		for (int i = 0; i < val.length(); i++) {
			char c = val.charAt(i);
			String s = new StringBuilder().append(c).toString();
			el.sendKeys(s);
			Thread.sleep(1000);
		}
	}
}
