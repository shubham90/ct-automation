package com.cleartrip.core.pages;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.cleartrip.core.model.MiscellaneousResponse;

public class IntermediatePage extends PageBase {

    public IntermediatePage(WebDriver driver, long timeout) throws Exception {
        super(driver, timeout, null);
        this.driver = driver;
    }

    @FindBy(how = How.XPATH, using = "//div[@id='nearByAirPortBookingWarnBox']//input[@value='Okay, continue']")
    private WebElement flightChangePopupContineButton;

    @FindBy(how = How.ID, using = "priceChangeDownBtn")
    private WebElement fareDecreasedAlertHandler;
    
    @FindBy(how = How.ID, using = "priceChangeUpBtn")
    private WebElement fareIncreasedAlertHandler;
    
    @FindBy(how = How.ID, using = "priceChangeIncreasedFare")
    private WebElement totalFareAfterIncreasedFare;
    
    @FindBy(how = How.ID, using = "priceChangeIncreasedDiff")
    private WebElement changedFareIncreasedValuediff;
    
    @FindBy(how = How.ID, using = "priceChangeDecreasedFare")
    private WebElement totalFareAfterDecreasedFare;
    
    @FindBy(how = How.ID, using = "priceChangeDecreasedDiff")
    private WebElement changedFareDecreasedValuediff;

    @FindBy(how = How.ID, using = "username")
    public WebElement emailInItineraryPage;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'LoginContinueBtn')]")
    public WebElement continueButtonInEmailSection;

    @FindBy(how = How.XPATH, using = "//div[@class='floatingMessage']//p/input[@value='English']")
    public WebElement domainAlert;
    
    @FindBy(how = How.ID, using = "itineraryBtn")
	public WebElement itineraryBtnElement;
    
    @FindBy(how = How.ID, using = "travellerBtn")
	public WebElement travellerBtn;
    
    @FindBy(how= How.ID, using="paymentSubmit")
    public WebElement paymentSubmitBtn;

    Double fareAfterFareChangeAlert=0.0;
    Double fareChangedDiff=0.0;
    List<String> domains = Arrays.asList("sa");


    public void validateFlightChangePopUpAndMove() {
    	WebElement element=fluentWait(By.xpath("//div[@id='nearByAirPortBookingWarnBox']//input[@value='Okay, continue']"), 30);
        addLog("Inside flight change popup :: " + flightChangePopupContineButton.isDisplayed());
        if (flightChangePopupContineButton.isDisplayed() && !element.equals(null)) {
        	applyExplicitWaitByWebElementForVisibilityOfElement(driver, flightChangePopupContineButton, 30);
            wait.until(ExpectedConditions.elementToBeClickable(flightChangePopupContineButton));

            flightChangePopupContineButton.click();
            
//            if(itineraryBtnElement.isDisplayed()) {
//            	clickByJseBasedOnWebElement(driver, itineraryBtnElement);
//            }
//            
//            if(travellerBtn.isDisplayed()) {
//            	clickByJseBasedOnWebElement(driver, travellerBtn);
//            }
//            
//            if(paymentSubmitBtn.isDisplayed()) {
//            	clickByJseBasedOnWebElement(driver, paymentSubmitBtn);
//            }
        }
    }

    public void flightPriceChangeAlertHandling(MiscellaneousResponse miscellaneousResponse) throws Exception {
    	WebElement elementDown=fluentWait(By.id("priceChangeDownBtn"), 30);
    	WebElement elementUp=fluentWait(By.id("priceChangeUpBtn"), 30);
        addLog("Price decreased alert came :::::: " + fareDecreasedAlertHandler.isDisplayed());
        addLog("Price increased alert came :::::: " + fareIncreasedAlertHandler.isDisplayed());
        if (fareDecreasedAlertHandler.isDisplayed() && !elementDown.equals(null)) {
        	applyExplicitWaitByWebElementForVisibilityOfElement(driver, fareDecreasedAlertHandler, 30);
            addLog("Continuing with decreased fare");
            // wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("modal_window"));

            fareAfterFareChangeAlert=Double.valueOf(replaceCurrencySymbolAndCommas(getText(totalFareAfterDecreasedFare)));
            miscellaneousResponse.setFareAfterFareChangeAlert(fareAfterFareChangeAlert);
            fareChangedDiff=Double.valueOf(replaceCurrencySymbolAndCommas(getText(changedFareDecreasedValuediff)));
            miscellaneousResponse.setFareChangedDiff(fareChangedDiff);
            
            fareDecreasedAlertHandler.click();
            //driver.switchTo().defaultContent();
            
//            if(itineraryBtnElement.isDisplayed()) {
//            	clickByJseBasedOnWebElement(driver, itineraryBtnElement);
//            }
//            
//            if(travellerBtn.isDisplayed()) {
//            	clickByJseBasedOnWebElement(driver, travellerBtn);
//            }
//            
//            if(paymentSubmitBtn.isDisplayed()) {
//            	clickByJseBasedOnWebElement(driver, paymentSubmitBtn);
//            }
        }else if(fareIncreasedAlertHandler.isDisplayed() && !elementUp.equals(null)) {
        	applyExplicitWaitByWebElementForVisibilityOfElement(driver, fareIncreasedAlertHandler, 30);
            addLog("Continuing with increased fare");
            fareAfterFareChangeAlert=Double.valueOf(replaceCurrencySymbolAndCommas(getText(totalFareAfterIncreasedFare)));
            miscellaneousResponse.setFareAfterFareChangeAlert(fareAfterFareChangeAlert);
            fareChangedDiff=Double.valueOf(replaceCurrencySymbolAndCommas(getText(changedFareIncreasedValuediff)));
            miscellaneousResponse.setFareChangedDiff(fareChangedDiff);
            fareIncreasedAlertHandler.click();
        }
    }

    public void loginVerification(String email,MiscellaneousResponse miscellaneousResponse) throws Exception {
        
        try {
            if (fluentWait(By.id("username")) != null) {
            	writeLoadTimeInFile("Time taken to load 2nd step login on itinerary page is : " +GetDateTimeDiff(miscellaneousResponse.getDateStart(), getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS")));
            	addLog("User is not signed in. Hence logging in with email " + email);
            	applyExplicitWaitByWebElementForVisibilityOfElement(driver, emailInItineraryPage, 20);
                emailInItineraryPage.sendKeys(email);
                wait.until(ExpectedConditions.elementToBeClickable(continueButtonInEmailSection));
                continueButtonInEmailSection.sendKeys(Keys.ENTER);
                miscellaneousResponse.setDateStart(getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS"));

                if (continueButtonInEmailSection.isDisplayed() && continueButtonInEmailSection.isEnabled()) {
                    continueButtonInEmailSection.sendKeys(Keys.ENTER);
                }
            }
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    public void domainHandling() {
        String domain = driver.getCurrentUrl().split("//")[1].split("\\.")[2].replaceAll("/", "");
        if (domains.contains(domain)) {
            domainAlert.click();
        }
    }


}
