package com.cleartrip.core.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cleartrip.core.constants.BookingType;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.FareBreakupInfo;
import com.cleartrip.core.model.FlightRecordInfo;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.SRPResponse;

public class SRPPage extends PageBase {

	List<FlightRecordInfo> flightSearchPageList = null;

	public SRPPage(WebDriver driver, long timeout, String url) throws Exception {
		super(driver, timeout, url);
		this.driver = driver;
	}
	
	String dateStart="";
    String dateStop="";

	@FindBy(how = How.XPATH, using = "//td/button[contains(@class,'booking')]")
	public WebElement oneWayBookElement;

	@FindBy(how = How.XPATH, using = "//section[@class='resultsContainer']//div[contains(@style,'display: block')]//button[@class='booking fRight'][1]")
	public WebElement specialRTBookElement;

	@FindBy(how = How.XPATH, using = "//nav[@class='stops']/ul[contains(@class,'clearFix inline')]/li")
	public List<WebElement> stopsSection;

	@FindBy(how = How.XPATH, using = "//nav[@class='stops']/ul[contains(@class,'clearFix inline')]/li/input[@value='0']/following-sibling::label")
	public WebElement zeroStop;

	@FindBy(how = How.XPATH, using = "//nav[@class='stops']/ul[contains(@class,'clearFix inline')]/li/input[@value='1']/following-sibling::label")
	public WebElement oneStop;

	@FindBy(how = How.XPATH, using = "//nav[@class='stops']/ul[contains(@class,'clearFix inline')]/li/input[@value='2']/following-sibling::label")
	public WebElement twoStop;

	@FindBy(how = How.XPATH, using = "//nav[@class='stops']/ul[contains(@class,'clearFix inline')]/li/input[@value='3']/following-sibling::label")
	public WebElement threeStop;

	@FindBy(how = How.XPATH, using = "//ul[@class='listView flights']/li[1]")
	public WebElement flightRecord;

	public String fareRecord = "//li[contains(@class,'listItem')][index]//td[@class='tabsContainer']//li[2]/a";

	@FindBy(how = How.XPATH, using = "//li[contains(@class,'open opened')]//div[@class='itinerary']//ul")
	public List<WebElement> flightDetailsContainer;

	String totalSolutins = "//ul[@class='listView flights']/li//ancestor::ul";
	String flight_Container = "//li[contains(@class,'open opened')]//div[@class='itinerary']//ul[";
	String flight_Code_Number = "]//div[@class='airlineName']/small[@class='flightNumber']";
	String flight_From = "]//li[@class='start']/time//span[@rel='tTooltip']";
	String flight_To = "]//li[@class='end']/time//span[@rel='tTooltip']";
	String airline_name = "]//div[@class='airlineName']/span[@class='name']";
	String from_time = "]//li[@class='start']/time//strong";
	String from_date = "]//li[@class='start']/time//span[@class='travelDate']";
	String from_airport_terminal_name = "]//li[@class='start']/small[@class='terminal']";
	String to_time = "]//li[@class='end']/time//strong";
	String to_date = "]//li[@class='end']/time//span[@class='travelDate']";
	String to_airport_terminal_name = "]//li[@class='end']/small[@class='terminal']";
	String travel_class = "]//div[@class='airlineName']/small[@class='travelClass']";

	String opened_flight_fare_rule_section = "//li[contains(@class,'open opened')]//div[@class='fareDetails row']";
	String base_fare = "//dd[@class='first fareBreakupBFare']";
	String taxes = "//dd[@class='otherChargesVal fareBreakupTaxFee']";
	String refundable_message = "//span[contains(@class,'status')]";
	String cancellation_fee = "//dt[contains(text(),'Cancellation')]//following::dl/dd/span/span";
	String change_flight_fee = "//dt[contains(text(),'Change')]//following::dl/dd/span/span";

	String baggage_info = "//li[contains(@class,'open opened')]//li[@class='baggageInfoTab']/a";
	String opened_flight_baggage_info_section = "//li[contains(@class,'open opened')]//div[@class='baggage']";
	String opened_flight_check_in_baggage_info = "//span[contains(text(),'Check-in')]/strong";
	String opened_flight_cabin_baggage_info = "//span[contains(text(),'Cabin')]/strong";

	@FindBy(how = How.XPATH, using = "//div[@class='searchSummary']/small")
	public WebElement header_pax_details;

	@FindBy(how = How.ID, using = "BaggageBundlingTemplate")
	public WebElement ItineraryAmount;

	@FindBy(how = How.XPATH, using = "//li[contains(@class,'open opened')]//a[contains(.,'Fare rules')]")
	public WebElement fareRules;

	@FindBy(how = How.XPATH, using = "//a[contains(.,'Fare rules')]")
	public WebElement FareRules;

	public String baseFare = "//li[contains(@class,'listItem')][index]//div[@class='fareBreakupContainer']//dd[@class='first fareBreakupBFare']";

	public String tax = "//li[contains(@class,'listItem')][index]//div[@class='fareBreakupContainer']//dd[@class='otherChargesVal fareBreakupTaxFee']";

	@FindBy(how = How.XPATH, using = "//strong[@class='fareBreakUpTotal']")
	public WebElement TotalFareInFareRules;

	@FindBy(how = How.XPATH, using = "//li[contains(@class,'listItem')]")
	public List<WebElement> flights;

	public String itinerary = "//li[contains(@class,'listItem')][index]//div[@class='itinerary']";

	public String tripDetails = "//li[contains(@class,'listItem')][index]//div[@class='itinerary'][itinind]//ul";

	public String flightNumber = "//li[contains(@class,'listItem')][index]//div[@class='itinerary'][itinind]//ul[indx]//div[@class='airlineName']//small[@class='flightNumber']";
	public String flightName = "//li[contains(@class,'listItem')][index]//div[@class='itinerary'][itinind]//ul[indx]//div[@class='airlineName']//span[@class='name']";

	public String flightClass = "//li[contains(@class,'listItem')][index]//div[@class='itinerary'][itinind]//ul[indx]//div[@class='airlineName']//small[@class='travelClass']";

	public String flightFromDetail = "//li[contains(@class,'listItem')][index]//div[@class='itinerary'][itinind]//ul[indx]//li[@class='start']/time//span[@rel='tTooltip']";

	public String flightToDetail = "//li[contains(@class,'listItem')][index]//div[@class='itinerary'][itinind]//ul[indx]//li[@class='end']/time//span[@rel='tTooltip']";

	public String flightFromTravelDate = "//li[contains(@class,'listItem')][index]//div[@class='itinerary'][itinind]//ul[indx]//li[@class='start']/time//span[@class='travelDate']";

	public String flightToTravelDate = "//li[contains(@class,'listItem')][index]//div[@class='itinerary'][itinind]//ul[indx]//li[@class='end']/time//span[@class='travelDate']";

	public String flightFromTravelTime = "//li[contains(@class,'listItem')][index]//div[@class='itinerary'][itinind]//ul[indx]//li[@class='start']/time//strong";

	public String flightToTravelTime = "//li[contains(@class,'listItem')][index]//div[@class='itinerary'][itinind]//ul[indx]//li[@class='end']/time//strong";

	public String flightSegmentInfo = "//li[contains(@class,'listItem')][index]//table[contains(@class,'resultUnit flightDetailsLink')]/tbody[@data-fk]";

	public String oneWayBookElementSelected = "//li[contains(@class,'listItem')][index]//table[contains(@class,'resultUnit flightDetailsLink')]//td/button[contains(@class,'booking')]";
	public String selectFlightDetails = "//li[contains(@class,'listItem')][index]//td[@class='tabsContainer']//li[1]/a";

	private String SPLIT_FLIGHT_SELECTION_ONWARD = "CT.masterJson.get().mapping[0][$('.listViewNav .selected')[0].getAttribute('data-id')].summary.fk";
	private String SPLIT_FLIGHT_SELECTION_RETRUN = "CT.masterJson.get().mapping[1][$('.listViewNav .selected')[1].getAttribute('data-id')].summary.fk";
	private String SPLIT_SELECTION_ONWARD = "//div[@data-leg='1']//ul[@class='listView flights']//li[index]//tbody[contains(@class,'segment')]//input";
	private String SPLIT_SELECTION_RETURN = "//div[@data-leg='2']//ul[@class='listView flights']//li[index]//tbody[contains(@class,'segment')]//input";

	@FindBy(how = How.XPATH, using = "//div[@data-leg='1']//ul[@class='listView flights']//li")
	public List<WebElement> flightsSelectionOnWard;

	@FindBy(how = How.XPATH, using = "//div[@data-leg='2']//ul[@class='listView flights']//li")
	public List<WebElement> flightsSelectionReturn;

	@FindBy(how = How.XPATH, using = "//a[@class='fareBreakUpLink weak']")
	public WebElement fareBreakupSplitRT;

	@FindBy(how = How.XPATH, using = "//div[@class='tipsyInner']//dd[contains(@class,'first')]//span ")
	public WebElement baseFareSplitRT;

	@FindBy(how = How.XPATH, using = "//div[@class='tipsyInner']//dd[contains(@class,'otherChargesVal')]//span")
	public WebElement totalTaxesSplitRT;

	@FindBy(how = How.XPATH, using = "//div[@class='tipsyInner']//dd//strong//span ")
	public WebElement totalSplitRT;

	@FindBy(how = How.XPATH, using = "//div[@data-block-type='airlines']//input")
	public List<WebElement> airlines;

	@FindBy(how = How.XPATH, using = "//div[@data-block-type='airlines']//input[@value='MULTI']")
	public WebElement multi_airlines;

	@FindBy(how = How.CSS, using = "input[type='checkbox'][value='0']:checked")
	public WebElement zeroSelected;

	@FindBy(how = How.CSS, using = "input[type='checkbox'][value='1']:checked")
	public WebElement oneSelected;

	@FindBy(how = How.CSS, using = "input[type='checkbox'][value='2']:checked")
	public WebElement twoSelected;

	@FindBy(how = How.CSS, using = "input[type='checkbox'][value='3']:checked")
	public WebElement threeSelected;

	@FindBy(how = How.XPATH, using = "//nav[@class='nearbyairportsDest']//input")
	public List<WebElement> nearBys;

	By by_specialPriceInSplitScreen = By.xpath("//p[@class='rtDiscount']/small[@class='strikeOut']");

	public String noMealsIndicator = "//li[contains(@class,'listItem')][index]//table[contains(@class,'resultUnit flightDetailsLink')]/tbody[@data-fk]//span[contains(@class,'noMeals')]";

	public SRPResponse book(int wait, boolean isSplitRT, boolean isConnected, String owSupplier, String owAirline,
			String rtSupplier, String rtAirline, String owConnected, String rtConnected, BookingType bookingType,
			String isNearbyFiltring, BookingRequest bookingRequest, boolean isMealFeatureTesting, boolean isNoMealCase,
			boolean isSpecialRT) throws InterruptedException, IOException {
		
		dateStart=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
		addLog("bookingType : " + bookingType + " , IsSplitRT : " + isSplitRT + " isMealFeatureTesting :  " + isMealFeatureTesting + " isNoMealCase : "
				+ isNoMealCase);

		WebElement element = oneWayBookElement;
		if (isSplitRT) {
			element = specialRTBookElement;
		}

		applyExplicitWaitByWebElementForVisibilityOfElement(driver, element, 200);
		dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
		writeLoadTimeInFile("Time taken to load search page is : " +GetDateTimeDiff(dateStart, dateStop));
		
		airlinesSelection(owAirline, rtAirline, owConnected, rtConnected, isConnected, bookingType);
		nearByFlitering(isNearbyFiltring, bookingRequest.getTo());
		int size = stopsSection.size();
		if (isConnected) {
			if (isElementVisible(zeroSelected)) {
				zeroStop.click();
			}

			if (size >= 2) {
				if (!isElementVisible(oneSelected) && oneStop.isDisplayed()) {
					oneStop.click();
				}
			}

			if (size >= 3) {

				if (!isElementVisible(twoSelected) && twoStop.isDisplayed()) {
					twoStop.click();
				}
			}

			if (size >= 4) {
				if (!isElementVisible(threeSelected) && threeStop.isDisplayed()) {
					threeStop.click();
				}
			}

			scrollDownTillEndOfThePage();
			scrollUPTillStartOfThePage();
		} else {
			if (!isElementVisible(zeroSelected) && zeroStop.isDisplayed()) {
				zeroStop.click();
			}

			if (size >= 2) {
				if (isElementVisible(oneSelected) && oneStop.isDisplayed()) {
					oneStop.click();
				}
			}

			if (size >= 3) {
				if (isElementVisible(twoSelected) && twoStop.isDisplayed()) {
					twoStop.click();
				}
			}

			if (size >= 4) {
				if (isElementVisible(threeSelected) && threeStop.isDisplayed()) {
					threeStop.click();
				}
			}

		}

		SRPResponse srpResponse = new SRPResponse();

		if (!isSplitRT) {
			Integer flightIndexInSplitScreen = selectFlightIndex(owSupplier, owAirline, rtSupplier, rtAirline,
					owConnected, rtConnected, isConnected, bookingType, isMealFeatureTesting, isNoMealCase);
			addLog("Matching flight found Index flag value for non Split screen i.e solution view: "
					+ flightIndexInSplitScreen);
			Assert.assertFalse(flightIndexInSplitScreen < 0, "Flight selection not found");
			List<FlightRecordInfo> flightRecordInfosInSplitScreen = captureFareDetails(wait, flightIndexInSplitScreen,
					bookingType);
//			addLog(flightRecordInfosInSplitScreen.toString());
//			addLog(flightIndexInSplitScreen.toString());
			FareBreakupInfo fareBreakupInfoInSplitScreen = fetchFareBreakupInfo(flightIndexInSplitScreen);

			srpResponse.setFareBreakupInfo(fareBreakupInfoInSplitScreen);
			srpResponse.setFlightRecordInfos(flightRecordInfosInSplitScreen);
			addLog(fareBreakupInfoInSplitScreen.toString());
			srpResponse.setSelectionBookElement(driver.findElement(
					By.xpath(oneWayBookElementSelected.replaceAll("index", (flightIndexInSplitScreen + 1) + ""))));
		} else {
			Integer flightIndexInSplitScreen = selectFlightIndexSRT(owSupplier, owAirline, rtSupplier, rtAirline,
					owConnected, rtConnected, isConnected, bookingType, isSpecialRT);
			addLog("Matching flight found Index flag value for Split screen: " + flightIndexInSplitScreen);
			Assert.assertFalse(flightIndexInSplitScreen < 0, "Flight selection not found");

			FareBreakupInfo fareBreakupInfoInSplitScreen = fetchFareBreakupInfoSplitScreen(flightIndexInSplitScreen);
			srpResponse.setFareBreakupInfo(fareBreakupInfoInSplitScreen);
			addLog(fareBreakupInfoInSplitScreen.toString());
			srpResponse.setSelectionBookElement(specialRTBookElement);

		}
		return srpResponse;

	}

	private boolean isFlightMatching(String supplier, String airline, String data, String connected,
			boolean isConnected) {

		if (supplier.equalsIgnoreCase("X") && airline.equalsIgnoreCase("X")) {
			return true;
		}
		String[] airlines = data.split("\\$\\$");
		Set<String> suppliers = new HashSet<>();
		String airlineName = null;
		Set<String> connectedAirLines = new HashSet<>();
		int index = 0;
		for (String air : airlines) {
			String supplierAirline = air.split("-")[0];
			String supplierName = supplierAirline.substring(0, supplierAirline.lastIndexOf("_"));
			String arilineName = supplierAirline.substring(supplierAirline.lastIndexOf("_") + 1,
					supplierAirline.length());
			suppliers.add(supplierName);
			if (index == 0) {
				airlineName = arilineName;
			} else {
				connectedAirLines.add(arilineName);
			}
			index++;
		}
		return validateMatchingConditions(suppliers, connectedAirLines, airlineName, supplier, airline, connected,
				isConnected);
	}

	private boolean validateMatchingConditions(Set<String> suppliers, Set<String> connectedAirLines, String airlineName,
			String supplier, String airline, String connected, boolean isConnected) {

		if (!supplier.equalsIgnoreCase("X") && (suppliers.size() > 1 || !suppliers.contains(supplier))) {
			return false;
		} else if (!airline.equalsIgnoreCase("X") && !airlineName.equalsIgnoreCase(airline)) {
			return false;
		} else if (isConnected && !connected.equalsIgnoreCase("X") && (!connectedAirLines.contains(connected))) {
			return false;
		}
		return true;
	}

	private Integer selectFlightIndex(String owSupplier, String owAirline, String rtSupplier, String rtAirline,
			String owConnected, String rtConnected, boolean isConnected, BookingType bookingType,
			boolean isMealFeatureTesting, boolean isNoMealCase) {

		for (int index = 0; index < flights.size(); index++) {

			boolean isNoMealBooking = false;
			String flightSegmentInfoData = driver
					.findElement(By.xpath(flightSegmentInfo.replaceAll("index", (index + 1) + "")))
					.getAttribute("data-fk");
			if (isMealFeatureTesting) {
				if (fluentWait(By.xpath(noMealsIndicator.replaceAll("index", (index + 1) + ""))) != null && driver
						.findElement(By.xpath(noMealsIndicator.replaceAll("index", (index + 1) + ""))).isDisplayed()) {
					isNoMealBooking = true;
				}

			}
			// addLog("data is :::: " + flightSegmentInfoData);
			addLog("isMealFeatureTesting : " + isMealFeatureTesting + "   isNoMealBooking :   " + isNoMealBooking
					+ "  isNoMealCase: " + isNoMealCase);
			String[] datas = flightSegmentInfoData.split("\\|\\|");
			int indexVal = 0;
			boolean isVerified = false;
			for (String data : datas) {
				// addLog("data is :::: " + data);

				if (indexVal == 0) {

					boolean result = isFlightMatching(owSupplier, owAirline, data, owConnected, isConnected);

					if (!result) {
						break;
					}
					if (isMealFeatureTesting) {
						if (isNoMealBooking != isNoMealCase) {
							break;
						}
					}
					if (bookingType.equals(BookingType.ONE_WAY)) {
						isVerified = true;
						break;

					}
				} else if (indexVal == 1) {

					boolean result = isFlightMatching(rtSupplier, rtAirline, data, rtConnected, isConnected);
					if (!result) {
						isVerified = false;

						break;
					}
					if (isMealFeatureTesting) {
						if (isNoMealBooking != isNoMealCase) {
							break;
						}
					}

					if (result) {
						isVerified = true;

						break;
					}
				}

				indexVal++;
			}

			if (isVerified) {
				addLog("Selected ::::: " + index);
				addLog("Selected  data ::::: " + flightSegmentInfoData);
				return index;

			}

		}
		return -1;

	}

	private List<FlightRecordInfo> captureFareDetails(int waitTimeout, Integer index, BookingType bookingType) {
		index = index + 1;
		WebElement farelink = driver.findElement(By.xpath(selectFlightDetails.replaceAll("index", (index) + "")));
		if (bookingType.equals(BookingType.ONE_WAY)) {
			Actions builder = new Actions(driver);
			builder.moveToElement(farelink).build().perform();
			wait.until(ExpectedConditions.elementToBeClickable(farelink));
		}

		Actions builder = new Actions(driver);
		builder.moveToElement(farelink).build().perform();
		builder.moveToElement(farelink).build().perform();
		farelink.click();

		// addLog("capture 1 ::: " + selectFlightDetails.replaceAll("index", (index) +
		// ""));

		List<FlightRecordInfo> flightRecordInfos = new ArrayList<FlightRecordInfo>();

		List<WebElement> itineraryDetails = driver.findElements(By.xpath(itinerary.replaceAll("index", (index) + "")));
		if (itineraryDetails.size() == 0) {
			farelink.click();
			itineraryDetails = driver.findElements(By.xpath(itinerary.replaceAll("index", (index) + "")));
		}

//		addLog("capture 3 ::: " + itinerary.replaceAll("index", (index) + ""));
		int itinind = 1;
		for (WebElement itineraryDetail : itineraryDetails) {
			List<WebElement> tripsDetails = itineraryDetail.findElements(
					By.xpath(tripDetails.replaceAll("index", (index) + "").replaceAll("itinind", itinind + "")));
//			addLog("capture 4 ::: "
//					+ tripDetails.replaceAll("index", (index) + "").replaceAll("itinind", itinind + ""));
			int indx = 1;
			for (WebElement tripsDetail : tripsDetails) {
//				addLog("capture 5 ::: " + flightNumber.replaceAll("index", (index) + "").replaceAll("indx", (indx) + "")
//						.replaceAll("itinind", itinind + ""));
//				addLog("capture 6 ::: " + flightFromDetail.replaceAll("index", (index) + "")
//						.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + ""));
//				addLog("capture 7 ::: " + flightToDetail.replaceAll("index", (index) + "")
//						.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + ""));
				waitForElement(driver.findElement(By.xpath(flightNumber.replaceAll("index", (index) + "")
						.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + ""))), 10);

				String flightNumberData = tripsDetail
						.findElement(By.xpath(flightNumber.replaceAll("index", (index) + "")
								.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + "")))
						.getText();
				String flightFromDetailData = tripsDetail
						.findElement(By.xpath(flightFromDetail.replaceAll("index", (index) + "")
								.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + "")))
						.getText();
				String flightToDetailData = tripsDetail
						.findElement(By.xpath(flightToDetail.replaceAll("index", (index) + "")
								.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + "")))
						.getText();
				String fromAirportTerminalName = tripsDetail
						.findElement(By.xpath(flightFromDetail.replaceAll("index", (index) + "")
								.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + "")))
						.getAttribute("title");
				String toAirportTerminalName = tripsDetail
						.findElement(By.xpath(flightToDetail.replaceAll("index", (index) + "")
								.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + "")))
						.getAttribute("title");
				String fromAirportTravelTime = tripsDetail
						.findElement(By.xpath(flightFromTravelTime.replaceAll("index", (index) + "")
								.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + "")))
						.getText();
				String toAirportTravelTime = tripsDetail
						.findElement(By.xpath(flightToTravelTime.replaceAll("index", (index) + "")
								.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + "")))
						.getText();
				String fromAirportTravelDate = tripsDetail
						.findElement(By.xpath(flightFromTravelDate.replaceAll("index", (index) + "")
								.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + "")))
						.getText();
				String toAirportTravelDate = tripsDetail
						.findElement(By.xpath(flightToTravelDate.replaceAll("index", (index) + "")
								.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + "")))
						.getText();
				String flightN = tripsDetail.findElement(By.xpath(flightName.replaceAll("index", (index) + "")
						.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + ""))).getText();
				String flightClazz = tripsDetail.findElement(By.xpath(flightClass.replaceAll("index", (index) + "")
						.replaceAll("indx", (indx) + "").replaceAll("itinind", itinind + ""))).getText();
				flightRecordInfos.add(new FlightRecordInfo(flightNumberData, flightClazz, flightFromDetailData,
						flightToDetailData, flightN, fromAirportTravelTime, fromAirportTravelDate,
						fromAirportTerminalName, toAirportTravelTime, toAirportTravelDate, toAirportTerminalName));
				indx++;
			}
			itinind++;
		}
		return flightRecordInfos;
	}

	public FareBreakupInfo fetchFareBreakupInfo(int index) {
		WebElement fareBreakUp = driver.findElement(By.xpath(fareRecord.replaceAll("index", (index + 1) + "")));

		wait.until(ExpectedConditions.elementToBeClickable(fareBreakUp));

		fareBreakUp.click();

		FareBreakupInfo fareBreakupInfo = new FareBreakupInfo();
		fareBreakupInfo.setBaseFare(Double.valueOf(replaceCurrencySymbolAndCommas(
				driver.findElement(By.xpath(baseFare.replaceAll("index", (index + 1) + ""))).getText())));
		fareBreakupInfo.setTax(Double.valueOf(replaceCurrencySymbolAndCommas(
				driver.findElement(By.xpath(tax.replaceAll("index", (index + 1) + ""))).getText())));
		fareBreakupInfo.setTotalFare(Double.valueOf(replaceCurrencySymbolAndCommas(TotalFareInFareRules.getText())));

		return fareBreakupInfo;
	}

	private Integer selectFlightIndexSRT(String owSupplier, String owAirline, String rtSupplier, String rtAirline,
			String owConnected, String rtConnected, boolean isConnected, BookingType bookingType, boolean isSpecialRT)
			throws InterruptedException {

		JavascriptExecutor js = (JavascriptExecutor) driver;

		addLog("Onward flight size: " + flightsSelectionOnWard.size() + "  Return flight size: "
				+ flightsSelectionReturn.size());
		for (int index = 1; index < (flightsSelectionOnWard.size() + 1); index++) {

			WebDriverWait wait = getExplicitWaitObject(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(By.xpath(SPLIT_SELECTION_ONWARD.replaceAll("index", index + "")))));
			if (!driver.findElement(By.xpath(SPLIT_SELECTION_ONWARD.replaceAll("index", index + ""))).isSelected()) {
				clickByJseBasedOnWebElement(driver,
						driver.findElement(By.xpath(SPLIT_SELECTION_ONWARD.replaceAll("index", index + ""))));
			}

			String onwardFlights = js
					.executeScript("return " + SPLIT_FLIGHT_SELECTION_ONWARD.replaceAll("index", index + ""))
					.toString();

			boolean result = isFlightMatching(owSupplier, owAirline, onwardFlights, owConnected, isConnected);

			if (result) {
				for (int index1 = 1; index1 < (flightsSelectionReturn.size() + 1); index1++) {

					wait.until(ExpectedConditions.elementToBeClickable(
							driver.findElement(By.xpath(SPLIT_SELECTION_RETURN.replaceAll("index", index1 + "")))));
					if (!driver.findElement(By.xpath(SPLIT_SELECTION_RETURN.replaceAll("index", index1 + "")))
							.isSelected()) {
						clickByJseBasedOnWebElement(driver,
								driver.findElement(By.xpath(SPLIT_SELECTION_RETURN.replaceAll("index", index1 + ""))));
					}
					String returnFlights = js
							.executeScript("return " + SPLIT_FLIGHT_SELECTION_RETRUN.replaceAll("index", index1 + ""))
							.toString();
					result = isFlightMatching(rtSupplier, rtAirline, returnFlights, rtConnected, isConnected);

					if (result) {

						if (isSpecialRT) {
							while (index == 1 || index1 == 1) {
								addLog("Checking the availability of special rt solution.");
								break;
							}
							
							if(fluentWait(by_specialPriceInSplitScreen, 1) == null) {
								continue;
							}else {
								addLog("Special RT solutins found.");
								return index;
							}
						} else {
							return index;
						}
					}
				}
			}
		}
		return -1;

	}

	public FareBreakupInfo fetchFareBreakupInfoSplitScreen(int index) {

		applyExplicitWaitByWebElementForVisibilityOfElement(driver, fareBreakupSplitRT, 20);
		WebDriverWait wait = getExplicitWaitObject(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(fareBreakupSplitRT));
		clickByJseBasedOnWebElement(driver, fareBreakupSplitRT);

//		addLog("split base fare : " + baseFareSplitRT.getAttribute("data-pr"));
//		addLog("split Taxes : " + totalTaxesSplitRT.getAttribute("data-pr"));
//		addLog("split total fare : " + totalSplitRT.getAttribute("data-pr"));

		FareBreakupInfo fareBreakupInfo = new FareBreakupInfo();
		fareBreakupInfo
				.setBaseFare(Double.valueOf(replaceCurrencySymbolAndCommas(baseFareSplitRT.getAttribute("data-pr"))));
		fareBreakupInfo
				.setTax(Double.valueOf(replaceCurrencySymbolAndCommas(totalTaxesSplitRT.getAttribute("data-pr"))));
		fareBreakupInfo
				.setTotalFare(Double.valueOf(replaceCurrencySymbolAndCommas(totalSplitRT.getAttribute("data-pr"))));

		return fareBreakupInfo;
	}

	private void airlinesSelection(String owAirline, String rtAirline, String owConnected, String rtConnected,
			boolean isConnected, BookingType bookingType) {

		Set<String> airlinesList = new HashSet<>();
		airlinesList.add(owAirline);
		airlinesList.add(rtAirline);
		airlinesList.add("MULTI");
		airlinesList.add(owConnected);
		airlinesList.add(rtConnected);
		WebDriverWait wait = getExplicitWaitObject(driver, 1);

		for (int index = 0; index < airlines.size(); index++) {
			wait.until(ExpectedConditions.elementToBeClickable(airlines.get(index)));
			if (!airlinesList.contains(airlines.get(index).getAttribute("value"))) {
				wait.until(ExpectedConditions.elementToBeClickable(airlines.get(index)));
				airlines.get(index).click();
			}
		}

		if (bookingType.toString().equalsIgnoreCase("ONE_WAY")) {
			if (isConnected) {
				if (owAirline.equalsIgnoreCase(owConnected)) {
					wait.until(ExpectedConditions.elementToBeClickable(multi_airlines)).click();
				}
			} else {
				wait.until(ExpectedConditions.elementToBeClickable(multi_airlines)).click();
			}
		} else if (bookingType.toString().equalsIgnoreCase("ROUND_TRIP")) {
			if (isConnected) {
				if (owAirline.equalsIgnoreCase(owConnected) && rtAirline.equalsIgnoreCase(rtConnected)) {
					wait.until(ExpectedConditions.elementToBeClickable(multi_airlines)).click();
				}
			} else {
				if ((StringUtils.isEmpty(owConnected) || owConnected.equalsIgnoreCase("X"))
						&& (StringUtils.isEmpty(rtConnected) || rtConnected.equalsIgnoreCase("X"))
						&& (owAirline.equalsIgnoreCase(rtAirline))) {
					wait.until(ExpectedConditions.elementToBeClickable(multi_airlines)).click();
				}
			}
		}
	}

	private void nearByFlitering(String isnEARbyFilteringRequired, String destination) {
		Boolean isRequired = Boolean.valueOf(isnEARbyFilteringRequired);
	//	addLog("isnEARbyFilteringRequired : " + isRequired + " destination :  " + destination);
		if (isRequired) {
			if (fluentWait(By.xpath("//nav[@class='nearbyairportsDest']//input"), 2) != null) {
				for (WebElement element : nearBys) {
					addLog("Required inside near by  : " + element.getAttribute("value"));
					
					if(!element.getAttribute("checked").equalsIgnoreCase("checked")) {
						//element.click();
					}
				}
			}
		}else {
			if (fluentWait(By.xpath("//nav[@class='nearbyairportsDest']//input"), 2) != null) {
				for (WebElement element : nearBys) {
					addLog("Not required inside near by  : " + element.getAttribute("value"));
					
					if (!element.getAttribute("value").equalsIgnoreCase(destination)) {
						element.click();
					}
				}
			}
		}
	}

	public void clickOnBookButton(IntermediatePage intermediatePage, MiscellaneousResponse miscellaneousResponse,
		WebElement element) throws Exception {
		dateStart=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
		clickByJseBasedOnWebElement(driver, element);
		waitForURL("itinerary", 130);
		if (!(driver.getCurrentUrl().contains("review") || driver.getCurrentUrl().contains("info"))) {
			driver.get(driver.getCurrentUrl()+"review");
		}
//		waitForJStoLoad(driver);
		dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
		writeLoadTimeInFile("Time taken to load Itinerary page after clicking on Book button from SRP is : " +GetDateTimeDiff(dateStart, dateStop));
		intermediatePage.validateFlightChangePopUpAndMove();
		intermediatePage.flightPriceChangeAlertHandling(miscellaneousResponse);
	}
}