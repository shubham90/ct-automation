package com.cleartrip.core.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.asserts.SoftAssert;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.Flight;
import com.cleartrip.core.model.HQResponse;
import com.cleartrip.core.model.PassangerDetail;
import com.cleartrip.core.model.TicketDetails;

public class HQPage extends PageBase {

	public HQPage(WebDriver driver, long timeout, String url) throws Exception {
		super(driver, timeout, url);
		this.driver = driver;
	}

	@FindBy(how = How.ID, using = "email")
	public WebElement email;

	@FindBy(how = How.ID, using = "password")
	public WebElement password;

	@FindBy(how = How.ID, using = "signInButton")
	public WebElement signInButton;

	@FindBy(how = How.XPATH, using = "//dt[contains(text(),'Adult')]/following-sibling::dd[1]")
	public WebElement adultPricing;

	@FindBy(how = How.XPATH, using = "//dt[contains(text(),'Child')]/following-sibling::dd[1]")
	public WebElement childPricing;

	@FindBy(how = How.XPATH, using = "//dt[contains(text(),'Infant')]/following-sibling::dd[1]")
	public WebElement infantPricing;

	@FindBy(how = How.XPATH, using = "//dt[contains(text(),'Other charges')]/following-sibling::dd[1]")
	public WebElement otherChargesPricing;

	@FindBy(how = How.XPATH, using = "//dt[contains(text(),'Insurance')]/following-sibling::dd[1]")
	public WebElement we_insurance;

	@FindBy(how = How.XPATH, using = "//dt[contains(text(),'Seat')]/following-sibling::dd[1]")
	public WebElement we_seat;

	@FindBy(how = How.XPATH, using = "//dt[contains(text(),'Meal')]/following-sibling::dd[1]")
	public WebElement we_meal;

	@FindBy(how = How.XPATH, using = "//dt[contains(text(),'Baggage')]/following-sibling::dd[1]")
	public WebElement we_baggage;

	@FindBy(how = How.XPATH, using = "//dt[contains(text(),'Discount')]/following-sibling::dd[1]")
	public WebElement we_discount;

	@FindBy(how = How.XPATH, using = "//dt[contains(text(),'GST')]/following-sibling::dd[1]")
	public WebElement gstAirlinePricing;

	@FindBy(how = How.XPATH, using = "//dt[contains(text(),'Convenience Fee')]/following-sibling::dd[1]")
	public WebElement convenienceFeePricing;

	@FindBy(how = How.XPATH, using = "//div[@class='col']//dt[contains(text(),'Total')]/following-sibling::dd[1]")
	public WebElement totalPricing;

	private String personName = "//div[@id='current_trip_details']//table[2]//tbody/tr[index]//strong";

	@FindBy(how = How.XPATH, using = "//div[@id='current_trip_details']//table[2]//tbody/tr")
	public List<WebElement> flighDetails;

	@FindBy(how = How.XPATH, using = "//div[@id='current_trip_details']//table[2]//tbody/tr[@class='caption']")
	public List<WebElement> paxDetailsInt;

	private String flightsDetail = "//div[@id='current_trip_details']//table[2]//tbody/tr[index]/td";

	private String itinaryDetail = "//div[@id='current_trip_details']//table[1]//tbody/tr[index]/td";

	@FindBy(how = How.XPATH, using = "//div[@id='current_trip_details']//table[1]//tbody/tr[@class='caption']")
	public List<WebElement> itinaryDetails;

	@FindBy(how = How.XPATH, using = "//div[@id='current_trip_details']//table[1]//tbody/tr")
	public List<WebElement> itinararySize;

	@FindBy(how = How.XPATH, using = "//a[@id='tab_2']")
	public WebElement paxDetailTab;

	@FindBy(how = How.XPATH, using = "//div[@id='pax_details']//div[contains(@id,'display-pax-details')]//table//tbody/tr")
	public List<WebElement> paxDetails;

	private String paxDetail = "//div[@id='pax_details']//div[contains(@id,'display-pax-details')]//table//tbody/tr[index]/td";

	private String paxDetailName = "//div[@id='pax_details']//div[contains(@id,'display-pax-details')]//table//tbody/tr[index]";

	@FindBy(how = How.XPATH, using = "//*[@id=\"global_signout\"]")
	public WebElement signout;

	public void open(String tripId) {
		addLog("Open the HQ Page");
		String hqPage = homePage + tripId;
		if(driver.getCurrentUrl().toLowerCase().contains("cleartrip.ae")) {
			hqPage=homePage.replace(".com/", ".ae/");
		}
		driver.get(hqPage);
	}

	public void login(String username, String passwd, int wait) {
		if ((signInButton.isDisplayed())) {
			waitForElement(adultPricing, wait);
			addLog("Open Login Page");
			email.sendKeys(username);
			password.sendKeys(passwd);
			signInButton.click();
		}
	}

	public HQResponse fetchDetails(int wait, BookingRequest bookRequest, SoftAssert softAssert) throws Exception {
		
		waitForElement(adultPricing, wait);
		addLog("Fetch information from HQ Page");

		HQResponse hqResponse = new HQResponse();

		String adultPricingText = getText(adultPricing).replace(",", "");
		hqResponse.setAdultAmount(Double
				.valueOf(replaceCurrencySymbolAndCommas(adultPricingText.substring(0, adultPricingText.indexOf("(")))));

		hqResponse.setAdultCount(Character.getNumericValue(adultPricingText.charAt(adultPricingText.length() - 2)));

		if (bookRequest.getChilds() > 0) {
			String childPricingText = getText(childPricing).replace(",", "");
			hqResponse.setChildAmount(Double.valueOf(
					replaceCurrencySymbolAndCommas(childPricingText.substring(0, childPricingText.indexOf("(")))));

			hqResponse.setChildCount(Character.getNumericValue(childPricingText.charAt(childPricingText.length() - 2)));
		} else {
			hqResponse.setChildAmount(0.0);
			hqResponse.setChildCount(0);
		}

		if (bookRequest.getInfants() > 0) {
			String infantPricingText = getText(infantPricing).replace(",", "");
			hqResponse.setInfantAmount(Double.valueOf(
					replaceCurrencySymbolAndCommas(infantPricingText.substring(0, infantPricingText.indexOf("(")))));

			hqResponse.setInfantCount(Character.getNumericValue(infantPricingText.charAt(infantPricingText.length() - 2)));
		} else {
			hqResponse.setInfantAmount(0.0);
			hqResponse.setInfantCount(0);
		}

		Double otherChargesText = Double.valueOf(replaceCurrencySymbolAndCommas(getText(otherChargesPricing)));
		hqResponse.setOtherCharges(otherChargesText);

		Double gstAirlineText = 0.0;
		if (fluentWait(By.xpath("//dt[contains(text(),'GST')]/following-sibling::dd[1]"), 5) != null
				&& gstAirlinePricing.isDisplayed()) {
			gstAirlineText = Double.valueOf(replaceCurrencySymbolAndCommas(getText(gstAirlinePricing)));
			hqResponse.setGstCharges(gstAirlineText);
		} else {
			hqResponse.setGstCharges(gstAirlineText);
		}

		hqResponse.setTotalTax(otherChargesText + gstAirlineText);

		Double insurance = 0.0;
		if (bookRequest.isInsuranceSelected()) {
			String insuranceText = getText(we_insurance);
			insurance = Double
					.valueOf(replaceCurrencySymbolAndCommas(insuranceText.substring(0, insuranceText.indexOf("("))));
			hqResponse.setInsurance(insurance);
		} else {
			hqResponse.setInsurance(insurance);
		}

		double mealfee = 0.0;
		if (bookRequest.isMealRequested()) {
			mealfee = Double.valueOf(replaceCurrencySymbolAndCommas(getText(we_meal)));
			hqResponse.setMeal(mealfee);
		} else {
			hqResponse.setMeal(mealfee);
		}

		double baggagefee = 0.0;
		if (bookRequest.isBaggageRequested()) {
			baggagefee = Double.valueOf(replaceCurrencySymbolAndCommas(getText(we_baggage)));
			hqResponse.setBaggage(baggagefee);
		} else {
			hqResponse.setBaggage(baggagefee);
		}

		double seatfee = 0.0;
		if (bookRequest.isSeatRequested()) {
			seatfee = Double.valueOf(replaceCurrencySymbolAndCommas(getText(we_seat)));
			hqResponse.setSeat(seatfee);
		} else {
			hqResponse.setSeat(seatfee);
		}

		double couponDiscount = 0.0;
		if (bookRequest.isSeatRequested()) {
			couponDiscount = Double.valueOf(replaceCurrencySymbolAndCommas(getText(we_discount)));
			hqResponse.setDiscount(couponDiscount);
		} else {
			hqResponse.setDiscount(couponDiscount);
		}

		String convienceFeesText = getText(convenienceFeePricing);
		hqResponse.setConvenienceFee(Double.valueOf(replaceCurrencySymbolAndCommas(convienceFeesText)));

		String totalText = getText(totalPricing);
		hqResponse.setTotal(Double.valueOf(replaceCurrencySymbolAndCommas(totalText)));

		int countPax = bookRequest.getAdults() + bookRequest.getChilds() + bookRequest.getInfants();

		for (int index = 0; index < paxDetailsInt.size();) {
//			addLog("Pax details: " + paxDetailsInt.size() + " Index:  " + index + "  TotalPaxCount:  " + countPax);
			String name = driver
					.findElement(By.xpath(
							personName.replaceAll("index", (1 + index * ((flighDetails.size()) / countPax)) + "")))
					.getText();
//			addLog("Pax details  name: " + name);

			for (int index1 = 3
					+ index * ((flighDetails.size()) / countPax); index1 <= ((flighDetails.size()) / countPax)
							* (index + 1); index1++) {
//				addLog("Pax booking details index: " + index1 + "  Pax booking details count:  "
//						+ ((flighDetails.size()) / countPax) * (index + 1));

				TicketDetails ticketDetails = new TicketDetails();
				List<WebElement> datas = driver
						.findElements(By.xpath(flightsDetail.replaceAll("index", (index1) + "")));
				ticketDetails.setSector(datas.get(0).getText());
				ticketDetails.setStatus(datas.get(1).getText());
				ticketDetails.setAirlinePNR(datas.get(2).getText());
				ticketDetails.setGdsPNR(datas.get(4).getText());
				ticketDetails.setClazz(datas.get(5).getText());
				ticketDetails.setTicket(datas.get(7).getText());
				ticketDetails.setFareBasesCode(datas.get(6).getText());
				ticketDetails.setPersonName(name);
				hqResponse.addTicket(ticketDetails);
			}
			index++;
		}

		for (int index = 0; index < itinaryDetails.size();) {
			for (int innerIndex = 0; innerIndex < (itinararySize.size() / itinaryDetails.size()) - 4; innerIndex++) {
//				addLog("Itinerary size " + itinaryDetails.size() + " itinararySize : " + itinararySize.size()
//						+ " (itinararySize.size()/itinaryDetails.size()) :"
//						+ (itinararySize.size() / itinaryDetails.size()));
//				addLog("(innerIndex + index*(itinararySize.size()/itinaryDetails.size())+2) "
//						+ (innerIndex + index * (itinararySize.size() / itinaryDetails.size()) + 2));
				Flight flight = new Flight();
				List<WebElement> datas = driver.findElements(By.xpath(itinaryDetail.replaceAll("index",
						(innerIndex + index * (itinararySize.size() / itinaryDetails.size()) + 4) + "")));
				addLog("datas size " + datas.size());
				String from = datas.get(0).getText();
				flight.setFrom(from.substring(from.indexOf("(") + 1, from.lastIndexOf(")")));
//				addLog("From " + from);
				String to = datas.get(1).getText();
				flight.setTo(to.substring(to.indexOf("(") + 1, to.lastIndexOf(")")));
//				addLog("TO " + to);
				flight.setAirline(datas.get(3).getText());
//				addLog("Airline " + datas.get(3).getText());
				flight.setClazz(datas.get(6).getText());
//				addLog("setClazz " + datas.get(6).getText());
				flight.setStop(datas.get(5).getText());
//				addLog("Stops " + datas.get(5).getText());
				flight.setSupplier(datas.get(7).getText());
//				addLog("Supplier " + datas.get(7).getText());
				flight.setNumber(datas.get(4).getText());
//				addLog("Number " + datas.get(4).getText());
				flight.setDepartTime(
						datas.get(0).getText().split("\n")[datas.get(0).getText().split("\n").length - 1].trim()
								+ " 2019");
//				addLog(datas.get(0).getText().split("\n")[datas.get(0).getText().split("\n").length - 1].trim()
//						+ " 2019");
				flight.setArrivalTime(
						datas.get(1).getText().split("\n")[datas.get(1).getText().split("\n").length - 1].trim()
								+ " 2019");
//				addLog("Arrival Time "
//						+ datas.get(1).getText().split("\n")[datas.get(1).getText().split("\n").length - 1].trim()
//						+ " 2019");
				if (index == 0) {
					flight.setOw(true);
				}
				hqResponse.addFlight(flight);
				innerIndex++;
			}
			index++;
		}

		paxDetailTab.click();
		paxDetailTab.click();
		waitForElement(
				driver.findElement(By
						.xpath("//div[@id='pax_details']//div[contains(@id,'display-pax-details')]//table//tbody/tr")),
				10);
		int count = 0;
		int nameIndex = 0;
		for (int index = 0; index < paxDetails.size();) {

			PassangerDetail passangerDetail = new PassangerDetail();
			List<WebElement> datas = driver.findElements(By.xpath(paxDetail.replaceAll("index", (3) + "")));
			passangerDetail.setDob(datas.get(count + 0).getText());
			passangerDetail.setPassportNumber(datas.get(count + 1).getText());
			passangerDetail.setCountry(datas.get(count + 2).getText());
			passangerDetail.setExpiry(datas.get(count + 4).getText());
			passangerDetail
					.setName(driver.findElements(By.xpath(paxDetailName.replaceAll("index", (1) + "") + "//strong"))
							.get(nameIndex).getText());
			hqResponse.addPassangerDetail(passangerDetail);
			index = index + 4;
			count = count + 7 + 1;
			nameIndex = nameIndex + 1;
		}
//		addLog(hqResponse.toString());
		return hqResponse;
	}
}
