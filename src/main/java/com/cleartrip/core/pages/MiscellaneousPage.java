package com.cleartrip.core.pages;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cleartrip.core.model.MiscellaneousResponse;

public class MiscellaneousPage extends PageBase {

    public MiscellaneousPage(WebDriver driver, long timeout) throws Exception {
        super(driver, timeout, null);
        this.driver = driver;
    }
    
    String dateStart="";
    String dateStop="";

    @FindBy(how = How.ID, using = "coupon")
    public WebElement coupontext;

    @FindBy(how = How.ID, using = "check_saving")
    public WebElement applycouponBtn;

    @FindBy(how = How.XPATH, using = "//*[@id='ValidCoupon']/p")
    public WebElement couponApplied;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'fare details')]")
    public WebElement fareDetailsLink;															

    @FindBy(how = How.ID, using = "coupondfee")
    public WebElement couponAmount;

	@FindBy(how = How.ID, using = "close")
    public WebElement fareDetailsPopupClose;							  

    public boolean applyCoupon(int waitTime,String coupon, Map<String, String> coupons, MiscellaneousResponse miscellaneousResponse) throws InterruptedException, IOException {

        addLog("Enter Coupon code: " + coupon);
	    miscellaneousResponse.setCouponDiscountValue(0.0);
								  

        if (StringUtils.isBlank(coupon) || !coupons.containsKey(coupon)) {
        	addTripIdLog("Coupon Required : false");
            return false;
        }

        applyExplicitWaitByWebElementForVisibilityOfElement(driver, coupontext, 10);
        coupontext.sendKeys(coupons.get(coupon));
        addLog("Click on apply coupon");

        applyExplicitWaitByWebElementForVisibilityOfElement(driver, applycouponBtn, 10);
        applycouponBtn.click();
        dateStart=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");

        applyExplicitWaitByWebElementForVisibilityOfElement(driver, couponApplied, 40);
        if (!getTextByInnerHtml(couponApplied).toLowerCase().contains("invalid")) {
        	dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
        	writeLoadTimeInFile("Time taken to apply coupon on itinerary page is : " +GetDateTimeDiff(dateStart, dateStop));
            addLog("Coupon Applied Successfully");
			fareDetailsLink.click();
            WebDriverWait wait = getExplicitWaitObject(driver, 15);
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("modal_window"));
            Double couponAmount = Double.valueOf(replaceCurrencySymbolAndCommas(getText(this.couponAmount)));
            miscellaneousResponse.setCouponDiscountValue(couponAmount);
            addLog("Coupon discount amount " + couponAmount);
            addTripIdLog("Coupon discount amount " + couponAmount);
            driver.switchTo().defaultContent();
            fareDetailsPopupClose.click();										  
            return true;
        }else {
        	addLog("Coupon code "+coupon+" is incorrect. Coupon did not apply successfully.");
        	addTripIdLog("Coupon code "+coupon+" is incorrect. Coupon did not apply successfully.");
        }
        return false;
    }
}
