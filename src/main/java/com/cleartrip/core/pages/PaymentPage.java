package com.cleartrip.core.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.PaymentResponse;

public class PaymentPage extends PageBase {

    public PaymentPage(WebDriver driver, long timeout, String url) throws Exception {
        super(driver, timeout, url);
        this.driver = driver;
    }

    @FindBy(how= How.ID, using="creditCardNumberDisp")
    public WebElement creditCardNumberDispElement;

    @FindBy(how= How.ID, using="CcExpirationMonth")
    public WebElement creditCardExpirationMonthElement;

    @FindBy(how= How.ID, using="CcExpirationYear")
    public WebElement creditCardExpirationYearElement;

    @FindBy(how= How.ID, using="BillName")
    public WebElement creditCardHolderNameElement;

    @FindBy(how= How.ID, using="cvvCode")
    public WebElement creditCardCVVNumberElement;

    @FindBy(how= How.ID, using="paymentSubmit")
    public WebElement paymentSubmitBtn;

    @FindBy(how= How.XPATH, using="//div[contains(@id,'travellerDone')]/div[1]/span[contains(@class,'steps')]")
    public WebElement stepsImg;
    
    @FindBy(how= How.XPATH, using="//*[@id=\"CCTab\"]/a")
    public WebElement CCTab;
    
	@FindBy(how = How.ID, using = "travellerBtn")
	public WebElement travellerBtn;
	
	@FindBy(how = How.ID, using = "walletOptOut")
	public WebElement walletCheckBox;
	
    public PaymentResponse book(String creditCardNumber, String creditCardExpMonth, String creditCardExpYear,
                                String creditCardHolderName, String creditCardCVVNumber, int waitTimeout,MiscellaneousResponse miscellaneousResponse) throws Exception {
        PaymentResponse paymentResponse = new PaymentResponse();
        String url=driver.getCurrentUrl();
        waitForElement(stepsImg, waitTimeout);
        CCTab.click();
        if(url.contains("qa2.cleartrip.com") || url.contains("www.cleartrip.com")) {
        	if (walletCheckBox.isDisplayed() && walletCheckBox.isEnabled()) {
        		clickByJseBasedOnWebElement(driver, walletCheckBox);
        	}
        }
        enterCardNumberSlowly(creditCardNumberDispElement, creditCardNumber);
//        creditCardNumberDispElement.sendKeys(creditCardNumber);
        creditCardExpirationMonthElement.sendKeys(creditCardExpMonth);
        creditCardExpirationYearElement.sendKeys(creditCardExpYear);
        creditCardHolderNameElement.sendKeys(creditCardHolderName);
        creditCardCVVNumberElement.sendKeys(creditCardCVVNumber);
        paymentResponse.setSubmitButton(paymentSubmitBtn);
        return paymentResponse;
    }
    
    public void clickOnMakePaymentButton(IntermediatePage intermediatePage,MiscellaneousResponse miscellaneousResponse, WebElement element_itinerary, WebElement element_payment) throws Exception {
		applyExplicitWaitByWebElementForVisibilityOfElement(driver, element_payment, 20);
		clickByJseBasedOnWebElement(driver, element_payment);
		miscellaneousResponse.setDateStart(getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS"));
		
    	intermediatePage.validateFlightChangePopUpAndMove();
    	intermediatePage.flightPriceChangeAlertHandling(miscellaneousResponse);
    	
    	if(element_itinerary.isDisplayed()) {
        	clickByJseBasedOnWebElement(driver, element_itinerary);
        }
    	
    	if(travellerBtn.isDisplayed()) {
        	clickByJseBasedOnWebElement(driver, travellerBtn);
        }
    	
    	if(element_payment.isDisplayed()) {
    		clickByJseBasedOnWebElement(driver, element_payment);
    	}
    }
}
