package com.cleartrip.core.pages;

import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.model.TripResponse;

public class ConfirmationPage extends PageBase {
	 
    private String confirmationPageURL = "confirmation";
   
	@FindBy(how= How.XPATH, using="//div[@class='header']/p/strong")
	public WebElement tripId;
	
	@FindBy(how=How.XPATH,using="//table[@id='airBookingdetails']/tbody/tr/td[2]/a")
	public List<WebElement> list_trip;
	
    public ConfirmationPage(WebDriver driver, long timeout, String url) throws Exception {
        super(driver, timeout, url);
        this.driver = driver;
    }

    public TripResponse confirm(String itineraryId, MiscellaneousResponse miscellaneousResponse) throws IOException {
    	waitForURL(confirmationPageURL,200);
	    TripResponse tripResponse = new TripResponse();
	    addLog("Getting trip Id from confirmation link.");
	    String trip_id="";
	    if(tripId.isDisplayed()) {
	    	applyExplicitWaitByWebElementForVisibilityOfElement(driver, tripId, 30);
	    	trip_id=getText(tripId);
	    	addLog("Trip Id from confirmation page: "+trip_id);
	    	addTripIdLog("Trip Id from confirmation page: "+trip_id);
	    }else {
	    	driver.get("http://172.17.12.171/airapi/bookreports.php?itineraryId="+itineraryId);
	    	applyExplicitWaitByListWebElementForVisibilityOfElement(driver, list_trip, 15);
	    	for (int i = 0; i < list_trip.size();) {
	    		trip_id=getText(list_trip.get(i));
	    		break;
			}
	    	addLog("Trip Id based on itinerary id: "+trip_id);
	    	addTripIdLog("Trip Id based on itinerary id: "+trip_id);
	    }
	    String dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
	    writeLoadTimeInFile("Time taken to load booking conformation page is : " +GetDateTimeDiff(miscellaneousResponse.getDateStart(), dateStop));
	    tripResponse.setTripId(trip_id);
	    return tripResponse;
    }

}
