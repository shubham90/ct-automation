package com.cleartrip.core.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cleartrip.core.model.FareDetailResponse;
import com.cleartrip.core.model.MiscellaneousResponse;
import com.cleartrip.core.utils.Helper;

public class FareDetailsPage extends PageBase {

    @FindBy(how = How.XPATH, using = "//a[@id='fareBreakupDetails']")
    public WebElement fareDetailsLink;

    @FindBy(how = How.XPATH, using = "//dt[text()='Base fare']//ancestor::dl/dd")
    public List<WebElement> baseFareSection;
	
    @FindBy(how = How.XPATH, using = "//p[@id='payBlockTotal']//strong[contains(@id,'totalFare')]/span[contains(@id,'counter')]")
    public WebElement we_totalAmountOnPaymentPage;

    @FindBy(how = How.ID, using = "processingFeeAmount")
    public WebElement we_conveniencePerPerson;

    @FindBy(how = How.ID, using = "rtMealsBaggageAmt")
    public WebElement mealsAndBaggage;

    @FindBy(how = How.ID, using = "rtMealsSeatAmt")
    public WebElement seat;

    @FindBy(how = How.ID, using = "insurancePrice")
    public WebElement insurance;

    @FindBy(how = How.ID, using = "coupondfee")
    public WebElement couponAmount;

    @FindBy(how = How.ID, using = "insurancePrice")
    public WebElement Insurance;
	
	@FindBy(how = How.XPATH, using = "//dt[@id='rtProcessingLabel' ]//ancestor::dl/dt")
    public List<WebElement> we_miscLabelList;

    @FindBy(how = How.XPATH, using = "//dt[@id='rtProcessingLabel']//ancestor::dl/dd")
    public List<WebElement> we_miscAmountList;
    
    @FindBy(how = How.XPATH, using = "//dt[@id='rtProcessingLabel']//ancestor::dl/dd/span[contains(@class,'value')]")
    public List<WebElement> we_taxAmountList;

    @FindBy(how = How.ID, using = "cashback_fare_block")
    public WebElement we_couponDiscount;

    @FindBy(how = How.ID, using = "gv_fare_block")
    public WebElement we_giftVoucher;

    @FindBy(how = How.ID, using = "insurance_fare_block")
    public WebElement we_insurance;

    @FindBy(how = How.ID, using = "rtTotalAmount")
    public WebElement we_totalAmount;

    @FindBy(how = How.XPATH, using = "//div[@id='TranslucentLayer']")
    public WebElement closeWindow;

    @FindBy(how = How.ID, using = "close")
    public WebElement fareDetailsPopupClose;

    @FindBy(how = How.XPATH, using = "//span[@id='step1TotalFare']//span[@id='counter']")
    public WebElement total_fare;

    private String fareDetailsPage = "pay";
    
    long timeout=60;
    String dateStop="";


    public FareDetailsPage(WebDriver driver, long timeout, String url) throws Exception {
        super(driver, timeout, url);
        this.driver = driver;
    }

    public FareDetailResponse fetchFareDetails(int waitTime,MiscellaneousResponse miscellaneousResponse) {
        addLog("Fetching fare break-up from itinerary step payment fare break up pop up window.");
        try {
        	applyExplicitWaitByWebElementForVisibilityOfElement(driver, we_totalAmountOnPaymentPage, 120);
            waitForURL(fareDetailsPage,timeout);
            waitForLoad(driver);
            dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
            writeLoadTimeInFile("Time taken to load Payment section on itinerary page is : " +GetDateTimeDiff(miscellaneousResponse.getDateStart(), dateStop));
            Actions actions = new Actions(driver);

            applyExplicitWaitByWebElementForVisibilityOfElement(driver, fareDetailsLink, 20);
            actions.moveToElement(fareDetailsLink).click().perform();
            //fareDetailsLink.click();
            return fetchDeailsFromPopup(waitTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public FareDetailResponse fetchDeailsFromPopup(int waitTime) {

        FareDetailResponse fareDetailResponse = new FareDetailResponse();
        try {
            WebDriverWait wait = new WebDriverWait(driver, waitTime);
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("modal_window"));
            applyExplicitWaitByWebElementForVisibilityOfElement(driver, we_totalAmount, 20);
            String adult_base_fare="0";
            String adult_count="0";
            String child_base_fare="0";
            String child_count="0";
            String infant_base_fare="0";
            String infant_count="0";

            int size=baseFareSection.size();
            for (int i = 0; i < size ; i++) {

                if (size == 3 ){
                    if (i == 0){
                        adult_base_fare=getText(baseFareSection.get(i).findElement(By.xpath("./span[2]")));
                        adult_count=getText(baseFareSection.get(i).findElement(By.xpath("./small"))).substring(1,2);
                        addLog("Number of adult is " + adult_count+" and base fare for the same is "+adult_base_fare );
                    }else if (i == 1){
                        child_base_fare=getText(baseFareSection.get(i).findElement(By.xpath("./span[2]")));
                        child_count=getText(baseFareSection.get(i).findElement(By.xpath("./small"))).substring(1,2);
                        addLog("Number of child is " + child_count+" and base fare for the same is "+child_base_fare );
                    }else if (i == 2){
                        infant_base_fare=getText(baseFareSection.get(i).findElement(By.xpath("./span[2]")));
                        infant_count=getText(baseFareSection.get(i).findElement(By.xpath("./small"))).substring(1,2);
                        addLog("Number of infant is " + infant_count+" and base fare for the same is "+infant_base_fare );
                    }

                }else if (size == 2){
                    if (i == 0){
                        adult_base_fare=getText(baseFareSection.get(i).findElement(By.xpath("./span[2]")));
                        adult_count=getText(baseFareSection.get(i).findElement(By.xpath("./small"))).substring(1,2);
                        addLog("Number of adult is " + adult_count+" and base fare for the same is "+adult_base_fare );
                    }else if (i == 1){
                        boolean is_child=(getText(baseFareSection.get(i).findElement(By.xpath("./small"))).toLowerCase()).contains("child");
                        if (is_child){
                            child_base_fare=getText(baseFareSection.get(i).findElement(By.xpath("./span[2]")));
                            child_count=getText(baseFareSection.get(i).findElement(By.xpath("./small"))).substring(1,2);
                            addLog("Number of child is " + child_count+" and base fare for the same is "+child_base_fare );
                        }else{
                            infant_base_fare=getText(baseFareSection.get(i).findElement(By.xpath("./span[2]")));
                            infant_count=getText(baseFareSection.get(i).findElement(By.xpath("./small"))).substring(1,2);
                            addLog("Number of infant is " + infant_count+" and base fare for the same is "+infant_base_fare );
                        }
                    }
                }else if (size == 1){
                    adult_base_fare=getText(baseFareSection.get(i).findElement(By.xpath("./span[2]")));
                    adult_count=getText(baseFareSection.get(i).findElement(By.xpath("./small"))).substring(1,2);
                    addLog("Number of adult is " + adult_count+" and base fare for the same is "+adult_base_fare );
                }
            }

            fareDetailResponse.setAdultCount(Integer.valueOf(adult_count));
            fareDetailResponse.setBaseFareAdult(Double.valueOf(replaceCurrencySymbolAndCommas(adult_base_fare)));
            fareDetailResponse.setChildCount(Integer.valueOf(child_count));
            fareDetailResponse.setBaseFareChild(Double.valueOf(replaceCurrencySymbolAndCommas(child_base_fare)));
            fareDetailResponse.setInfantCount(Integer.valueOf(infant_count));
            fareDetailResponse.setBaseFareInfant(Double.valueOf(replaceCurrencySymbolAndCommas(infant_base_fare)));

			int miscSize = we_miscLabelList.size();
            double totalTax = 0.0;
            double convenienceFee =0.0;
            double emiProcessingFee =0.0;
            double cleartripCash =0.0;
            double mealsAndBaggage =0.0;
            double seat =0.0;
            int j = 0;
            String labelList = "Convenience fee,EMI Processing Fee,CleartripCash,Meals & baggage,Seat".toLowerCase();
            for (int i = 0; i < miscSize; i++) {
                String labelName = we_miscLabelList.get(i).getText().toLowerCase();
                if (!labelList.contains(labelName)) {
                	if(j < we_taxAmountList.size() ) {
                        double taxBreakUp = Double.valueOf(replaceCurrencySymbolAndCommas(we_taxAmountList.get(j).getAttribute("data-pr")));
                        totalTax = totalTax + taxBreakUp;
                        addLog("The tax value for this " + labelName + " is " + taxBreakUp);
                        j++;
                	}
                } else if (labelName.equalsIgnoreCase("Convenience fee")) {
                    convenienceFee = Double.valueOf(replaceCurrencySymbolAndCommas(we_miscAmountList.get(i).getText()));
                    addLog("The Convenience fee is " + convenienceFee);
                } else if (labelName.equalsIgnoreCase("EMI Processing Fee")) {
                    emiProcessingFee = Double.valueOf(replaceCurrencySymbolAndCommas(we_miscAmountList.get(i).getText()));
                    addLog("The EMI processing fee is " + emiProcessingFee);
                } else if (labelName.equalsIgnoreCase("CleartripCash")) {
                    cleartripCash = Double.valueOf(replaceCurrencySymbolAndCommas(we_miscAmountList.get(i).getText()));
                    addLog("The CleartripCash is " + cleartripCash);
                } else if (labelName.equalsIgnoreCase("Meals & baggage")) {
                    mealsAndBaggage = Double.valueOf(replaceCurrencySymbolAndCommas(we_miscAmountList.get(i).getText()));
                    addLog("The Meals & baggage fee is " + mealsAndBaggage);
                } else if (labelName.equalsIgnoreCase("Seat")) {
                    seat = Double.valueOf(replaceCurrencySymbolAndCommas(we_miscAmountList.get(i).getText()));
                    addLog("The Seat fee is " + seat);
                }
            }

            fareDetailResponse.setTotalTax(Helper.roundAvoid(totalTax,0));
            addLog("Total tax value is " + totalTax);

            fareDetailResponse.setConvenienceFee(convenienceFee);
            fareDetailResponse.setEmiProcessingFee(emiProcessingFee);
            fareDetailResponse.setCleartripCash(cleartripCash);
            fareDetailResponse.setMealsAndBaggage_fare(mealsAndBaggage);
            fareDetailResponse.setSeat_fare(seat);

            Double coupon_discount = 0.0;
            Double giftVoucher = 0.0;
            Double insurance = 0.0;
            Double convenienceFeePerPerson=0.0;
            Double totalAmountOnPaymentPage=0.0;

            if (fluentWait(By.id("cashback_fare_block")) != null &&  !we_couponDiscount.getAttribute("style").toLowerCase().contains("none")) {
                coupon_discount = Double.valueOf(replaceCurrencySymbolAndCommas(we_couponDiscount.findElement(By.xpath("//span[@id='coupondfee']")).getText()));
            }

            if (fluentWait(By.id("gv_fare_block")) != null &&  !we_giftVoucher.getAttribute("style").toLowerCase().contains("none")) {
                giftVoucher = Double.valueOf(replaceCurrencySymbolAndCommas(we_giftVoucher.findElement(By.xpath("//span[@id='gvamount']")).getText()));
            }

            if (fluentWait(By.id("insurance_fare_block")) != null &&  !we_insurance.getAttribute("style").toLowerCase().contains("none")) {
                insurance = Double.valueOf(replaceCurrencySymbolAndCommas(we_insurance.findElement(By.xpath("//span[@id='insurancePrice']")).getText()));
            }

            fareDetailResponse.setCouponDiscountValue(coupon_discount);
            addLog("Coupon discount value is " + coupon_discount);
            
            fareDetailResponse.setGiftVoucherValue(giftVoucher);
            addLog("Gift voucher value is " + giftVoucher);
            
            fareDetailResponse.setInsuranceValue(insurance);
            addLog("Insurance value is " + insurance);
            
            Double totalAmount = Double.valueOf(replaceCurrencySymbolAndCommas(we_totalAmount.getText()));
            fareDetailResponse.setTotalAmount(totalAmount);
            addLog("Total Amount to pay on fare break up window : " + totalAmount);
            
            driver.switchTo().defaultContent();
            fareDetailsPopupClose.click();

            applyExplicitWaitByWebElementForVisibilityOfElement(driver, we_totalAmountOnPaymentPage, 20);
            convenienceFeePerPerson = Double.valueOf(replaceCurrencySymbolAndCommas(getTextByInnerHtml(we_conveniencePerPerson)));
            fareDetailResponse.setConvenienceFeePerPerson(convenienceFeePerPerson);
            addLog("Convenience processing fee per person : " + convenienceFeePerPerson);
            
            totalAmountOnPaymentPage=Double.valueOf(replaceCurrencySymbolAndCommas(getTextByInnerHtml(we_totalAmountOnPaymentPage)));
            addLog("Total Amount to pay on payment page above make payment button : " + totalAmountOnPaymentPage);
            fareDetailResponse.setTotalAmountOnPaymentPage(totalAmountOnPaymentPage);
            
        } catch (Exception e) {
            addLog(e.toString());
            e.printStackTrace();
        }
        return fareDetailResponse;

    }


}
