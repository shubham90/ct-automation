package com.cleartrip.core.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.asserts.SoftAssert;

import com.cleartrip.core.model.FlightRecordInfo;
import com.cleartrip.core.model.ItineraryResponse;
import com.cleartrip.core.model.MiscellaneousResponse;

public class ItineraryPage extends PageBase {


	public ItineraryPage(WebDriver driver, long timeout, String url) throws Exception {
		super(driver, timeout, url);
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'itinerary clearFix')]")
	public List<WebElement> listOfItinerary;

	public String flightCodeAndNumber = "//div[contains(@class,'itinerary clearFix')][index]//ul[contains(@class,'itinerarySummary')]//div[@class='airlineName']/small[@class='flightNumber']";

	public String travelClass = "//div[contains(@class,'itinerary clearFix')][index]//ul[contains(@class,'itinerarySummary')]//div[@class='airlineName']/small[@class='flightNumber']//following-sibling::small";

	public String airlineName = "//div[contains(@class,'itinerary clearFix')][index]//ul[contains(@class,'itinerarySummary')]//div[@class='airlineName']/span";

	public String fromTime = "//div[contains(@class,'itinerary clearFix')][index]//li[@class='start']/time//span[@rel='tTooltip']//following-sibling::strong";

	public String fromDate = "//div[contains(@class,'itinerary clearFix')][index]//li[@class='start']/time//span[@rel='tTooltip']//ancestor::span/following-sibling::span";

	public String fromAirportAndTerminalName = "//div[contains(@class,'itinerary clearFix')][index]//li[@class='start']/small[@class='terminal']";

	public String toTime = "//div[contains(@class,'itinerary clearFix')][index]//li[@class='end']/time//span[@rel='tTooltip']//preceding-sibling::strong";

	public String toDate = "//div[contains(@class,'itinerary clearFix')][index]//li[@class='end']/time//span[@rel='tTooltip']//ancestor::span/following-sibling::span";

	public String toAirportAndTerminalName = "//div[contains(@class,'itinerary clearFix')][index]//li[@class='end']/small[@class='terminal']";

	public String source = "//div[contains(@class,'itinerary clearFix')][index]//li[@class='start']/time//span[@rel='tTooltip']";

	public String destination = "//div[contains(@class,'itinerary clearFix')][index]//li[@class='end']/time//span[@rel='tTooltip']";

	@FindBy(how = How.ID, using = "itineraryBtn")
	public WebElement itineraryBtnElement;

	@FindBy(how = How.XPATH, using = "//strong[contains(@id,'totalFare')]/span[contains(@id,'counter')]")
	public WebElement counterElement;

	@FindBy(how = How.XPATH, using = "//li[@class='baggageInfo']/small[contains(text(),'Check')]")
	public WebElement checkinBaggage;

	@FindBy(how = How.XPATH, using = "//li[@class='baggageInfo']/small[contains(text(),'Cabin')]")
	public WebElement cabinBaggage;

	public ItineraryResponse continueBooking(int wait, SoftAssert softassert) {
		// waitForElement(checkinBaggage, wait);
		waitForLoad(driver);
		ItineraryResponse itineraryResponse = new ItineraryResponse();
		itineraryResponse.setItineraryBtnElement(itineraryBtnElement);
		List<FlightRecordInfo> flightRecordInfos = fetchFlightInformation();
		itineraryResponse.setFlightRecordInfos(flightRecordInfos);
		return itineraryResponse;
	}

	private List<FlightRecordInfo> fetchFlightInformation() {
		List<FlightRecordInfo> flightRecordInfos = new ArrayList<>();

		for (int index = 1; index < listOfItinerary.size() + 1; index++) {
			FlightRecordInfo flightRecordInfo = new FlightRecordInfo();

			flightRecordInfo.setFlightCodeAndNumber(getTextResponse(flightCodeAndNumber, index + ""));

			flightRecordInfo.setTravelClass(getTextResponse(travelClass, index + ""));

			flightRecordInfo.setFrom(getTextResponse(source, index + ""));

			flightRecordInfo.setTo(getTextResponse(destination, index + ""));

			flightRecordInfo.setAirlineName(getTextResponse(airlineName, index + ""));

			flightRecordInfo.setFromTime(getTextResponse(fromTime, index + ""));

			flightRecordInfo.setFromDate(getTextResponse(fromDate, index + ""));

			flightRecordInfo.setFromAirportTerminalName(getTextResponse(fromAirportAndTerminalName, index + ""));

			flightRecordInfo.setToTime(getTextResponse(toTime, index + ""));

			flightRecordInfo.setToDate(getTextResponse(toDate, index + ""));

			flightRecordInfo.setToAirportTerminalName(getTextResponse(toAirportAndTerminalName, index + ""));
			/*
			 * if (checkinBaggage.isDisplayed()) {
			 * flightRecordInfo.setCheckinBaggage(getText(checkinBaggage)); }
			 * 
			 * if (cabinBaggage.isDisplayed()) {
			 * flightRecordInfo.setCabinBaggage(getText(cabinBaggage)); }
			 */
			flightRecordInfos.add(flightRecordInfo);
		}
		return flightRecordInfos;
	}

	private String getTextResponse(String element, String index) {
		return driver.findElement(By.xpath(element.replaceAll("index", index))).getText();
	}

	public void clickOnItineraryContinueButton(IntermediatePage intermediatePage,MiscellaneousResponse miscellaneousResponse,WebElement element) throws Exception {
    	clickByJseBasedOnWebElement(driver, element);
    	miscellaneousResponse.setDateStart(getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS"));
    	
    	intermediatePage.validateFlightChangePopUpAndMove();
    	intermediatePage.flightPriceChangeAlertHandling(miscellaneousResponse);
    	
    	if(element.isDisplayed()) {
        	clickByJseBasedOnWebElement(driver, element);
        }
    }
}