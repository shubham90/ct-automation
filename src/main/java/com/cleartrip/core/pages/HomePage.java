package com.cleartrip.core.pages;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.cleartrip.core.constants.BookingType;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.MiscellaneousResponse;


public class HomePage extends PageBase {

    public HomePage(WebDriver driver, long timeout, String url) throws Exception {
        super(driver, timeout, url);
        this.driver = driver;
    }
    
    String dateStart="";
    String dateStop="";

    @FindBy(how = How.ID, using = "userAccountLink")
    public WebElement yourTripMenu;

    @FindBy(how = How.ID, using = "SignIn")
    public WebElement signIn;

    @FindBy(how = How.ID, using = "email")
    public WebElement email;

    @FindBy(how = How.ID, using = "password")
    public WebElement password;

    @FindBy(how = How.ID, using = "signInButton")
    public WebElement signInButton;

    @FindBy(how = How.ID, using = "FromTag")
    public WebElement fromElement;

    @FindBy(how = How.ID, using = "ToTag")
    public WebElement toElement;

    @FindBy(how = How.ID, using = "DepartDate")
    public WebElement departDateElement;

    @FindBy(how = How.ID, using = "ReturnDate")
    public WebElement returnDateElement;

    @FindBy(how = How.ID, using = "Adults")
    public WebElement adultsElement;

    @FindBy(how = How.ID, using = "Childrens")
    public WebElement childrensElement;

    @FindBy(how = How.ID, using = "Infants")
    public WebElement infantsElement;

    @FindBy(how = How.ID, using = "SearchBtn")
    public WebElement searchBtnElement;

    @FindBy(how = How.ID, using = "OneWay")
    public WebElement oneWayElement;

    @FindBy(how = How.ID, using = "RoundTrip")
    public WebElement roundTripElement;

    @FindBy(how = How.ID, using = "AirlineAutocomplete")
    public WebElement airlineAutocompleteElement;

    @FindBy(how = How.ID, using = "MoreOptionsLink")
    public WebElement moreOptionsLink;

    @FindBy(how = How.XPATH, using = "//div[@id='ui-datepicker-div']")
    public WebElement dateModel;
    
    @FindBy(how= How.XPATH,using="//*[@id=\"userAccountLink\"]/span[2]")
    public WebElement loginDone;

    public String selectionFrom = "ui-id-1";

    public String selectionTO = "ui-id-2";

    public String selectionPreferredAirline = "ui-id-3";

    @FindBy(how = How.ID, using = "Class")
    public WebElement travelClassElement;

    @FindBy(how = How.XPATH, using = "//div[@class='floatingMessage']//p/input[@value='English']")
    public WebElement DomainAlert;

    @FindBy(how = How.XPATH, using = "//li[contains(@class,'userAccountMenuContainer')]//span[contains(@class,'span')]")
    public WebElement loginElement;
    
    @FindBy(how = How.XPATH, using = "//div[contains(@class,'last')]/div/a[@data-handler='next']")
    public WebElement we_nextArrowOnDatePicker;
    
    String selectDate = "//span[contains(text(),'monthName')]/following-sibling::span[contains(text(),'year')]/ancestor::div[@class='header']/following-sibling::table//tr[weekNumberOfTheMonth]/td/a[contains(text(),'day')]";
    
    String monthOnDatePicker = "//div[contains(@class,'monthBlock')]//span[contains(text(),'monthName')]";
    
    public void open(MiscellaneousResponse miscellaneousResponse) throws IOException, InterruptedException {
    	String env=miscellaneousResponse.getEnvironment();
    	String domain=miscellaneousResponse.getDomain();
    	String channel=miscellaneousResponse.getChannel();
    	addLog("Opening "+channel+" Home Page of "+domain+" domain on "+env+" environment.");
    	addTripIdLog("Opening "+channel+" Home Page of "+domain+" domain on "+env+" environment.");
        dateStart=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
        driver.get(homePage);
    }

    public void login(String username, String passwd, int wait) throws IOException {
    	applyExplicitWaitByWebElementForVisibilityOfElement(driver, yourTripMenu, 60);
    	dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
    	writeLoadTimeInFile("Time taken to load home page is : " +GetDateTimeDiff(dateStart, dateStop));
        addLog("Open Login Page");
        yourTripMenu.click();
        signIn.click();

        driver.switchTo().frame("modal_window");

        applyExplicitWaitByWebElementForVisibilityOfElement(driver, signInButton, 30);
        sendKeys(driver, email, username);
        sendKeys(driver, password, passwd);
        
        dateStart=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
        clickByJseBasedOnWebElement(driver, signInButton);
    }

    public void search(BookingRequest bookingRequest, int wait) throws InterruptedException, IOException {
        waitForElement(yourTripMenu, wait);
        
        applyExplicitWaitByWebElementForVisibilityOfElement(driver, oneWayElement, 30);
        dateStop=getCurrentDateTime("MM/dd/yyyy HH:mm:ss.SSS");
    	writeLoadTimeInFile("Time taken to signIn on home page is : " +GetDateTimeDiff(dateStart, dateStop));
    	
        if (bookingRequest.getBookingType().equals(BookingType.ONE_WAY)) {
            oneWayElement.click();
        } else {
            roundTripElement.click();
        }
        String from=bookingRequest.getFrom();
        String to=bookingRequest.getTo();
        sendKeys(driver, fromElement, from);
        sendKeys(driver, toElement, to);
        
        departDateElement.clear();
        clickByJseBasedOnWebElement(driver, departDateElement);

        int departYear=bookingRequest.getDepartYear();
        String departMonthName=bookingRequest.getDepartMonthName();
        int departDay=bookingRequest.getDepartDay();
        int departWeekOfTheMonth=bookingRequest.getDepartWeekOfTheMonth();
        
        String departMonthOnDatePicker=monthOnDatePicker.replace("monthName", departMonthName);
        for (int i = 0; i < 12; i++) {
        	if (fluentWait(By.xpath(departMonthOnDatePicker), 1) == null) {
    			clickByJseBasedOnWebElement(driver, we_nextArrowOnDatePicker);
    		}else {
    			break;
    		}
		}
        String selectDepartDate=selectDate.replace("monthName", departMonthName).replace("year", String.valueOf(departYear)).replace("weekNumberOfTheMonth", String.valueOf(departWeekOfTheMonth)).replace("day", String.valueOf(departDay));
        clickByJseBasedOnXpath(driver, selectDepartDate);
        
        if (!StringUtils.isEmpty(bookingRequest.getReturnDate())) {
            returnDateElement.clear();
            clickByJseBasedOnWebElement(driver, returnDateElement);
            
            int returnYear=bookingRequest.getReturnYear();
            String returnMonthName=bookingRequest.getReturnMonthName();
            int returnDay=bookingRequest.getReturnDay();
            int returnWeekOfTheMonth=bookingRequest.getReturnWeekOfTheMonth();
            
            String returnMonthOnDatePicker=monthOnDatePicker.replace("monthName", returnMonthName);
            for (int i = 0; i < 12; i++) {
            	if (fluentWait(By.xpath(returnMonthOnDatePicker), 1) == null) {
        			clickByJseBasedOnWebElement(driver, we_nextArrowOnDatePicker);
        		}else {
        			break;
        		}
    		}
            String selectReturnDate=selectDate.replace("monthName", returnMonthName).replace("year", String.valueOf(returnYear)).replace("weekNumberOfTheMonth", String.valueOf(returnWeekOfTheMonth)).replace("day", String.valueOf(returnDay));
            clickByJseBasedOnXpath(driver, selectReturnDate);		
        }
        
        adultsElement.sendKeys(String.valueOf(bookingRequest.getAdults()));
        childrensElement.sendKeys(String.valueOf(bookingRequest.getChilds()));
        infantsElement.sendKeys(String.valueOf(bookingRequest.getInfants()));
       
        moreOptionsLink.click();
        selectWithVisibleText(travelClassElement, bookingRequest.getTravelClass());
        if (!StringUtils.isEmpty(bookingRequest.getPreferredAirLine())) {
            selectOptionWithText(airlineAutocompleteElement, bookingRequest.getPreferredAirLine(), selectionPreferredAirline, true);
        }
        searchBtnElement.submit();
    }

    public String fetchLoginDetails() {
    	applyExplicitWaitByWebElementForVisibilityOfElement(driver, loginDone, 120);
        return loginElement.getText();
    }
}
