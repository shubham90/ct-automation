package com.cleartrip.core.service;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;
import org.apache.commons.jxpath.xml.DOMParser;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.w3c.dom.Document;

import com.cleartrip.core.factory.HttpClientFactory;
import com.cleartrip.core.model.BookingRequest;
import com.cleartrip.core.model.ConnectorResponse;
import com.cleartrip.core.pages.PageBase;

public class ConnectorService{

	private SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
	private SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd/MM/yyyy");

	private String ALL_RESPONSE = "//air-search-result/solutions/solution";
	private String ONWARD_RESPONSE = "//air-search-result/onward-solutions/solution";

	private static HttpClient httpClient;
	private String URL;
	
	public ConnectorService(String userName, String password, String url) {
		this.URL = url;
		httpClient = HttpClientFactory.getHttpClient(userName, password);
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public ConnectorResponse fetch(BookingRequest bookingRequest, Map<String, Set<String>> mappings,String channel,String domain,boolean isIntl) throws Exception {

		ConnectorResponse connectorResponse = new ConnectorResponse();
		String intl="n";
		if(isIntl) {
			intl="y";
		}

		for (Map.Entry<String, Set<String>> entry : mappings.entrySet()) {
			for (String airline : entry.getValue()) {
				if (airline.equalsIgnoreCase("X")) {
					continue;
				}
				String url = URL + bookingRequestGenerator(bookingRequest, entry.getKey(), airline,channel,domain)+"intl="+intl;
				PageBase.addLog("connector url ::: " + url);
				PageBase.addTripIdLog("connector url ::: " + url);

				int CONNECTION_TIMEOUT_MS = 120 * 1000; // Timeout in millis.
				RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
						.setConnectTimeout(CONNECTION_TIMEOUT_MS).setSocketTimeout(CONNECTION_TIMEOUT_MS).build();

				HttpGet httpget = new HttpGet(url);
				httpget.setConfig(requestConfig);
				HttpResponse response = httpClient.execute(httpget);
				String xml = IOUtils.toString(response.getEntity().getContent());
				Document doc = (Document) new DOMParser().parseXML(new ByteArrayInputStream(xml.getBytes("UTF-8")));
				JXPathContext context = JXPathContext.newContext(doc);

				String resultExpression = ONWARD_RESPONSE;
				Iterator<Pointer> iteration = context.iteratePointers(resultExpression);

				if (!iteration.hasNext()) {
					resultExpression = ALL_RESPONSE;
				}

				int count = 0;
				for (Iterator<Pointer> iter = context.iteratePointers(resultExpression); iter.hasNext();) {
					Pointer p = iter.next();
//                    System.out.println("result :::" + p);
					++count;
				}
				PageBase.addLog("Total solutions count in connector search : " + count);
				if (count == 0) {
					PageBase.addLog(
							"Connector result not found for supplier ::: " + entry.getKey() + "  Airline : " + airline);
					connectorResponse.setSuccess(false);
					return connectorResponse;
				}
			}
		}
		connectorResponse.setSuccess(true);
		return connectorResponse;

	}

	private String bookingRequestGenerator(BookingRequest bookingRequest, String supplier, String airline,String channel,String domain)
			throws Exception {
		StringBuilder sb = new StringBuilder();
		append(sb, "from", bookingRequest.getFrom());
		append(sb, "to", bookingRequest.getTo());
		append(sb, "depart_date", simpleDateFormat2.format(simpleDateFormat1.parse(bookingRequest.getDepartDate())));
		if (StringUtils.isNotBlank(bookingRequest.getReturnDate())) {
			append(sb, "return_date",
					simpleDateFormat2.format(simpleDateFormat1.parse(bookingRequest.getReturnDate())));
		}
		append(sb, "adults", bookingRequest.getAdults() + "");
		append(sb, "childs", bookingRequest.getChilds() + "");
		append(sb, "infants", bookingRequest.getInfants() + "");
		append(sb, "class", bookingRequest.getTravelClass());
		append(sb, "src", "connector");
		append(sb, "airline", airline.toUpperCase());
		append(sb, "carrier", airline.toUpperCase());
		
		if (StringUtils.isNotEmpty(channel)) {
			append(sb, "source", channel.trim().toUpperCase());
		}
		
		if (StringUtils.isNotEmpty(domain)) {
			append(sb, "sct", domain.trim().toUpperCase().replaceAll("COM", "IN"));
		}

		if (!supplier.equalsIgnoreCase(("X"))) {
			append(sb, "suppliers", supplier.toUpperCase());
		}

		return sb.toString();
	}

	private void append(StringBuilder sb, String key, String value) {
		sb.append(key);
		sb.append("=");

		sb.append(value);
		sb.append("&");
	}

}
