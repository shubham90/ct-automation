package com.cleartrip.core.service;

import java.io.ByteArrayInputStream;
import java.util.Iterator;

import org.apache.commons.io.IOUtils;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;
import org.apache.commons.jxpath.xml.DOMParser;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.w3c.dom.Document;

import com.cleartrip.core.factory.HttpClientFactory;
import com.cleartrip.core.model.TripXMLResponse;
import com.cleartrip.core.pages.PageBase;

public class TripXMLService {

    private static HttpClient httpClient;
    private String URL;
    private static String PNR_REGEX = "/trip/air-bookings/air-booking/booking-info-list/booking-info/airline-pnr";
    private static String BOOKING_STATUS = "//booking-status";
    private static String TRIP_BOOK_STATUS = "/trip/booking-status";
    private static String TRIP_ID = "TRIP-ID";


    public TripXMLService(String userName, String password, String url) {
        this.URL = url;
        httpClient = HttpClientFactory.getHttpClient(userName, password);

    }

    @SuppressWarnings("unchecked")
	public TripXMLResponse fetchTripXML(String tripId) throws Exception {
		TripXMLResponse tripXMLResponse = new TripXMLResponse();

		PageBase.addLog("Trip XML url: "+URL.replaceAll(TRIP_ID, tripId));
		//PageBase.addTripIdLog("Trip XML url: "+URL.replaceAll(TRIP_ID, tripId));
		for (int count = 0; count < 20; count++) {
			HttpGet httpget = new HttpGet(URL.replaceAll(TRIP_ID, tripId));
			HttpResponse response = httpClient.execute(httpget);
			String xml = IOUtils.toString(response.getEntity().getContent());
			Document doc = (Document) new DOMParser().parseXML(new ByteArrayInputStream(xml.getBytes("UTF-8")));
			JXPathContext context = JXPathContext.newContext(doc);

			String status = (String) context.getValue(TRIP_BOOK_STATUS);

			tripXMLResponse.setOverAllBookingStatus(status);
			if (StringUtils.isBlank(status)) {
				Thread.sleep(10000);
				if(!(count == 19)) {
					continue;
				}
			}

			for (Iterator<Pointer> iter = context.iteratePointers(PNR_REGEX); iter.hasNext();) {
				String pnr = (String) ((Pointer) iter.next()).getValue();
				tripXMLResponse.addPNR(pnr);

			}

			for (Iterator<Pointer> iter = context.iteratePointers(BOOKING_STATUS); iter.hasNext();) {
				String bookingStatus = (String) ((Pointer) iter.next()).getValue();
				tripXMLResponse.addIndividualBookingStatus(bookingStatus);

			}
			return tripXMLResponse;

		}
		tripXMLResponse.setOverAllBookingStatus("");
		return tripXMLResponse;
	}

}
