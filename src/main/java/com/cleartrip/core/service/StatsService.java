package com.cleartrip.core.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import com.cleartrip.core.factory.HttpClientFactory;
import com.cleartrip.core.model.APIResponse;
import com.cleartrip.core.model.StatsResponse;
import com.cleartrip.core.pages.PageBase;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class StatsService {

	private static HttpClient httpClient;
	private String URL;
	ObjectMapper mapper = new ObjectMapper();
	private String SMS_VERIFY_STRING = "air-SMS";
	private String SFF_VERIFY_STRING = "profitOnCtc";
	private List<String> ignorableList = Arrays.asList("freqFlyerService");

	public StatsService(String userName, String password, String url) {
		this.URL = url;
		httpClient = HttpClientFactory.getHttpClient(userName, password);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public StatsResponse fetchStats(String tripId) throws Exception {
		PageBase.addLog("Stats Url: "+URL.replaceAll("TRIP-ID", tripId));
		//PageBase.addTripIdLog("Stats Url: "+URL.replaceAll("TRIP-ID", tripId));
		HttpGet httpget = new HttpGet(URL.replaceAll("TRIP-ID", tripId));
		List<APIResponse> responses = new ArrayList<>();
		HttpResponse response = httpClient.execute(httpget);
		Map<String, Object> map = new HashMap<>();

		map = mapper.readValue(IOUtils.toString(response.getEntity().getContent()),
				new TypeReference<Map<String, Object>>() {
				});

		StatsResponse statsResponse = new StatsResponse();
		List<String> logs = ((ArrayList) map.get("tomcat_logs"));
		for (String log : logs) {
			if (log.contains(SMS_VERIFY_STRING)) {
				statsResponse.setSmsFlow(true);
			}
			if (log.contains(SFF_VERIFY_STRING)) {
				statsResponse.setSffFlow(true);
			}
		}

		List<Map> apiApiResponse = ((ArrayList) map.get("air_api_calls"));
		List<Map> ctResponse = ((ArrayList) map.get("air_ct_calls"));
		List<String> apiCalls = new ArrayList<>();

		Map<String, String> apiFailureResponse = failuresCalls(apiApiResponse, true, apiCalls, responses);
		Map<String, String> ctFailureResponse = failuresCalls(ctResponse, false, null, responses);

		statsResponse.getFailures().putAll(apiFailureResponse);
		statsResponse.getFailures().putAll(ctFailureResponse);
		statsResponse.getApiCalls().addAll(apiCalls);
		statsResponse.setApiResponses(responses);
		//PageBase.addLog("statsResponse : " + statsResponse);
		return statsResponse;

	}

	@SuppressWarnings("rawtypes")
	private Map<String, String> failuresCalls(List<Map> apiResponse, boolean calls, List<String> apiCalls, List<APIResponse> responses)
			throws Exception {
		Map<String, String> faliures = new HashMap<>();
		boolean isContinue = false;
		for (Map res : apiResponse) {
			Integer httpCode = (Integer) res.get("http_code");
			String id = (String) res.get("id");
			String tid = (String) res.get("tid");
			String event = (String) res.get("event");
			String apiType = (String) res.get("api_type");
			String url = (String) res.get("url");
			String api = (String) res.get("api");
			
			if (StringUtils.isNotBlank(url) ) {
				for (String ignorable : ignorableList) {
					if (url.contains(ignorable)) {
						isContinue = true;
						break;
					}
				}
				
			}
			
			if (isContinue) {
				isContinue = false;
				continue;
			}
			
			if (calls) {
				apiCalls.add(apiType);
			}

			String statsInnerUrl = "http://172.17.12.171/showbookresult.php?itineraryId=" + id +

					"&event=" + event + "&column=api_res" + "&tid=" + tid;

			if (url == null || url.contains("CACHED") || url.contains("airservice") || url.contains("chronicle")) {
				continue;
			}

			HttpGet httpget = new HttpGet(statsInnerUrl);

			HttpResponse response = httpClient.execute(httpget);
			String responseSub = IOUtils.toString(response.getEntity().getContent());
			boolean isError = responseSub.toLowerCase().matches("timeout|time out|exception");
			responses.add(new APIResponse(responseSub, api, url));
			if (isError) {

				faliures.put(apiType, responseSub);

			} else if (httpCode != 200) {
				if (httpCode != 0) {
					faliures.put(apiType, httpCode + "");
				}
			}

		}
		return faliures;
	}
}
